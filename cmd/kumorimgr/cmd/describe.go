/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package cmd

import (
	"cluster-manager/pkg/admission"
	"cluster-manager/pkg/logger"
	"fmt"
	"math"
	"os"
	"regexp"
	"sort"
	"strconv"
	"strings"

	"github.com/Jeffail/gabs"
	"github.com/fatih/color"
	"github.com/olekukonko/tablewriter"
	"github.com/spf13/cobra"
)

// describeCmd represents the describe command
var describeCmd = &cobra.Command{
	Use:   "describe",
	Short: `Describe cluster`,
	Long: `
This command gathers nodes and pods information.`,
	Run: func(cmd *cobra.Command, args []string) { runDescribe(cmd, args) },
}

func init() {
	rootCmd.AddCommand(describeCmd)
	describeCmd.Flags().StringP(
		"output", "o", "table",
		"Output format: table or json",
	)
	describeCmd.Flags().StringP(
		"nodes", "n", "all",
		"Nodes to take into account: all, masters, workers, exact node name or regular expression",
	)
	describeCmd.Flags().BoolP(
		"print-pods", "p", false,
		"Show pods per node",
	)
}

func runDescribe(cmd *cobra.Command, args []string) {
	meth := "describe.runDescribe()"
	logger.Info("Describe", "meth", meth)

	output, err := cmd.Flags().GetString("output")
	if err != nil {
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}
	nodes, err := cmd.Flags().GetString("nodes")
	if err != nil {
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}
	printPods, err := cmd.Flags().GetBool("print-pods")
	if err != nil {
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}

	var state *gabs.Container
	state, err = admission.DescribeCluster()
	if err != nil {
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}

	err = filterNodes(state, nodes)
	if err != nil {
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}

	if output == "table" {
		prettyPrintState(state, printPods)
	} else if output == "json" {
		if !printPods {
			// For JSON output remove Pod info
			err = removePods(state)
			if err != nil {
				logger.Error(err.Error(), "meth", meth)
				os.Exit(1)
			}
		}
		fmt.Println(state.StringIndent("", "  "))
	} else {
		err = fmt.Errorf("Invalid output format %s. Valid formats are 'table' and 'json'", output)
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}
	return
}

func filterNodes(state *gabs.Container, filter string) (err error) {
	nodelist, err := state.ChildrenMap()
	if err != nil {
		return
	}
	switch filter {
	case "all":
		return
	case "master":
	case "worker":
		for key, value := range nodelist {
			role, ok := value.Path("role").Data().(string)
			if ok {
				if (filter == "master" && role != "master") ||
					(filter == "worker" && role != "worker") {
					state.Delete(key)
				}
			}
		}
		return
	default: // regular expression
		var matched bool
		for key := range nodelist {
			matched, err = regexp.MatchString(filter, key)
			if err != nil {
				return
			}
			if !matched {
				state.Delete(key)
			}
		}
	}
	return
}

func removePods(state *gabs.Container) (err error) {
	nodelist, err := state.ChildrenMap()
	if err != nil {
		return
	}
	for _, value := range nodelist {
		value.Delete("pods")
	}
	return
}

func prettyPrintState(state *gabs.Container, printPods bool) {

	fmt.Println()
	// Nodes overview table
	var mastersData [][]string
	var workersData [][]string

	mastersData, workersData = generateNodesTableData(state)

	nodesTable := tablewriter.NewWriter(os.Stdout)
	nodesTable.SetHeader([]string{"NODE", "ROLE", "READY", "PIDs", "DISK", "MEM", "NET",
		"PODS", "CPUs", "USAGE CPU", "CPU REQ", "CPU LIM", "MEM Gi", "USAGE MEM",
		"MEM REQ", "MEM LIM"})
	nodesTable.SetBorder(true) // Set Border to false
	nodesTable.SetAutoWrapText(false)
	nodesTable.AppendBulk(mastersData) // Add Bulk Data
	nodesTable.AppendBulk(workersData) // Add Bulk Data
	nodesTable.SetAlignment(tablewriter.ALIGN_CENTER)
	nodesTable.Render()
	fmt.Println()
	fmt.Println()

	if printPods {
		// Pods per node table - One table per Node
		data, _ := state.ChildrenMap()

		// Get keys (nodeName) and sort them to loop in order
		keys := make([]string, len(data))
		i := 0
		for k := range data {
			keys[i] = k
			i++
		}
		sort.Strings(keys)

		for _, nodeName := range keys {
			nodeData := data[nodeName]
			nodeRole := strings.ToUpper(nodeData.Path("role").Data().(string))

			var podsData [][]string

			podsData = generatePodsTableData(nodeData.Path("pods"), nodeName, nodeRole)

			podsTable := tablewriter.NewWriter(os.Stdout)
			podsTable.SetHeader([]string{
				"NODE",
				"ROLE",
				"PODS",
				"NAMESPACE",
				"PHASE",
				"SCHEDULED",
				"READY",
				"CONTS READY",
				"INITIALIZED",
				"CONTAINERS"})
			podsTable.SetBorder(true)      // Set Border to false
			podsTable.AppendBulk(podsData) // Add Bulk Data
			podsTable.SetAlignment(tablewriter.ALIGN_CENTER)

			podsTable.SetColumnAlignment([]int{
				tablewriter.ALIGN_CENTER,
				tablewriter.ALIGN_CENTER,
				tablewriter.ALIGN_LEFT,
				tablewriter.ALIGN_LEFT,
				tablewriter.ALIGN_CENTER,
				tablewriter.ALIGN_CENTER,
				tablewriter.ALIGN_CENTER,
				tablewriter.ALIGN_CENTER,
				tablewriter.ALIGN_CENTER,
				tablewriter.ALIGN_CENTER})

			podsTable.Render()
			fmt.Println()
			fmt.Println()
		}
	}
}

func generatePodsTableData(state *gabs.Container, nodeName string, nodeRole string) (podsData [][]string) {
	// Iterate Node pods to build Pod overview
	data, _ := state.ChildrenMap()

	// Get keys (nodeName) and sort them to loop in order
	keys := make([]string, len(data))
	i := 0
	for k := range data {
		keys[i] = k
		i++
	}
	sort.Strings(keys)

	isFirst := true
	for _, podName := range keys {
		podData := data[podName]
		podRow := []string{}

		if isFirst {
			podRow = append(podRow, prettyBold(nodeName))
			podRow = append(podRow, prettyBold(nodeRole))
			isFirst = false
		} else {
			podRow = append(podRow, " ")
			podRow = append(podRow, " ")
		}

		podRow = append(podRow, podName)
		podRow = append(podRow, podData.Path("namespace").Data().(string))
		podRow = append(podRow, prettyPodPhase(podData.Path("phase").Data().(string)))

		// Pod Conditions
		podRow = append(podRow, prettyConditionPositive(strings.ToUpper(podData.Path("conditions.PodScheduled").Data().(string))))
		podRow = append(podRow, prettyConditionPositive(strings.ToUpper(podData.Path("conditions.Ready").Data().(string))))
		podRow = append(podRow, prettyConditionPositive(strings.ToUpper(podData.Path("conditions.ContainersReady").Data().(string))))
		podRow = append(podRow, prettyConditionPositive(strings.ToUpper(podData.Path("conditions.Initialized").Data().(string))))

		podContainers, _ := podData.Path("containers").ChildrenMap()
		podRow = append(podRow, strconv.Itoa(len(podContainers)))

		podsData = append(podsData, podRow)
	}
	return
}

func generateNodesTableData(state *gabs.Container) (mastersData [][]string, workersData [][]string) {
	// Iterate JSON Nodes data to build general Node overview
	data, _ := state.ChildrenMap()

	// Get keys (nodeName) and sort them to loop in order
	keys := make([]string, len(data))
	i := 0
	for k := range data {
		keys[i] = k
		i++
	}
	sort.Strings(keys)

	for _, nodeName := range keys {
		nodeData := data[nodeName]
		nodeRow := []string{nodeName}
		role := strings.ToUpper(nodeData.Path("role").Data().(string))
		isMaster := role == "MASTER"

		nodeRow = append(nodeRow, strings.ToUpper(nodeData.Path("role").Data().(string)))

		// Maintenance mode label
		isMaintenance := false
		if nodeData.Search("maintenance").Data() != nil {
			isMaintenance = nodeData.Path("maintenance").Data().(bool)
		}

		// Node Conditions
		readyStr := prettyConditionPositive(strings.ToUpper(nodeData.Path("conditions.Ready").Data().(string)))
		if isMaintenance {
			readyStr = readyStr + " " + prettyMaintenance("(MAINT)")
		}
		nodeRow = append(nodeRow, readyStr)
		nodeRow = append(nodeRow, prettyConditionNegative(strings.ToUpper(nodeData.Path("conditions.PIDPressure").Data().(string))))
		nodeRow = append(nodeRow, prettyConditionNegative(strings.ToUpper(nodeData.Path("conditions.DiskPressure").Data().(string))))
		nodeRow = append(nodeRow, prettyConditionNegative(strings.ToUpper(nodeData.Path("conditions.MemoryPressure").Data().(string))))

		if nodeData.Search("conditions", "NetworkUnavailable").Data() != nil {
			nodeRow = append(nodeRow, prettyConditionNegative(strings.ToUpper(nodeData.Path("conditions.NetworkUnavailable").Data().(string))))
		} else {
			nodeRow = append(nodeRow, prettyConditionNegative("Unknown"))
		}

		// Number of PODS in the Node
		nodePods, _ := nodeData.Path("pods").ChildrenMap()
		nodeRow = append(nodeRow, strconv.Itoa(len(nodePods)))

		// Node resources usage - CPU
		nodeRow = append(nodeRow, nodeData.Path("usage.cpu.capacity").String())
		var perc = (nodeData.Path("usage.cpu.measured").Data().(float64) / nodeData.Path("usage.cpu.capacity").Data().(float64)) * 100
		nodeRow = append(nodeRow, fmt.Sprintf("%.2f%%", perc))
		nodeRow = append(nodeRow, fmt.Sprintf("%.2f", nodeData.Path("usage.cpu.requestTotal").Data().(float64)))
		nodeRow = append(nodeRow, fmt.Sprintf("%.2f", nodeData.Path("usage.cpu.limitTotal").Data().(float64)))

		// Node resources usage - MEMORY
		var cleanMemCapacity = nodeData.Path("usage.memory.capacity").Data().(float64) / (1024 * 1024 * 1024)
		nodeRow = append(nodeRow, fmt.Sprintf("%.0f", math.Round(cleanMemCapacity)))
		perc = (nodeData.Path("usage.memory.measured").Data().(float64) / nodeData.Path("usage.memory.capacity").Data().(float64)) * 100
		nodeRow = append(nodeRow, fmt.Sprintf("%.2f%%", perc))
		cleanMemRequests := nodeData.Path("usage.memory.requestTotal").Data().(float64) / (1024 * 1024 * 1024)
		cleanMemLimits := nodeData.Path("usage.memory.limitTotal").Data().(float64) / (1024 * 1024 * 1024)
		nodeRow = append(nodeRow, fmt.Sprintf("%.2f", cleanMemRequests))
		nodeRow = append(nodeRow, fmt.Sprintf("%.2f", cleanMemLimits))

		if isMaster {
			mastersData = append(mastersData, nodeRow)
		} else {
			workersData = append(workersData, nodeRow)
		}
	}
	return
}

// Conditions where True means bad (DiskPressure, etc.)
func prettyConditionNegative(conditionValue string) (prettyValue string) {
	// yellow := color.New(color.FgYellow).SprintFunc()
	red := color.New(color.FgRed, color.Bold).SprintFunc()
	green := color.New(color.FgGreen).SprintFunc()
	yellow := color.New(color.FgYellow).SprintFunc()
	if (conditionValue == "True") || (conditionValue == "TRUE") {
		prettyValue = fmt.Sprintf("%s", red("TRUE"))
		//fmt.Println(prettyValue)
	} else if (conditionValue == "Unknown") || (conditionValue == "UNKNOWN") {
		prettyValue = fmt.Sprintf("%s", yellow("N/A"))
		//fmt.Println(prettyValue)
	} else {
		prettyValue = fmt.Sprintf("%s", green("OK"))
		//fmt.Println(prettyValue)
	}
	return
}

// Conditions where True means good (Ready, Initialized, etc.)
func prettyConditionPositive(readyValue string) (prettyValue string) {
	// yellow := color.New(color.FgYellow).SprintFunc()
	red := color.New(color.FgRed, color.Bold).SprintFunc()
	green := color.New(color.FgGreen, color.Bold).SprintFunc()
	if (readyValue == "True") || (readyValue == "TRUE") {
		prettyValue = fmt.Sprintf("%s", green("YES"))
		//fmt.Println(prettyValue)
	} else {
		prettyValue = fmt.Sprintf("%s", red("NO"))
		//fmt.Println(prettyValue)
	}
	return
}

// Conditions where True means good (Ready, Initialized, etc.)
func prettyBold(value string) (prettyValue string) {
	// yellow := color.New(color.FgYellow).SprintFunc()
	bold := color.New(color.Bold).SprintFunc()
	prettyValue = fmt.Sprintf("%s", bold(value))
	return
}

// Format as bold yellow
func prettyMaintenance(value string) (prettyValue string) {
	boldYellow := color.New(color.FgYellow, color.Bold).SprintFunc()
	prettyValue = fmt.Sprintf("%s", boldYellow(value))
	return
}

// Pod phases
func prettyPodPhase(value string) (prettyValue string) {
	green := color.New(color.FgGreen, color.Bold).SprintFunc()
	yellow := color.New(color.FgYellow, color.Bold).SprintFunc()
	red := color.New(color.FgRed, color.Bold).SprintFunc()
	if strings.EqualFold(value, "pending") {
		prettyValue = fmt.Sprintf("%s", yellow(value))
	} else if strings.EqualFold(value, "running") {
		prettyValue = fmt.Sprintf("%s", green(value))
	} else if strings.EqualFold(value, "succeeded") {
		prettyValue = fmt.Sprintf("%s", green(value))
	} else if strings.EqualFold(value, "failed") {
		prettyValue = fmt.Sprintf("%s", red(value))
	} else if strings.EqualFold(value, "unknown") {
		prettyValue = fmt.Sprintf("%s", yellow(value))
	} else {
		prettyValue = value
	}
	return
}
