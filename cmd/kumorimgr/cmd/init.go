/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package cmd

import (
	"cluster-manager/pkg/git"
	"cluster-manager/pkg/helper"
	"cluster-manager/pkg/logger"
	"cluster-manager/pkg/viper"
	"fmt"
	"os"
	"strings"
	"time"

	dirCopy "github.com/otiai10/copy"

	"github.com/spf13/cobra"
	cuedm "gitlab.com/kumori-systems/community/libraries/cue-dependency-manager"
)

// Init command:
// - Removes current configuration
// - Creates a base config from cluster (a cue module)
// - Resolves the cue dependencies of that config, available in git
// - Downloads scripts used for create the cluster, downloaded from git

// initCmd represents the init command
var initCmd = &cobra.Command{
	Use:   "init",
	Short: `Initialize a workspace for kumorimgr`,
	Long: `
For kumorimgr to work, a workspace must be initialized in a directory.

This command creates a base configuration in './cluster' directory, with cluster.cue, distribution.cue and config.cue files (and its 'cue' dependencies).
This command creates the scripts, in './scripts' directory, used by kumorimgr to create a new cluster.

If './cluster' or './scripts' already exists, a backup is created.

Moreover, it creates the config file required to use kumorimgr in .kumori directory. In order to create config file in your HOME directory, set ` + "`--global`" + ` flag. You can also use ` + "`--config`" + ` flag to specify a custom location.

The config file is created using global config or, if missing, defaults. To customize configuration parameters, then use ` + "`kumorictl config`" + `.

Please note this command will purposely fail if the current directory has already been initialized as a workspace.`,
	Run: func(cmd *cobra.Command, args []string) { runInit(cmd, args) },
}

const clusterDir = "./cluster"
const scriptsDir = "./scripts"
const communityScriptsDir = "./community-scripts"
const enterpriseScriptsDir = "./enterprise-scripts"

func init() {
	rootCmd.AddCommand(initCmd)
	initCmd.Flags().BoolP("global", "g", false, "write config file in HOME directory")
	initCmd.Flags().StringP(
		"distribution", "d", "community",
		"Distribution to be used in the cluster",
	)
}

func runInit(cmd *cobra.Command, args []string) {
	meth := "init.runInit()"
	distribution, err := cmd.Flags().GetString("distribution")
	if err != nil {
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}
	initCreateConfigFile(cmd)
	initBackupWorkspace()
	initCreateWorkspace(distribution)
}

func initCreateConfigFile(cmd *cobra.Command) {
	meth := "init.initCreateConfigFile()"
	if cfgFile == "" {
		logger.Debug("Creating config file", "meth", meth)
		global, err := cmd.Flags().GetBool("global")
		if err != nil {
			logger.Error(err.Error(), "meth", meth)
			os.Exit(1)
		}
		if global {
			viper.Global.SetConfigFile(home + "/.kumori/kumorimgr.json")
		} else {
			viper.Global.SetConfigFile("./.kumori/kumorimgr.json")
		}
		writeDefaultConfigFile()
		// Reload configuration from configuration file (it may just have been created)
		err = viper.Global.ReadInConfig()
		if err != nil {
			logger.Error(err.Error())
			os.Exit(1)
		}
	}
}

func initBackupWorkspace() {
	meth := "init.initBackupWorkspace()"

	// Creates a backup of current directories
	logger.Info("Creating workspace backup", "meth", meth)
	nowStr := time.Now().Format("20060102150405")
	err := helper.RenameIfExists(clusterDir, "./bak/cluster_"+nowStr)
	if err != nil {
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}
	err = helper.RenameIfExists(scriptsDir, "./bak/scripts_"+nowStr)
	if err != nil {
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}
	err = helper.RenameIfExists(communityScriptsDir, "./bak/community-scripts_"+nowStr)
	if err != nil {
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}
	err = helper.RenameIfExists(enterpriseScriptsDir, "./bak/enterprise-scripts_"+nowStr)
	if err != nil {
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}
}

func initCreateWorkspace(distribution string) {
	meth := "init.initCreateWorkspace()"

	switch distribution {
	case "enterprise":
		initCreateWorkspaceEnterprise(distribution)
	default:
		initCreateWorkspaceGeneric(distribution)
	}

	// Store the currently initialized distribution name in the config file
	viper.Global.Set("current-distribution", distribution)
	err := viper.Global.WriteConfig()
	if err != nil {
		logger.Error("Failed saving distribution name to config file. "+err.Error(), "meth", meth)
		os.Exit(1)
	}

	return
}

func initCreateWorkspaceGeneric(distribution string) {
	meth := "init.initCreateWorkspaceGeneric()"

	// Creates a the cluster configuration directory, using git
	// Creates a scripts directory, using git
	logger.Info("Cloning configuration", "meth", meth)

	// Check that the distribution configuration exists
	genericDistribution := "cluster-configuration::" + distribution
	if !viper.Global.IsSet(genericDistribution) {
		err := fmt.Errorf("Distribution configuration not found for '" + distribution + "'")
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}

	// Get git credentials from config section (if any)
	var credConfig *git.Credentials = nil
	credConfig, err := git.NewCredentialsFromConfig(genericDistribution)
	if err != nil {
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}

	// Clone Community distribution
	if !viper.Global.IsSet(genericDistribution + "::distribution-url") {
		err := fmt.Errorf("Distribution URL not found in '" + distribution + "' configuration")
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}
	clusterURL := viper.Global.GetString(genericDistribution + "::distribution-url")
	err = git.Clone(clusterDir, clusterURL, credConfig)
	if err != nil {
		errMsg := fmt.Sprintf("Error cloning repository %s: %s", clusterURL, err.Error())
		logger.Error(errMsg, "meth", meth)
		os.Exit(1)
	}

	// Resolve dependencies of the cluster (cue) directory, using git
	logger.Debug("Resolving cue dependencies (kumori-model)", "meth", meth)
	ctx, err := cuedm.Resolve(clusterDir)
	if err != nil {
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}
	for _, module := range ctx.CUEModules {
		logger.Debug(
			"Cluster-Dependency",
			"Module", module.Name,
			"Resolved", module.Resolved,
			"meth", meth,
		)
	}

	// Creates the scripts directory, using git
	logger.Info("Cloning "+distribution+" scripts", "meth", meth)

	if !viper.Global.IsSet(genericDistribution + "::scripts-url") {
		err := fmt.Errorf("Scripts URL not found in '" + distribution + "' configuration")
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}
	scriptsURL := viper.Global.GetString(genericDistribution + "::scripts-url")
	err = git.Clone(scriptsDir, scriptsURL, credConfig)
	if err != nil {
		errMsg := fmt.Sprintf("Error cloning repository %s: %s", scriptsURL, err.Error())
		logger.Error(errMsg, "meth", meth)
		os.Exit(1)
	}

	fmt.Println("Workspace initialized")
}

func initCreateWorkspaceEnterprise(distribution string) {
	meth := "init.initCreateWorkspaceEnterprise()"

	// Creates a the cluster configuration directory, using git
	// Creates two scripts directories one for community and one for enterprise, using git
	logger.Info("Cloning configuration", "meth", meth)

	// Check that the enterprise distribution configuration exists
	enterpriseDistribution := "cluster-configuration::enterprise"
	if !viper.Global.IsSet(enterpriseDistribution) {
		err := fmt.Errorf("Enterprise distribution configuration not found")
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}

	// Extract credentials from license info
	var credsConfig map[string]*git.Credentials
	credsConfig, err := NewCredentialsFromLicense(enterpriseDistribution)
	if err != nil {
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}

	// Clone Enterprise distribution
	if !viper.Global.IsSet(enterpriseDistribution + "::distribution-url") {
		err := fmt.Errorf("Distribution URL not found in '" + distribution + "' configuration")
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}
	enterpriseDistributionURL := viper.Global.GetString(enterpriseDistribution + "::distribution-url")
	err = git.Clone(clusterDir, enterpriseDistributionURL, credsConfig["distribution"])
	if err != nil {
		errMsg := fmt.Sprintf("Error cloning repository %s: %s", enterpriseDistributionURL, err.Error())
		logger.Error(errMsg, "meth", meth)
		os.Exit(1)
	}

	// Resolve dependencies of the cluster (cue) directory, using git
	logger.Debug("Resolving cue dependencies (kumori-model)", "meth", meth)
	ctx, err := cuedm.Resolve(clusterDir)
	if err != nil {
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}
	for _, module := range ctx.CUEModules {
		logger.Debug(
			"Cluster-Dependency",
			"Module", module.Name,
			"Resolved", module.Resolved,
			"meth", meth,
		)
	}

	// Creates the Enterprise scripts directory, using git
	logger.Info("Cloning Enterprise scripts", "meth", meth)

	if !viper.Global.IsSet(enterpriseDistribution + "::scripts-url") {
		err := fmt.Errorf("Scripts URL not found in '" + distribution + "' configuration")
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}
	enterpriseScriptsURL := viper.Global.GetString(enterpriseDistribution + "::scripts-url")
	logger.Info("Enterprise scripts repository "+enterpriseScriptsURL, "meth", meth)
	err = git.Clone(scriptsDir, enterpriseScriptsURL, credsConfig["scripts"])
	if err != nil {
		errMsg := fmt.Sprintf("Error cloning repository %s: %s", enterpriseScriptsURL, err.Error())
		logger.Error(errMsg, "meth", meth)
		os.Exit(1)
	}

	fmt.Println("Workspace initialized")
}

// This method is not used anymore because another approach is being tested.
// We leave it here since we may go back to KumoriMgr merging scripts directories.
func mergeScripts(communityScriptsDir, enterpriseScriptsDir, dstDir string) {
	meth := "init.mergeScripts()"

	opts := dirCopy.Options{
		Skip: func(src string) (bool, error) {
			return strings.HasSuffix(src, ".git-like"), nil
		},
		OnDirExists: func(src, dst string) dirCopy.DirExistsAction {
			return dirCopy.Merge
		},
	}

	// Merge Community scripts into the final scripts directory (empty at this stage)
	err := dirCopy.Copy(communityScriptsDir, dstDir, opts)
	if err != nil {
		errMsg := fmt.Sprintf("Error preparing Kumorimgr scripts (merge): %s", err.Error())
		logger.Error(errMsg, "meth", meth)
		os.Exit(1)
	}

	// Merge Enterprise scripts into the final scripts directory (merge with Community)
	err = dirCopy.Copy(enterpriseScriptsDir, dstDir, opts)
	if err != nil {
		errMsg := fmt.Sprintf("Error preparing Kumorimgr scripts (merge): %s", err.Error())
		logger.Error(errMsg, "meth", meth)
		os.Exit(1)
	}

}

func writeDefaultConfigFile() {
	meth := "init.writeDefaultConfigFile()"
	filePath := viper.Global.ConfigFileUsed()
	logger.Info("Writing config file: "+filePath, "meth", meth)
	if !helper.FileExists(filePath) {
		logger.Info("Creating config file", "file", filePath, "meth", meth)
		err := helper.CreateFile(filePath, AppConfigFilePerm, []byte(DefaultAppConfig))
		if err != nil {
			logger.Error(err.Error(), "meth", meth)
			os.Exit(1)
		}
	}
}
