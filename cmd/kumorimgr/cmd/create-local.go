/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package cmd

import (
	"cluster-manager/pkg/cue2json"
	"cluster-manager/pkg/logger"
	"cluster-manager/pkg/types"
	"cluster-manager/pkg/viper"
	"fmt"
	"os"

	"github.com/spf13/cobra"
)

// Create-Local command:
// - Compiles cue project
// - Generates variables.sh file expected by the installation scripts, from template
// - Run local (dev) installer scripts

// createLocalCmd represents the create command
var createLocalCmd = &cobra.Command{
	Use:   "create-local",
	Short: `Create a new local development cluster`,
	Long: `
Using the configuration stored in the './cluster' directory of the workspace, creates a new local development cluster.

It is required to have installed the CUE utility (https://cuelang.org/docs/install)`,
	Run: func(cmd *cobra.Command, args []string) { runCreateLocal(cmd, args) },
}

func init() {
	rootCmd.AddCommand(createLocalCmd)
}

func runCreateLocal(cmd *cobra.Command, args []string) {
	meth := "create.runCreateLocal()"
	logger.Info("Creating local development cluster", "meth", meth)

	// Determine the currently initialized distribution name (stored in config file)
	if !viper.Global.IsSet("current-distribution") {
		err := fmt.Errorf("Unable to determine currently initialized distribution name.")
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}
	distributionName := viper.Global.GetString("current-distribution")
	logger.Debug("Currently initialized distribution: " + distributionName)

	if !viper.Global.IsSet("cluster-configuration::" + distributionName + "::addons-installer::image") {
		err := fmt.Errorf("Addons installer image configuration not set.")
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}
	image := viper.Global.GetString("cluster-configuration::" + distributionName + "::addons-installer::image")

	// Compiling cue directories
	logger.Info("Compiling CUE files", "meth", meth)
	err := cue2json.ExportAll("./cluster", "./all.json")
	if err != nil {
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}
	fmt.Println("CUE files compiled")

	// Load json into structs, and add inline parameters to it
	logger.Debug("Loading json files into structs", "meth", meth)
	clusterAndDistribution, err := types.NewClusterAndDistributionFromFile("./cluster/all.json")
	if err != nil {
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}

	// TODO : decoupling from the script structure

	// Generates /scripts/scripts/installer-dev/variables.sh, used by the scripts
	logger.Debug(
		"Generating variables for scripts",
		"content", clusterAndDistribution, "meth", meth,
	)
	resultFile := "./scripts/scripts/installer-dev/variables.sh"
	err = prepareVarsFile(resultFile, clusterAndDistribution)
	if err != nil {
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}

	// Launch scripts
	err = launchScript("./scripts/scripts/installer-dev/installer.sh", image)
	if err != nil {
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}
}
