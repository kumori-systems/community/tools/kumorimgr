/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package cmd

import (
	"cluster-manager/pkg/cue2json"
	"cluster-manager/pkg/logger"
	"cluster-manager/pkg/types"
	"fmt"
	"os"

	"github.com/spf13/cobra"
)

// destroy-local command:
// - Compiles cue project
// - Generates variables.sh file expected by the installation scripts, from template
// - Run local (dev) cluster destruction scripts

// destroyLocalCmd represents the destroy local cluster command
var destroyLocalCmd = &cobra.Command{
	Use:   "destroy-local",
	Short: `Destroys a previously created local development cluster`,
	Long: `
Using the configuration stored in the './cluster' directory of the workspace, destroys a previously created local development cluster.

It is required to have installed the CUE utility (https://cuelang.org/docs/install)`,
	Run: func(cmd *cobra.Command, args []string) { runDestroyLocal(cmd, args) },
}

func init() {
	rootCmd.AddCommand(destroyLocalCmd)
}

func runDestroyLocal(cmd *cobra.Command, args []string) {
	meth := "create.runDestroyLocal()"
	logger.Info("Destroying local development cluster", "meth", meth)

	// Compiling cue directories
	logger.Info("Compiling CUE files", "meth", meth)
	err := cue2json.ExportAll("./cluster", "./all.json")
	if err != nil {
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}
	fmt.Println("CUE files compiled")

	// Load json into structs, and add inline parameters to it
	logger.Debug("Loading json files into structs", "meth", meth)
	clusterAndDistribution, err := types.NewClusterAndDistributionFromFile("./cluster/all.json")
	if err != nil {
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}

	// TODO : decoupling from the script structure

	// Generates /scripts/scripts/installer-dev/variables.sh, used by the scripts
	logger.Debug(
		"Generating variables for scripts",
		"content", clusterAndDistribution, "meth", meth,
	)
	resultFile := "./scripts/scripts/installer-dev/variables.sh"
	err = prepareVarsFile(resultFile, clusterAndDistribution)
	if err != nil {
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}

	// Launch scripts
	err = launchScript("./scripts/scripts/installer-dev/destroyer.sh", "")
	if err != nil {
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}
}
