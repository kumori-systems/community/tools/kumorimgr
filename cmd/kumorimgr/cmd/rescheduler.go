/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package cmd

import (
	"github.com/spf13/cobra"
)

// Rescheduler is a set of commands to manage custom runs of the platform internal
// descheduler

// reschedulerCmd represents the reschedule command
var reschedulerCmd = &cobra.Command{
	Use:   "rescheduler",
	Short: `Platform services instances rescheduling`,
	Long: `
The rescheduler is internally used to keep the role instances properly spread among
the platform nodes. This process is executed periodically using the configuration
provided during the cluster creation.

The rescheduler set of commands can be used to manually run the platform internal
descheduler and obtain information about the results.
`,
}

func init() {
	rootCmd.AddCommand(reschedulerCmd)
}
