/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package cmd

import (
	"cluster-manager/pkg/logger"
	"encoding/json"
	"fmt"
	"os"

	"cluster-manager/pkg/rescheduler"

	"github.com/spf13/cobra"
)

// Run command executes the platform deschedule process once. This process is
// used to evict pods if they fulfil certain conditions. By default, the reschedule
// picks up the same configuration used by the platform internal descheduler.
// However, a custom configuration can be also provided

// rescheduleCmd represents the reschedule command
var reschedulerRunCmd = &cobra.Command{
	Use:   "run",
	Short: `Runs the rescheduling process once`,
	Long: `
The rescheduler is internally used to keep the role instances properly spread among
the platform nodes. This process is executed periodically using the configuration
provided during the cluster creation.

This command manually runs the rescheduler. By default, it uses the same configuration
used by the internal rescheduler. However, a custom configuration file can be also
provided using the --strategies flag.

For example:

{
  "lowNodeUtilization": {
    "enabled": true,
    "threshold": {
      "cpu": 30,
      "memory": 30,
      "pods": 30
    },
    "target": {
      "cpu": 60,
      "memory": 60,
      "pods": 60
    }
  },
  "spreadConstraintsViolation": {
    "enabled": true
  }
}

The results of this custom executions are hold for one hour. After that, the
results are deleted from the cluster database.
`,
	Run: func(cmd *cobra.Command, args []string) { runReschedule(cmd, args) },
}

func init() {
	reschedulerCmd.AddCommand(reschedulerRunCmd)
	reschedulerRunCmd.Flags().StringP(
		"strategies", "s", "",
		"Path to the strategies configuration to be used by the rescheduling process.",
	)
	// rescheduleCmd.Flags().MarkHidden("strategies")
}

func runReschedule(cmd *cobra.Command, args []string) {
	meth := "rescheduler.runReschedule()"
	logger.Info(meth)
	configFilePath, err := cmd.Flags().GetString("strategies")
	if err != nil {
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}

	var resc rescheduler.Rescheduler
	if configFilePath != "" {
		logger.Debug(
			"Running rescheduler",
			"ConfigFilePath", configFilePath,
			"meth", meth,
		)
		config, err := loadConfig(configFilePath)
		if err != nil {
			logger.Error(err.Error(), "meth", meth)
			os.Exit(1)
		}
		resc = rescheduler.Rescheduler{
			Config: &config,
		}
	} else {
		logger.Debug(
			"Running rescheduler",
			"ConfigFilePath", "internal",
			"meth", meth,
		)
		resc = rescheduler.Rescheduler{}
	}

	err = resc.Run()
	if err != nil {
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	} else {
		fmt.Println("Rescheduler: launched")
	}
}

func loadConfig(configFilePath string) (config rescheduler.Config, err error) {
	configFileData, err := os.ReadFile(configFilePath)
	if err != nil {
		return
	}
	err = json.Unmarshal(configFileData, &config)
	if err != nil {
		return
	}
	return
}
