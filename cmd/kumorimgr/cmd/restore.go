/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package cmd

import (
	"cluster-manager/pkg/helper"
	"cluster-manager/pkg/logger"
	"fmt"
	"os"
	"path/filepath"

	"github.com/spf13/cobra"
)

// Restore command recover an existing cluster (perhaps because its state is
// corrupted), reusing its master and worker nodes, with the content of an
// etcd backup.

// restoreCmd represents the restore command
var restoreCmd = &cobra.Command{
	Use:   "restore <etcd-backup file>",
	Short: "Restore an etcd backup in the cluster",
	Long: `
Restores a provided etcd backup in all the masters of the cluster.

This command can be used to restore the state of a cluster whose etcd has been corrupted.`,
	Args: cobra.ExactArgs(1),
	Run:  func(cmd *cobra.Command, args []string) { runRestore(cmd, args) },
}

func init() {
	rootCmd.AddCommand(restoreCmd)
	restoreCmd.Flags().BoolVar(&skipScriptsCheck, "skip-scripts-check", false, "skip validation of local scripts repository")
}

func runRestore(cmd *cobra.Command, args []string) {
	meth := "restore.runRestore()"
	logger.Info("Restoring cluster", "meth", meth)

	// Validate local Scripts repository
	if !skipScriptsCheck {
		err := checkScriptsRepository()
		if err != nil {
			msg := fmt.Sprintf("Local scripts repository validation failed. This validation can be by-passed with the '--skip-scripts-check' flag.")
			fmt.Println(helper.ToRed(msg))
			err = fmt.Errorf("Validation error: : %s", err.Error())
			logger.Error(err.Error())
			os.Exit(1)
		}
	} else {
		logger.Debug("Scripts local repository check skipped.")
	}

	etcdRestoreFullPath := args[0]
	if !helper.FileExists(etcdRestoreFullPath) {
		logger.Error("File "+etcdRestoreFullPath+" not found", "meth", meth)
		os.Exit(1)
	}

	etcdRestoreScriptsDir := "./scripts/scripts/etcd-restorer/etcdbackup"
	etcdBackupFile := filepath.Base(etcdRestoreFullPath)
	_, err := helper.Copy(
		etcdRestoreFullPath,
		etcdRestoreScriptsDir+"/"+etcdBackupFile,
	)

	// Get cluster configuration, and add inline parameters to it
	cfgDir := "./cluster"
	cluster, err := getClusterConfiguration(cfgDir)
	if err != nil {
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}
	cluster.InlineParams.EtcdBackupFile = etcdBackupFile

	// TODO : decoupling from the script structure
	// Generates /scripts/scripts/installer/variables.sh, used by the scripts
	logger.Debug(
		"Generating variables for scripts",
		"content", cluster, "meth", meth,
	)
	resultFile := "./scripts/scripts/etcd-restorer/variables.sh"
	err = prepareVarsFile(resultFile, cluster)
	if err != nil {
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}

	// Launch scripts
	err = launchScript("./scripts/scripts/etcd-restorer/etcd-restorer.sh", "")
	if err != nil {
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}
}
