/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package cmd

import (
	"cluster-manager/pkg/logger"
	"encoding/json"
	"fmt"
	"os"
	"sort"
	"time"

	"cluster-manager/pkg/rescheduler"

	"github.com/olekukonko/tablewriter"
	"github.com/spf13/cobra"
)

// List command gets the last runs of the rescheduler run command. Each time the
// rescheduler run command is executed, the results are hold for one day. The
// list command will return the rescheduler runs executed in the last 24 hours.

// reschedulerListCmd represents the reschedule command
var reschedulerListCmd = &cobra.Command{
	Use:   "list",
	Short: `Lists the last rescheduler executions`,
	Long: `
The rescheduler is internally used to keep the role instances properly spread among
the platform nodes. This process is executed periodically using the configuration
provided during the cluster creation.

The rescheduler run command can be used to run a custom executions of the rescheduling
process. The results of this custom executions are hold for one hour.

The rescheduler lists command can be used to get a list of the custom rescheduler
executions executed in the last hour.
`,
	Run: func(cmd *cobra.Command, args []string) { listRescheduler(cmd, args) },
}

func init() {
	reschedulerCmd.AddCommand(reschedulerListCmd)
	reschedulerListCmd.Flags().StringP(
		"output", "o", "table",
		"Output format: table or json",
	)
}

func listRescheduler(cmd *cobra.Command, args []string) {
	meth := "rescheduler.listRescheduler()"
	logger.Info(meth)

	logger.Debug(
		"Listing rescheduler runs",
		"meth", meth,
	)

	output, err := cmd.Flags().GetString("output")
	if err != nil {
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}

	var resc rescheduler.Rescheduler
	runsJSON, err := resc.GetReschedulerRuns()
	if err != nil {
		fmt.Println("Error listing rescheduler runs: " + err.Error())
		return
	}
	runsList, err := rescheduler.RunsListFromJSON(runsJSON, true)
	sort.Sort(runsList)
	if err != nil {
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}

	if (runsList == nil) || (len(*runsList) == 0) {
		fmt.Printf("\nRescheduler jobs not found\n\n")
		return
	}

	if output == "table" {
		prettyPrintReschedulerRunsList(runsList)
	} else if output == "json" {
		content, err := json.MarshalIndent(runsList, "", " ")
		if err != nil {
			logger.Error(err.Error(), "meth", meth)
			os.Exit(1)
		}
		fmt.Println(string(content))
	} else {
		err = fmt.Errorf("Invalid output format %s. Valid formats are 'table' and 'json'", output)
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}

}

func prettyPrintReschedulerRunsList(runsList *rescheduler.RunsList) {
	fmt.Println()
	runsData := generateReschedulerRunsData(runsList)
	runsTable := tablewriter.NewWriter(os.Stdout)
	runsTable.SetHeader([]string{"Started", "Duration", "Status"})
	runsTable.SetBorder(true)
	runsTable.AppendBulk(runsData)
	runsTable.SetAlignment(tablewriter.ALIGN_CENTER)
	runsTable.Render()
	fmt.Println()
	return
}

func generateReschedulerRunsData(runsList *rescheduler.RunsList) (runsData [][]string) {
	meth := "rescheduler.generateReschedulerRunsData()"

	for _, reschedulerRun := range *runsList {
		startTime, err := time.Parse(time.RFC3339, reschedulerRun.StartTime)
		if err != nil {
			logger.Error(err.Error(), "meth", meth)
			return
		}
		startTimeStr := startTime.Format("2006.01.02 15:04:05")

		var endTime time.Time
		if reschedulerRun.CompletionTime == nil {
			endTime = time.Now()
		} else {
			endTime, err = time.Parse(time.RFC3339, *reschedulerRun.CompletionTime)
			if err != nil {
				logger.Error(err.Error(), "meth", meth)
				return
			}
		}
		runDuration := endTime.Sub(startTime).Truncate(time.Second)
		runDurationStr := runDuration.String()

		runRow := []string{
			startTimeStr,
			runDurationStr,
			reschedulerRun.Status,
		}
		runsData = append(runsData, runRow)
	}
	return
}
