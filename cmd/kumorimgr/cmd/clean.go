/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package cmd

import (
	"cluster-manager/pkg/helper"
	"cluster-manager/pkg/logger"
	"cluster-manager/pkg/types"
	"fmt"
	"os"

	"github.com/spf13/cobra"
)

// Clean command cleans a PREVIOUSLY REMOVED from cluster node
// Its just for test/develop purposes

var nodeIP string

// cleanCmd represents the clean command
var cleanCmd = &cobra.Command{
	Use:   "clean <all|master|worker>",
	Short: "Clean all nodes or specific node",
	Long: `
Cleans a node, so that it is ready to be incorporated again to a cluster.
If all the nodes are cleaned, then all of them are ready to be incorporated in a new cluster.
If a particular node is to be cleaned, it must have been removed from the cluster previously.

This command can be used for tests purposes.
For example, a node can be excluded from the cluster (using the update command), cleaned (using the clean command), and then incorporated back into the cluster (using the update command again).
Or all the nodes in the cluster can be cleaned, with the purpose of creating it again.`,
	ValidArgs: []string{"all", "master", "worker"},
	Args:      cobra.ExactValidArgs(1),
	Run:       func(cmd *cobra.Command, args []string) { runClean(cmd, args) },
}

func init() {
	rootCmd.AddCommand(cleanCmd)
	cleanCmd.Flags().StringVar(
		&nodeIP, "nodeIP", "",
		"Node IP, when a specific node is to be cleaned",
	)
	cleanCmd.Flags().BoolVar(&skipScriptsCheck, "skip-scripts-check", false, "skip validation of local scripts repository")
}

func runClean(cmd *cobra.Command, args []string) {
	meth := "clean.runClean()"
	logger.Info("Cleaning nodes", "meth", meth)

	cleanType := args[0] // all | master | worker
	if cleanType != "all" {
		if nodeIP == "" {
			logger.Error("Flag --nodeIP is mandatory", "meth", meth)
			os.Exit(1)
		}
	}

	// Validate local Scripts repository
	if !skipScriptsCheck {
		err := checkScriptsRepository()
		if err != nil {
			msg := fmt.Sprintf("Local scripts repository validation failed. This validation can be by-passed with the '--skip-scripts-check' flag.")
			fmt.Println(helper.ToRed(msg))
			err = fmt.Errorf("Validation error: : %s", err.Error())
			logger.Error(err.Error())
			os.Exit(1)
		}
	} else {
		logger.Debug("Scripts local repository check skipped.")
	}

	// Get cluster configuration
	cfgDir := "./cluster"
	cluster, err := getClusterConfiguration(cfgDir)
	if err != nil {
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}

	// TODO : decoupling from the script structure
	// Generates /scripts/scripts/cleaner-hard/variables.sh, used by the scripts
	logger.Debug(
		"Generating variables for scripts",
		"content", cluster, "meth", meth,
	)
	varsFile := "./scripts/scripts/cleaner-hard/variables.sh"
	if cleanType == "all" {
		prepareVarsFileAll(varsFile, cluster)
	} else {
		prepareVarsFileNode(cleanType, nodeIP, varsFile, cluster)
	}

	// Launch scripts
	if cleanType == "all" {
		err = launchScript("./scripts/scripts/cleaner-hard/cleaner-hard.sh", "")
	} else {
		err = launchScript("./scripts/scripts/cleaner-hard/cleaner-hard.sh", "exclude-supermaster")
	}
	if err != nil {
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}
}

// Generates /scripts/scripts/cleaner-hard/variables.sh, taking into account all nodes
func prepareVarsFileAll(
	varsFile string, config *types.ClusterAndDistribution,
) (
	err error,
) {
	err = prepareVarsFile(varsFile, config)
	return
}

// Generates /scripts/scripts/cleaner-hard/variables.sh, taking into account one node
func prepareVarsFileNode(
	nodeType string, nodeIP string, varsFile string, config *types.ClusterAndDistribution,
) (
	err error,
) {
	// Modify cluster configuration, to force to have just the specific node
	// and the supermaster (that will not be cleaned)
	machines := []types.Machine{}
	for _, m := range config.Cluster.Machines {
		if m.IsRole("supermaster") {
			machines = append(machines, m)
		}
	}
	node := types.Machine{
		IP: nodeIP,
		Labels: []types.Label{
			{
				Noderole: nodeType,
			},
		},
	}
	machines = append(machines, node)
	config.Cluster.Machines = machines
	err = prepareVarsFile(varsFile, config)
	return
}
