/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package cmd

import (
	"cluster-manager/pkg/logger"
	"cluster-manager/pkg/users"
	"fmt"
	"os"
	"strconv"
	"strings"

	"github.com/olekukonko/tablewriter"
	"github.com/spf13/cobra"
)

// listCmd represents the list command
var usersListCmd = &cobra.Command{
	Use:   "list",
	Short: "List the users on the platform.",
	Long:  `List the users on the platform.`,
	Args:  cobra.NoArgs,
	Run:   func(cmd *cobra.Command, args []string) { runUsersList(cmd, args) },
}

func init() {
	usersCmd.AddCommand(usersListCmd)
	usersListCmd.Flags().StringP(
		"output", "o", "table",
		"Output format: table or json",
	)
}

func runUsersList(cmd *cobra.Command, args []string) {
	meth := "runUsersList"

	logger.Debug(
		"Listing users",
		"meth", meth,
	)

	output, err := cmd.Flags().GetString("output")
	if err != nil {
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}

	usersJSON, usersErr := users.GetUsers()
	if usersErr != nil {
		fmt.Println("Error listing users: " + usersErr.Error())
		return
	}

	if output == "table" {
		usersList, err := users.NewUsersListFromJSON(usersJSON.String(), true)
		if err != nil {
			logger.Error(err.Error(), "meth", meth)
			os.Exit(1)
		}
		prettyPrintUsersList(usersList)
	} else if output == "json" {
		fmt.Println(usersJSON.StringIndent("", "  "))
	} else {
		err = fmt.Errorf("Invalid output format %s. Valid formats are 'table' and 'json'", output)
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}
}

func prettyPrintUsersList(usersList *users.UsersList) {
	fmt.Println()
	usersData := generateUsersData(usersList)
	usersTable := tablewriter.NewWriter(os.Stdout)
	usersTable.SetHeader([]string{"UserName", "Email", "FirstName", "LastName",
		"Enabled", "Groups"})
	usersTable.SetBorder(true)
	usersTable.AppendBulk(usersData)
	usersTable.SetAlignment(tablewriter.ALIGN_CENTER)
	usersTable.Render()
	fmt.Println()
	return
}

func generateUsersData(usersList *users.UsersList) (usersData [][]string) {
	for _, user := range *usersList {
		userRow := []string{
			user.UserName,
			user.Email,
			user.FirstName,
			user.LastName,
			strconv.FormatBool(user.Enabled),
			strings.Join(user.Groups, ","),
		}
		usersData = append(usersData, userRow)
	}
	return
}
