/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package cmd

import (
	"cluster-manager/pkg/admission"
	"cluster-manager/pkg/logger"
	"cluster-manager/pkg/viper"
	"fmt"
	"os"
	goruntime "runtime"
	"strings"

	"github.com/spf13/cobra"

	homedir "github.com/mitchellh/go-homedir"
)

var cfgFile string
var logLevelFlag string
var home string

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "kumorimgr",
	Short: "kumorimgr allows create and manage Kumori clusters",
	Long: `
kumorimgr uses a cluster and distribution definition to create and manage a Kumori cluster.`,
	Version: KumoriMgrVersion + " " + goruntime.GOOS + "/" + goruntime.GOARCH,
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	meth := "root.Execute()"
	logger.SetLevel("fatal") // Later (initConfig function) will be configured
	if err := rootCmd.Execute(); err != nil {
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)

	rootCmd.PersistentFlags().StringVar(
		&logLevelFlag, "log-level", "",
		"log level (values debug, info, warn, error)",
	)

	rootCmd.PersistentFlags().StringVar(
		&cfgFile, "config", "",
		"config file (by default looks for ./.kumori/kumorimgr.json or $HOME/.kumori/kumorimgr.json)",
	)
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	meth := "root.initConfig()"
	if cfgFile != "" {
		// Use config file from the flag.
		viper.Global.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		var err error
		home, err = homedir.Dir()
		if err != nil {
			logger.Error(err.Error(), "meth", meth)
			os.Exit(1)
		}
		// Search config in home directory with name ".kumori/kumorimgr" (without extension).
		viper.Global.AddConfigPath("./.kumori")
		viper.Global.AddConfigPath(home + "/.kumori")
		viper.Global.SetConfigName("kumorimgr")
	}

	viper.Global.AutomaticEnv() // read in environment variables that match

	err := viper.Global.ReadInConfig()
	if err != nil {
		if strings.Contains(err.Error(), "Not Found") {
			// Config file doesn't exist: create a new one with default values in
			// current directory.
			viper.Global.SetConfigFile("./.kumori/kumorimgr.json")
			writeDefaultConfigFile()
			fmt.Println("Created new configuration file in ./.kumori/kumorimgr.json")
			err = viper.Global.ReadInConfig()
		}
		if err != nil {
			logger.Error(err.Error())
			os.Exit(1)
		}
	}

	// Check version of configuration file
	version := viper.Global.GetString("config-version")
	if version != AppConfigVersion {
		logger.Error(
			"Invalid app-config version",
			"versions", version+"/"+AppConfigVersion, "meth", meth,
		)
		os.Exit(1)
	}

	// Set loglevel
	logLevel := logLevelFlag
	if logLevel == "" {
		logLevel = viper.Global.GetString("log-level")
	}
	logger.SetLevel(logLevel)

	// Check if admission token has expired, and refresh it
	refreshed, err := admission.RefreshTokenIfNeeded()
	if err != nil {
		logger.Error("Refreshing token: "+err.Error(), "meth", meth)
		// Ticket 1016: we remove the following exit statement to allow execution of the 'logout'
		// command even if the refresh token is invlaid/corrupted.
		// os.Exit(1)
	}
	if refreshed {
		logger.Info("Tokens refreshed", "meth", meth)
		fmt.Println("Token refreshed")
	}

	logger.Debug("Using config file", "file", viper.Global.ConfigFileUsed(), "meth", meth)
}
