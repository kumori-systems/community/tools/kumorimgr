/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package cmd

import (
	"cluster-manager/pkg/admission"
	"cluster-manager/pkg/helper"
	"cluster-manager/pkg/logger"
	"cluster-manager/pkg/viper"
	"fmt"
	"os"

	"github.com/spf13/cobra"
)

// loginCmd represents the login command
var loginCmd = &cobra.Command{
	Use:   "login <user>",
	Short: "Login to Kumori Platform",
	Long: `
Login to Kumori Platform.

This command interactively asks for a password and authenticates against Admission service to obtain access and refresh tokens, and their expiry dates. This is saved in config file and is used for later interactions with the Platform.

Tokens are dynamically renewed during kumorictl usage. Once refresh token expires, user will need to login again.`,
	Args: cobra.ExactArgs(1),
	Run:  func(cmd *cobra.Command, args []string) { runLogin(cmd, args) },
}

func init() {
	rootCmd.AddCommand(loginCmd)
}

func runLogin(cmd *cobra.Command, args []string) {
	meth := "login.runLogin()"
	logger.Info("Login", "meth", meth)

	admissionAuth := viper.Global.GetString("admission-authentication-type")
	logger.Debug("Authentication", "Method", admissionAuth)

	if admissionAuth != "TokenClientCertAdmissionAuth" {
		logger.Fatal(fmt.Sprintf("Login command cannot be used with %s authentication method", admissionAuth))
	}

	password, err := helper.GetPasswd("Enter Password: ", true)
	if err != nil {
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}
	user := args[0]
	err = admission.Login(user, password)
	if err != nil {
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}
	fmt.Println("Login OK")
}
