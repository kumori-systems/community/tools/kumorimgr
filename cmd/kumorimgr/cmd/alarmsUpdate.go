/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package cmd

import (
	"bufio"
	"cluster-manager/pkg/alarms"
	"cluster-manager/pkg/helper"
	"cluster-manager/pkg/logger"
	"fmt"
	"os"

	"github.com/olekukonko/tablewriter"
	"github.com/spf13/cobra"
)

// alarmsUpdateCmd represents the alarmsUpdate command
var alarmsUpdateCmd = &cobra.Command{
	Use:   "update",
	Short: "Update the alarms on the platform.",
	Long: `Update the alarms on the platform, using the new configuration stored
in the './cluster' directory of the workspace.
The previous (current) alarm configuration is retrieved from the cluster, via
Admission service.`,
	Args: cobra.NoArgs,
	Run:  func(cmd *cobra.Command, args []string) { runAlarmsUpdate(cmd, args) },
}

func init() {
	alarmsCmd.AddCommand(alarmsUpdateCmd)
}

func runAlarmsUpdate(cmd *cobra.Command, args []string) {
	meth := "runAlarmsUpdate"

	logger.Debug(
		"Updating alarms",
		"meth", meth,
	)

	// Get the current alarms configuration from cluster
	currentAlarmsConfig, err := alarms.GetAlarmsConfig()
	if err != nil {
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}

	// Get the current alarms configuration from workspace
	workspaceAlarmsConfig, err := alarms.GetAlarmsConfigFromWorkspace()
	if err != nil {
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}

	// Calculate the differences: elements to remove
	alarmsDefToRemove, alarmsImplToRemove, alarmsToRemove,
		alarmsImplToRemoveByGarbage, alarmsToRemoveByGarbage :=
		GetAlarmsToRemove(currentAlarmsConfig, workspaceAlarmsConfig)

	// Calculate the differences: elements to create
	alarmsDefToCreate, alarmsImplToCreate, alarmsToCreate :=
		GetAlarmsToCreate(currentAlarmsConfig, workspaceAlarmsConfig)

	// Calculate the differences: elements to update
	alarmsDefToUpdate, alarmsImplToUpdate, alarmsToUpdate :=
		GetAlarmsToUpdate(currentAlarmsConfig, workspaceAlarmsConfig)

	// No changes detected
	if len(alarmsDefToRemove) == 0 &&
		len(alarmsImplToRemove) == 0 &&
		len(alarmsToRemove) == 0 &&
		len(alarmsImplToRemoveByGarbage) == 0 &&
		len(alarmsToRemoveByGarbage) == 0 &&
		len(alarmsDefToCreate) == 0 &&
		len(alarmsImplToCreate) == 0 &&
		len(alarmsToCreate) == 0 &&
		len(alarmsDefToUpdate) == 0 &&
		len(alarmsImplToUpdate) == 0 &&
		len(alarmsToUpdate) == 0 {
		fmt.Println("No changes detected. Exiting")
		fmt.Println("")
		os.Exit(0)
	}

	// Show differences and allow abort operation
	fmt.Println("")
	ShowAlarmDifferences(
		alarmsDefToRemove, alarmsImplToRemove, alarmsToRemove,
		alarmsImplToRemoveByGarbage, alarmsToRemoveByGarbage,
		alarmsDefToCreate, alarmsImplToCreate, alarmsToCreate,
		alarmsDefToUpdate, alarmsImplToUpdate, alarmsToUpdate,
	)

	// Allow the user abort the operation
	reader := bufio.NewReader(os.Stdin)
	fmt.Println("")
	fmt.Print("Press <ENTER> to continue or <CTRL-c> to abort")
	_, _ = reader.ReadString('\n')

	// Apply changes
	RemoveAlarms(alarmsToRemove)
	RemoveAlarmImpls(alarmsImplToRemove)
	RemoveAlarmDefs(alarmsDefToRemove) // Definitions removed the last
	UpdateAlarmDefs(alarmsDefToUpdate) // Definitions updated the first
	UpdateAlarmImpls(alarmsImplToUpdate)
	UpdateAlarms(alarmsToUpdate)
	CreateAlarmDefs(alarmsDefToCreate) // Definitions created the first
	CreateAlarmImpls(alarmsImplToCreate)
	CreateAlarms(alarmsToCreate)
}

// GetAlarmsToRemove calculate the elements to remove
// The function take into account that a removed alarmdefinition will remove
// automatically dependent alarms and alarmimplementations, in the
// "alarmsImplToRemoveByGarbage" and "alamsToRemoveByGarbage" lists
func GetAlarmsToRemove(
	currentAlarmsConfig alarms.AlarmsConfig,
	workspaceAlarmsConfig alarms.AlarmsConfig,
) (
	alarmsDefToRemove []alarms.AlarmDefinition,
	alarmsImplToRemove map[string][]alarms.IAlarmImpl,
	alarmsToRemove []alarms.Alarm,
	alarmsImplToRemoveByGarbage map[string][]alarms.IAlarmImpl,
	alarmsToRemoveByGarbage []alarms.Alarm,

) {

	for _, v := range currentAlarmsConfig.AlarmDefinitions {
		exists, _ := workspaceAlarmsConfig.GetDefinition(v.Name)
		if !exists {
			alarmsDefToRemove = append(alarmsDefToRemove, v)
		}
	}

	alarmsImplToRemove = map[string][]alarms.IAlarmImpl{}
	alarmsImplToRemoveByGarbage = map[string][]alarms.IAlarmImpl{}
	for implType, implList := range currentAlarmsConfig.AlarmImpls {
		for _, v := range implList {
			exists, _ := workspaceAlarmsConfig.GetImplementation(v.GetName(), implType)
			if !exists {
				alarmsImplToRemove[implType] = append(alarmsImplToRemove[implType], v)
			} else if alarms.ContainsAlarmDef(v.GetAlarmDefinition(), alarmsDefToRemove) {
				alarmsImplToRemoveByGarbage[implType] = append(alarmsImplToRemoveByGarbage[implType], v)
			}
		}
	}

	for _, v := range currentAlarmsConfig.Alarms {
		exists, _ := workspaceAlarmsConfig.GetAlarm(v.Name)
		if !exists {
			alarmsToRemove = append(alarmsToRemove, v)
		} else if alarms.ContainsAlarmDef(v.AlarmDefinition, alarmsDefToRemove) {
			alarmsToRemoveByGarbage = append(alarmsToRemoveByGarbage, v)
		}
	}

	return
}

// GetAlarmsToCreate calculate the elements to create
func GetAlarmsToCreate(
	currentAlarmsConfig alarms.AlarmsConfig,
	workspaceAlarmsConfig alarms.AlarmsConfig,
) (
	alarmsDefToCreate []alarms.AlarmDefinition,
	alarmsImplToCreate map[string][]alarms.IAlarmImpl,
	alarmsToCreate []alarms.Alarm,
) {
	for _, v := range workspaceAlarmsConfig.AlarmDefinitions {
		exists, _ := currentAlarmsConfig.GetDefinition(v.Name)
		if !exists {
			alarmsDefToCreate = append(alarmsDefToCreate, v)
		}
	}
	alarmsImplToCreate = map[string][]alarms.IAlarmImpl{}
	for implType, implList := range workspaceAlarmsConfig.AlarmImpls {
		for _, v := range implList {
			exists, _ := currentAlarmsConfig.GetImplementation(v.GetName(), implType)
			if !exists {
				alarmsImplToCreate[implType] = append(alarmsImplToCreate[implType], v)
			}
		}
	}
	for _, v := range workspaceAlarmsConfig.Alarms {
		exists, _ := currentAlarmsConfig.GetAlarm(v.Name)
		if !exists {
			alarmsToCreate = append(alarmsToCreate, v)
		}
	}
	return
}

// GetAlarmsToUpdate calculate the elements to update
func GetAlarmsToUpdate(
	currentAlarmsConfig alarms.AlarmsConfig,
	workspaceAlarmsConfig alarms.AlarmsConfig,
) (
	alarmsDefToUpdate []alarms.AlarmDefinition,
	alarmsImplToUpdate map[string][]alarms.IAlarmImpl,
	alarmsToUpdate []alarms.Alarm,
) {
	for _, v1 := range workspaceAlarmsConfig.AlarmDefinitions {
		exists, v2 := currentAlarmsConfig.GetDefinition(v1.Name)
		if exists && !v1.IsEqual(v2) {
			alarmsDefToUpdate = append(alarmsDefToUpdate, v1)
		}
	}
	alarmsImplToUpdate = map[string][]alarms.IAlarmImpl{}
	for implType, implList := range workspaceAlarmsConfig.AlarmImpls {
		for _, v1 := range implList {
			exists, v2 := currentAlarmsConfig.GetImplementation(v1.GetName(), implType)
			if exists && !alarms.AlarmImplIsEqual(v1, v2, implType) {
				alarmsImplToUpdate[implType] = append(alarmsImplToUpdate[implType], v1)
			}
		}
	}
	for _, v1 := range workspaceAlarmsConfig.Alarms {
		exists, v2 := currentAlarmsConfig.GetAlarm(v1.Name)
		if exists && !v1.IsEqual(v2) {
			alarmsToUpdate = append(alarmsToUpdate, v1)
		}
	}
	return
}

// ShowAlarmDifferences prints the changes that will be applied
func ShowAlarmDifferences(
	alarmsDefToRemove []alarms.AlarmDefinition,
	alarmsImplToRemove map[string][]alarms.IAlarmImpl,
	alarmsToRemove []alarms.Alarm,
	alarmsImplToRemoveByGarbage map[string][]alarms.IAlarmImpl,
	alarmsToRemoveByGarbage []alarms.Alarm,
	alarmsDefToCreate []alarms.AlarmDefinition,
	alarmsImplToCreate map[string][]alarms.IAlarmImpl,
	alarmsToCreate []alarms.Alarm,
	alarmsDefToUpdate []alarms.AlarmDefinition,
	alarmsImplToUpdate map[string][]alarms.IAlarmImpl,
	alarmsToUpdate []alarms.Alarm,
) {

	rowDefs := []string{helper.ToBold("DEFINITIONS")}
	aux := ""
	for _, v := range alarmsDefToRemove {
		aux = aux + v.Name + "\n"
	}
	rowDefs = append(rowDefs, aux)
	aux = ""
	for _, v := range alarmsDefToUpdate {
		aux = aux + v.Name + "\n"
	}
	rowDefs = append(rowDefs, aux)
	aux = ""
	for _, v := range alarmsDefToCreate {
		aux = aux + v.Name + "\n"
	}
	rowDefs = append(rowDefs, aux)

	rowImpls := []string{helper.ToBold("IMPLEMENTATIONS")}
	aux = ""
	for _, implList := range alarmsImplToRemove {
		for _, v := range implList {
			aux = aux + v.GetName() + "\n"
		}
	}
	for _, implList := range alarmsImplToRemoveByGarbage {
		for _, v := range implList {
			aux = aux + helper.ToRed(v.GetName()+"*") + "\n"
		}
	}
	rowImpls = append(rowImpls, aux)
	aux = ""
	for _, implList := range alarmsImplToUpdate {
		for _, v := range implList {
			aux = aux + v.GetName() + "\n"
		}
	}
	rowImpls = append(rowImpls, aux)
	aux = ""
	for _, implList := range alarmsImplToCreate {
		for _, v := range implList {
			aux = aux + v.GetName() + "\n"
		}
	}
	rowImpls = append(rowImpls, aux)

	rowAlarms := []string{helper.ToBold("ALARMS")}
	aux = ""
	for _, v := range alarmsToRemove {
		aux = aux + v.Name + "\n"
	}
	for _, v := range alarmsToRemoveByGarbage {
		aux = aux + helper.ToRed(v.Name+"*") + "\n"
	}
	rowAlarms = append(rowAlarms, aux)
	aux = ""
	for _, v := range alarmsToUpdate {
		aux = aux + v.Name + "\n"
	}
	rowAlarms = append(rowAlarms, aux)
	aux = ""
	for _, v := range alarmsToCreate {
		aux = aux + v.Name + "\n"
	}
	rowAlarms = append(rowAlarms, aux)

	rows := [][]string{
		rowDefs,
		rowImpls,
		rowAlarms,
	}

	diffTable := tablewriter.NewWriter(os.Stdout)
	diffTable.SetHeader([]string{"ELEMENT", "REMOVE", "UPDATE", "CREATE"})
	diffTable.SetHeaderColor(
		tablewriter.Colors{tablewriter.Bold},
		tablewriter.Colors{tablewriter.Bold, tablewriter.FgRedColor},
		tablewriter.Colors{tablewriter.Bold, tablewriter.FgYellowColor},
		tablewriter.Colors{tablewriter.Bold, tablewriter.FgGreenColor},
	)
	diffTable.SetCenterSeparator("+")
	diffTable.SetColumnSeparator("|")
	diffTable.SetRowLine(true)
	diffTable.SetRowSeparator("-")
	diffTable.SetHeaderLine(true)
	diffTable.SetBorder(true)
	diffTable.SetNoWhiteSpace(false)
	diffTable.SetAlignment(tablewriter.ALIGN_LEFT)
	diffTable.AppendBulk(rows)
	diffTable.Render()

	if len(alarmsImplToRemoveByGarbage) > 0 ||
		len(alarmsToRemoveByGarbage) > 0 {
		fmt.Println()
		fmt.Println(helper.ToRed("* Will bemoved because its AlarmDefinition will be removed"))
	}
}

func RemoveAlarms(alarmsToRemove []alarms.Alarm) {
	meth := "RemoveAlarms"
	for _, v := range alarmsToRemove {
		fmt.Println("Removing alarm", v.Name)
		err := v.Remove()
		if err != nil {
			fmt.Println(helper.ToRed("Error removing alarm: " + err.Error()))
			logger.Warn(err.Error(), "meth", meth)
		}
	}
}

func RemoveAlarmImpls(alarmsImplToRemove map[string][]alarms.IAlarmImpl) {
	meth := "RemoveAlarmImpls"
	for implType, implList := range alarmsImplToRemove {
		for _, v := range implList {
			fmt.Println("Removing alarm implementation", implType, v.GetName())
			err := alarms.AlarmImplRemove(v, implType)
			if err != nil {
				fmt.Println(helper.ToRed("Error removing alarm implementation: " + err.Error()))
				logger.Warn(err.Error(), "meth", meth)
			}
		}
	}
}

func RemoveAlarmDefs(alarmsDefToRemove []alarms.AlarmDefinition) {
	meth := "RemoveAlarmDefs"
	for _, v := range alarmsDefToRemove {
		fmt.Println("Removing alarm definition", v.Name)
		err := v.Remove()
		if err != nil {
			fmt.Println(helper.ToRed("Error removing alarm definition: " + err.Error()))
			logger.Warn(err.Error(), "meth", meth)
		}
	}
}

func UpdateAlarms(alarmsToUpdate []alarms.Alarm) {
	meth := "UpdateAlarms"
	for _, v := range alarmsToUpdate {
		fmt.Println("Updating alarm", v.Name)
		err := v.Update()
		if err != nil {
			fmt.Println(helper.ToRed("Error updating alarm: " + err.Error()))
			logger.Warn(err.Error(), "meth", meth)
		}
	}
}

func UpdateAlarmImpls(alarmsImplToUpdate map[string][]alarms.IAlarmImpl) {
	meth := "UpdateAlarmImpls"
	for implType, implList := range alarmsImplToUpdate {
		for _, v := range implList {
			fmt.Println("Updating alarm implementation", implType, v.GetName())
			err := alarms.AlarmImplUpdate(v, implType)
			if err != nil {
				fmt.Println(helper.ToRed("Error updating alarm implementation: " + err.Error()))
				logger.Warn(err.Error(), "meth", meth)
			}
		}
	}
}

func UpdateAlarmDefs(alarmsDefToUpdate []alarms.AlarmDefinition) {
	meth := "UpdateAlarmDefs"
	for _, v := range alarmsDefToUpdate {
		fmt.Println("Updating alarm definition", v.Name)
		err := v.Update()
		if err != nil {
			fmt.Println(helper.ToRed("Error updating alarm definition: " + err.Error()))
			logger.Warn(err.Error(), "meth", meth)
		}
	}
}

func CreateAlarms(alarmsToCreate []alarms.Alarm) {
	meth := "CreateAlarms"
	for _, v := range alarmsToCreate {
		fmt.Println("Creating alarm", v.Name)
		err := v.Create()
		if err != nil {
			fmt.Println(helper.ToRed("Error creating alarm: " + err.Error()))
			logger.Warn(err.Error(), "meth", meth)
		}
	}
}

func CreateAlarmImpls(alarmsImplToCreate map[string][]alarms.IAlarmImpl) {
	meth := "CreateAlarmImpls"
	for implType, implList := range alarmsImplToCreate {
		for _, v := range implList {
			fmt.Println("Creating alarm implementation", implType, v.GetName())
			err := alarms.AlarmImplCreate(v, implType)
			if err != nil {
				fmt.Println(helper.ToRed("Error creating alarm implementation: " + err.Error()))
				logger.Warn(err.Error(), "meth", meth)
			}
		}
	}
}

func CreateAlarmDefs(alarmsDefToCreate []alarms.AlarmDefinition) {
	meth := "CreateAlarmDefs"
	for _, v := range alarmsDefToCreate {
		fmt.Println("Creating alarm definition", v.Name)
		err := v.Create()
		if err != nil {
			fmt.Println(helper.ToRed("Error creating alarm definition: " + err.Error()))
			logger.Warn(err.Error(), "meth", meth)
		}
	}
}
