/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package cmd

import (
	"cluster-manager/pkg/cue2json"
	"cluster-manager/pkg/git"
	"cluster-manager/pkg/logger"
	"cluster-manager/pkg/tmpl"
	"cluster-manager/pkg/types"
	"cluster-manager/pkg/viper"
	"encoding/base64"
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"strconv"
	"strings"
	"text/template"
)

func getClusterConfiguration(
	cfgDir string,
) (
	config *types.ClusterAndDistribution, err error,
) {
	meth := "common.getClusterConfiguration()"
	logger.Info("Generating cluster configuration", "cfgDir", cfgDir, "meth", meth)

	cfgDir, err = filepath.Abs(cfgDir)
	if err != nil {
		return
	}

	// Compile cue
	logger.Info("Compiling CUE files", "meth", meth)
	err = cue2json.ExportAll(cfgDir, cfgDir+"/all.json")
	if err != nil {
		return
	}

	// Load json into struct
	logger.Debug("Loading json files into structs", "meth", meth)
	config, err = types.NewClusterAndDistributionFromFile(cfgDir + "/all.json")
	if err != nil {
		return
	}

	return
}

// clusterContains returns true if cluster contains provided machina
// BE CAREFUL: just IP is checked!
func clusterContains(cluster types.Cluster, machine types.Machine) bool {
	return machineListContains(cluster.Machines, machine)
}

// machineListContains returns true if the provided machine list contains
// provided machine
// BE CAREFUL: just IP is checked!
func machineListContains(machineList []types.Machine, machine types.Machine) bool {
	for _, m := range machineList {
		if m.IP == machine.IP {
			return true
		}
	}
	return false
}

// TODO : decoupling from the script structure
// Launch scripts
func launchScript(fullPathScriptFile string, param string) (err error) {
	meth := "common.launchScript()"
	logger.Info("Launching scripts", "meth", meth, "script", fullPathScriptFile)
	fmt.Println("Launching scripts")
	env := os.Environ()
	path, err := filepath.Abs(fullPathScriptFile)
	if err != nil {
		return
	}
	var cmd *exec.Cmd
	if param != "" {
		//err = syscall.Exec(path, []string{param}, env) --> not working for shell scripts + parameters
		cmd = exec.Command(path, param)
	} else {
		//err = syscall.Exec(path, []string{}, env)
		cmd = exec.Command(path)
	}
	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	cmd.Env = env
	err = cmd.Run()

	if err != nil {
		return
	}
	return
}

// TODO : decoupling from the script structure
// Generates /scripts/scripts/XXX/variables.sh, taking into account all nodes
func prepareVarsFile(varsFile string, config *types.ClusterAndDistribution) (err error) {
	t := template.Must(template.New("scripts").Parse(tmpl.ScriptsVarsTemplate))
	outputFile, err := os.Create(varsFile)
	if err != nil {
		return
	}
	err = t.Execute(outputFile, config)
	if err != nil {
		return
	}
	outputFile.Close()
	return
}

// Generates a file with bash var representing the Distribution software versions
func prepareDistributionVersionsVarsFile(varsFile string, config *types.ClusterAndDistribution) (err error) {
	t := template.Must(template.New("scripts").Parse(tmpl.DistributionVersionsVarsTemplate))
	outputFile, err := os.Create(varsFile)
	if err != nil {
		return
	}
	err = t.Execute(outputFile, config)
	if err != nil {
		return
	}
	outputFile.Close()
	return
}

// Generates a file with bash vars representing a Cluster configuration
func prepareClusterConfigVarsFile(varsFile string, config *types.ClusterAndDistribution) (err error) {
	t := template.Must(template.New("scripts").Parse(tmpl.ClusterConfigVarsTemplate))
	outputFile, err := os.Create(varsFile)
	if err != nil {
		return
	}
	err = t.Execute(outputFile, config)
	if err != nil {
		return
	}
	outputFile.Close()
	return
}

// Generates a file with bash vars representing a Cluster Addons configuration
func prepareClusterAddonsConfigVarsFile(varsFile string, config *types.ClusterAndDistribution) (err error) {
	t := template.Must(template.New("scripts").Parse(tmpl.ClusterAddonsConfigVarsTemplate))
	outputFile, err := os.Create(varsFile)
	if err != nil {
		return
	}
	err = t.Execute(outputFile, config)
	if err != nil {
		return
	}
	outputFile.Close()
	return
}

func checkScriptsRepository() (err error) {
	meth := "checkScriptsRepository"

	// Determine the currently initialized distribution name (stored in config file)
	if !viper.Global.IsSet("current-distribution") {
		err := fmt.Errorf("Unable to determine currently initialized distribution name.")
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}
	distributionName := viper.Global.GetString("current-distribution")
	logger.Debug("Currently initialized distribution: " + distributionName)

	// Get local Scripts path and tag
	scriptsURL := viper.Global.GetString("cluster-configuration::" + distributionName + "::scripts-url")
	parts := strings.Split(scriptsURL, "#")
	if len(parts) != 2 {
		err = fmt.Errorf("Wrong scripts URL format in %s", scriptsURL)
		return err
	}
	scriptRepoPath := "./scripts"
	scriptRepoVersion := parts[1]
	logger.Debug("Checking local scripts repository (version " + scriptRepoVersion + ") at " + scriptRepoPath + "...")

	// Check repository is clean and in the expected tag
	err = git.CheckRepository(scriptRepoPath, scriptRepoVersion)
	return err
}

func NewCredentialsFromLicense(entryName string) (creds map[string]*git.Credentials, err error) {
	if !viper.Global.IsSet(entryName + "::license") {
		// Credentials is nil
		return
	}

	licenseToken := viper.Global.GetString(entryName + "::license::token")

	// A valid license must include a username and a token
	if licenseToken == "" {
		err = fmt.Errorf("License information is missing.")
		return
	}

	// Decode license token
	licenseData, err := base64.StdEncoding.DecodeString(licenseToken)
	if err != nil {
		logger.Debug("Error processing license information.", "err", err.Error())
		err = fmt.Errorf("Error processing license information. Invalid token (unable to decode).")
		return
	}

	// Extract usernames and tokens from license data
	var distributionUsername string
	var distributionToken string
	var scriptsUsername string
	var scriptsToken string
	var controllersUsername string
	var controllersToken string

	// A license token is the concatenation of three sets of credentials:
	//
	//   1) for cloning Enterprise distribution repository
	//   2) for cloning Enterprise scripts repository
	//   3) Enterprise private registry credentials
	//
	// The token format is: <username1>:<token1>:<username2>:<token2>:<username3>:<token3>
	//
	licenseTokens := strings.Split(string(licenseData), ":")
	logger.Debug("Decoded license tokens. Found " + strconv.Itoa(len(licenseTokens)) + " tokens.")

	if len(licenseTokens) == 2 {
		// If license contains only one username and token, use it for all credentials (for example a personal token)
		distributionUsername = licenseTokens[0]
		scriptsUsername = licenseTokens[0]
		controllersUsername = licenseTokens[0]
		distributionToken = licenseTokens[1]
		scriptsToken = licenseTokens[1]
		controllersToken = licenseTokens[1]
	} else if len(licenseTokens) == 6 {
		// If license contains 6 elements, use one username and token for each credential
		distributionUsername = licenseTokens[0]
		distributionToken = licenseTokens[1]
		scriptsUsername = licenseTokens[2]
		scriptsToken = licenseTokens[3]
		controllersUsername = licenseTokens[4]
		controllersToken = licenseTokens[5]
	} else {
		err = fmt.Errorf("Error processing license information. Invalid number of credentials (expected 2 or 6, got %d).", len(licenseTokens))
		return
	}

	creds = map[string]*git.Credentials{}

	creds["distribution"] = &git.Credentials{
		CredType: "token",
		Username: distributionUsername,
		Token:    &distributionToken,
	}

	creds["scripts"] = &git.Credentials{
		CredType: "token",
		Username: scriptsUsername,
		Token:    &scriptsToken,
	}

	creds["controllers"] = &git.Credentials{
		CredType: "token",
		Username: controllersUsername,
		Token:    &controllersToken,
	}

	return
}
