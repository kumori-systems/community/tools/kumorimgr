/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package cmd

import (
	"cluster-manager/pkg/cue2json"
	"cluster-manager/pkg/logger"
	"cluster-manager/pkg/types"
	"fmt"
	"os"

	"github.com/spf13/cobra"
	cuedm "gitlab.com/kumori-systems/community/libraries/cue-dependency-manager"
)

// Create-Local command:
// - Compiles cue project
// - Generates variables.sh file expected by the installation scripts, from template
// - Run local (dev) installer scripts

// generateVersionsFileCmd represents the create command
var generateVersionsFileCmd = &cobra.Command{
	Use:   "generate-versions-file",
	Short: `Create a file containing bash variables representing all the Distribution software versions.`,
	Long: `
Using the configuration stored in the distribution directory, it creates a file containing bash variables representing all the Distribution software versions.

It is required to have installed the CUE utility (https://cuelang.org/docs/install)`,
	Run: func(cmd *cobra.Command, args []string) { runGenerateVersionsFile(cmd, args) },
}

func init() {
	rootCmd.AddCommand(generateVersionsFileCmd)
	generateVersionsFileCmd.Flags().StringP("distribution-dir", "d", "./cluster", "Directory where the cluster distribution files are")
}

func runGenerateVersionsFile(cmd *cobra.Command, args []string) {
	meth := "create.runGenerateVersionsFile()"
	distributionDir, err := cmd.Flags().GetString("distribution-dir")
	if err != nil {
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}
	logger.Info("Creating distribution versions file from directory "+distributionDir, "meth", meth)

	// Resolve dependencies of the cluster (cue) directory, using git
	logger.Debug("Resolving cue dependencies (kumori-model)", "meth", meth)
	ctx, err := cuedm.Resolve(distributionDir)
	if err != nil {
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}
	for _, module := range ctx.CUEModules {
		logger.Debug(
			"Cluster-Dependency",
			"Module", module.Name,
			"Resolved", module.Resolved,
			"meth", meth,
		)
	}

	// Compiling cue directories
	logger.Debug("Compiling CUE files", "meth", meth)
	err = cue2json.ExportAll(distributionDir, "./all.json")
	if err != nil {
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}
	fmt.Println("CUE files compiled")

	// Load json into structs, and add inline parameters to it
	logger.Debug("Loading json files into structs", "meth", meth)
	clusterAndDistribution, err := types.NewClusterAndDistributionFromFile(distributionDir + "/all.json")
	if err != nil {
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}

	// Generates a distribution-variables.sh file used by the VM image generator
	logger.Debug(
		"Generating variables for VM image generator",
		"content", clusterAndDistribution, "meth", meth,
	)
	resultFile := "./distribution-variables.sh"
	err = prepareDistributionVersionsVarsFile(resultFile, clusterAndDistribution)
	if err != nil {
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}
}
