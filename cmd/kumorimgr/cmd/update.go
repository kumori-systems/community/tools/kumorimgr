/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package cmd

import (
	"cluster-manager/pkg/helper"
	"cluster-manager/pkg/logger"
	"cluster-manager/pkg/types"
	"cluster-manager/pkg/viper"
	"fmt"
	"os"

	"github.com/spf13/cobra"
)

// updateCmd represents the update command
var updateCmd = &cobra.Command{
	Use:   "update",
	Short: `Update the cluster`,
	Long: `
Update a cluster using the new configuration stored in the './cluster' directory
of the workspace.

The previous (current) configuration directory must also be provided, and can
be specified using the --current-configuration parameter. If no directory is
provided, the default is used ('./cluster-old').

Offline nodes can be removed from the cluster using the --offline-nodes. Before
removing offline nodes, operators should make sure the nodes have been previously
stopped.

Only changes to the list of nodes are processed; any other changes will be ignored.

This command requires CUE to be installed (https://cuelang.org/docs/install)`,
	Run: func(cmd *cobra.Command, args []string) { runUpdate(cmd, args) },
}

func init() {
	rootCmd.AddCommand(updateCmd)
	updateCmd.Flags().StringP(
		"current-configuration", "c", "./cluster-old",
		"Directory with the previous (current) configuration",
	)
	updateCmd.Flags().Bool("offline-nodes", false,
		"instructs Kumorimgr that the nodes to remove are offline (shutdown)")
	updateCmd.Flags().BoolVar(&skipScriptsCheck, "skip-scripts-check", false, "skip validation of local scripts repository")
}

func runUpdate(cmd *cobra.Command, args []string) {
	meth := "update.runUpdate()"

	currentCfgDir, err := cmd.Flags().GetString("current-configuration")
	if err != nil {
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}

	offlineNodes, err := cmd.Flags().GetBool("offline-nodes")
	if err != nil {
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}

	newCfgDir := "./cluster"
	logger.Info(
		"Updating cluster", "currentCfgDir", currentCfgDir, "newCfgDir", newCfgDir,
		"offlineNodes", offlineNodes, "meth", meth,
	)

	// Validate local Scripts repository
	if !skipScriptsCheck {
		err := checkScriptsRepository()
		if err != nil {
			msg := fmt.Sprintf("Local scripts repository validation failed. This validation can be by-passed with the '--skip-scripts-check' flag.")
			fmt.Println(helper.ToRed(msg))
			err = fmt.Errorf("Validation error: : %s", err.Error())
			logger.Error(err.Error())
			os.Exit(1)
		}
	} else {
		logger.Debug("Scripts local repository check skipped.")
	}

	// Determine the currently initialized distribution name (stored in config file)
	if !viper.Global.IsSet("current-distribution") {
		err := fmt.Errorf("Unable to determine currently initialized distribution name.")
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}
	distributionName := viper.Global.GetString("current-distribution")
	logger.Debug("Currently initialized distribution: " + distributionName)

	// Look for Kumori license and extract credentials
	dockerControllersUsername := ""
	dockerControllersPassword := ""

	if distributionName == "enterprise" {
		enterpriseDistribution := "cluster-configuration::enterprise"
		if viper.Global.IsSet(enterpriseDistribution) {
			// Extract credentials from license info
			credsConfig, err := NewCredentialsFromLicense(enterpriseDistribution)
			if err != nil {
				logger.Error(err.Error(), "meth", meth)
				os.Exit(1)
			}
			if credsConfig != nil {
				dockerControllersUsername = credsConfig["controllers"].Username
				dockerControllersPassword = *credsConfig["controllers"].Token
			}
		}
	}

	// Old configuration
	currentCluster, err := getClusterConfiguration(currentCfgDir)
	if err != nil {
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}

	// New configuration
	newCluster, err := getClusterConfiguration(newCfgDir)
	if err != nil {
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}

	// If there are Kumori License credentials, fill them in in both cluster configs
	if dockerControllersUsername != "" && dockerControllersPassword != "" {
		currentCluster.InlineParams.ControllersRegistryUsername = dockerControllersUsername
		currentCluster.InlineParams.ControllersRegistryPassword = dockerControllersPassword
		newCluster.InlineParams.ControllersRegistryUsername = dockerControllersUsername
		newCluster.InlineParams.ControllersRegistryPassword = dockerControllersPassword
	}

	// Calculate node differences between old and new configuration
	logger.Debug("Creating update configuration", "meth", meth)
	updateParams, err := calculateUpdateParams(currentCluster, newCluster)
	if err != nil {
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}
	newCluster.UpdateParams = updateParams

	// TODO : decoupling from the script structure
	// Generates /scripts/scripts/installer/variables.sh, used by the scripts
	logger.Debug(
		"Generating variables for scripts",
		"content", newCluster, "meth", meth,
	)
	resultFile := "./scripts/scripts/updater/variables.sh"
	err = prepareVarsFile(resultFile, newCluster)
	if err != nil {
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}

	// Launch scripts
	if offlineNodes {
		err = launchScript("./scripts/scripts/updater/upd-remove-offline.sh", "")
		if err != nil {
			logger.Error(err.Error(), "meth", meth)
			os.Exit(1)
		}
	} else {
		err = launchScript("./scripts/scripts/updater/updater.sh", "")
		if err != nil {
			logger.Error(err.Error(), "meth", meth)
			os.Exit(1)
		}
	}
}

func calculateUpdateParams(
	oldCluster *types.ClusterAndDistribution, newCluster *types.ClusterAndDistribution,
) (
	updateParams types.UpdateParams, err error,
) {
	updateParams = types.UpdateParams{
		AddMastersIPs:               []string{},
		AddWorkersIPs:               []string{},
		RemoveMastersIPs:            []string{},
		RemoveWorkersIPs:            []string{},
		AddStorageIPs:               []string{},
		RemoveStorageIPs:            []string{},
		AddMaintenanceMastersIPs:    []string{},
		AddMaintenanceWorkersIPs:    []string{},
		RemoveMaintenanceMastersIPs: []string{},
		RemoveMaintenanceWorkersIPs: []string{},
	}

	// Nodes to be removed
	removeMachines, err := clusterSubstraction(oldCluster.Cluster, newCluster.Cluster)
	if err != nil {
		return
	}
	for _, m := range removeMachines {
		if m.IsRole(types.MasterRole) {
			updateParams.RemoveMastersIPs = append(updateParams.RemoveMastersIPs, m.IP)
		} else if m.IsRole(types.WorkerRole) {
			updateParams.RemoveWorkersIPs = append(updateParams.RemoveWorkersIPs, m.IP)
		}
	}

	// Nodes to be added
	addMachines, err := clusterSubstraction(newCluster.Cluster, oldCluster.Cluster)
	if err != nil {
		return
	}
	for _, m := range addMachines {
		if m.IsRole(types.MasterRole) {
			updateParams.AddMastersIPs = append(updateParams.AddMastersIPs, m.IP)
		} else if m.IsRole(types.WorkerRole) {
			updateParams.AddWorkersIPs = append(updateParams.AddWorkersIPs, m.IP)
		}
	}

	// Storage nodes added
	addStorageMachines, err := clusterStorageSubstraction(newCluster.Cluster, oldCluster.Cluster)
	if err != nil {
		return
	}
	for _, m := range addStorageMachines {
		updateParams.AddStorageIPs = append(updateParams.AddStorageIPs, m.IP)
	}

	// Storage nodes removed
	removeStorageMachines, err := clusterStorageSubstraction(oldCluster.Cluster, newCluster.Cluster)
	if err != nil {
		return
	}
	for _, m := range removeStorageMachines {
		updateParams.RemoveStorageIPs = append(updateParams.RemoveStorageIPs, m.IP)
	}

	// Master Nodes to be put into maintenance
	maintenanceAddMasterMachines, err := clusterMaintenanceSubstraction(newCluster.Cluster, oldCluster.Cluster, "master")
	if err != nil {
		return
	}
	for _, m := range maintenanceAddMasterMachines {
		updateParams.AddMaintenanceMastersIPs = append(updateParams.AddMaintenanceMastersIPs, m.IP)
	}

	// Worker Nodes to be put into maintenance
	maintenanceAddWorkerMachines, err := clusterMaintenanceSubstraction(newCluster.Cluster, oldCluster.Cluster, "worker")
	if err != nil {
		return
	}
	for _, m := range maintenanceAddWorkerMachines {
		updateParams.AddMaintenanceWorkersIPs = append(updateParams.AddMaintenanceWorkersIPs, m.IP)
	}

	// Master Nodes to be put out of maintenance
	maintenanceRemoveMasterMachines, err := clusterMaintenanceSubstraction(oldCluster.Cluster, newCluster.Cluster, "master")
	if err != nil {
		return
	}
	for _, m := range maintenanceRemoveMasterMachines {
		updateParams.RemoveMaintenanceMastersIPs = append(updateParams.RemoveMaintenanceMastersIPs, m.IP)
	}

	// Worker Nodes to be put out of maintenance
	maintenanceRemoveWorkerMachines, err := clusterMaintenanceSubstraction(oldCluster.Cluster, newCluster.Cluster, "worker")
	if err != nil {
		return
	}
	for _, m := range maintenanceRemoveWorkerMachines {
		updateParams.RemoveMaintenanceWorkersIPs = append(updateParams.RemoveMaintenanceWorkersIPs, m.IP)
	}

	return
}

// clusterSubstraction returns machines (masters, workers) of clusterA that are
// not in clusterB
func clusterSubstraction(
	clusterA types.Cluster, clusterB types.Cluster,
) (
	machines []types.Machine,
	err error,
) {
	machines = []types.Machine{}
	for _, machine := range clusterA.Machines {
		if !clusterContains(clusterB, machine) {
			machines = append(machines, machine)
		}
	}
	return
}

// clusterStorageSubstraction returns Storage machines of clusterA that are
// not in clusterB
func clusterStorageSubstraction(
	clusterA types.Cluster, clusterB types.Cluster,
) (
	machines []types.Machine,
	err error,
) {
	machines = []types.Machine{}
	for _, machine := range clusterA.Storage.Nodes {
		if !machineListContains(clusterB.Storage.Nodes, machine) {
			machines = append(machines, machine)
		}
	}
	return
}

// clusterSubstraction returns machines of a specific role (masters or workers)
// of clusterA that were in maintenance (label) and are not in maintenance in
// clusterB not in clusterB.
func clusterMaintenanceSubstraction(
	clusterA types.Cluster, clusterB types.Cluster, role string,
) (
	machines []types.Machine,
	err error,
) {
	machines = []types.Machine{}

	var aMaintenance = []types.Machine{}
	var bMaintenance = []types.Machine{}

	// Calculate list of Maintenance nodes in Cluster A
	for _, machine := range clusterA.Machines {
		if machine.IsInMaintenance() && machine.IsRole(role) {
			aMaintenance = append(aMaintenance, machine)
		}
	}

	// Calculate list of Maintenance nodes in Cluster B
	for _, machine := range clusterB.Machines {
		if machine.IsInMaintenance() && machine.IsRole(role) {
			bMaintenance = append(bMaintenance, machine)
		}
	}

	// Calculate difference of Maintenance nodes
	for _, aMachine := range aMaintenance {
		found := false
		for _, bMachine := range bMaintenance {
			if aMachine.IP == bMachine.IP {
				// Machine is also in maintenance in clusterB
				found = true
				break
			}
		}
		if !found {
			// Machine is not in maintenance in ClusterB, check it exist
			if clusterContains(clusterB, aMachine) {
				// Machine exists, so it is a Maintenance state change
				machines = append(machines, aMachine)
			} else {
				// Machine does not exist in clusterB, so it is NOT a Maintenance change
			}
		}
	}
	return
}
