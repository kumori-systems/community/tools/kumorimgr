/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package cmd

import (
	"strings"

	"github.com/spf13/cobra"
)

// usersCmd represents the users command
var usersCmd = &cobra.Command{
	Use:     "users",
	Aliases: []string{"user"},
	Short:   "Basic user management",
}

func init() {
	rootCmd.AddCommand(usersCmd)
}

func prettyUserAPIError(err error) (prettyErr string) {
	// Admission users API returns errors like:
	//   ERROR: <what>: <why>
	// For example:
	//   ERROR: Failed to delete user with username 'user-one': User user-one not found.
	// This function removes the "ERROR: " string
	prettyErr = err.Error()
	if strings.HasPrefix(prettyErr, "ERROR: ") {
		prettyErr = strings.Replace(prettyErr, "ERROR: ", "", 1)
	}
	return
}
