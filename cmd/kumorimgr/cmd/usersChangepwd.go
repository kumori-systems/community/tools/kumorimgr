/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package cmd

import (
	"cluster-manager/pkg/helper"
	"cluster-manager/pkg/logger"
	"cluster-manager/pkg/users"
	"fmt"
	"os"

	"cluster-manager/pkg/viper"

	"github.com/spf13/cobra"
)

// usersChangepwdCmd represents the usersChangepwd command
var usersChangepwdCmd = &cobra.Command{
	Use:   "changepwd <user>",
	Short: "Change user password",
	Long: `Change user password

This command interactively asks for a password. Only administrators can change the password of other users.
If <user> is not provided, current user is used.`,
	Args: cobra.RangeArgs(0, 1),
	Run:  func(cmd *cobra.Command, args []string) { runUsersChangepwd(cmd, args) },
}

func init() {
	usersCmd.AddCommand(usersChangepwdCmd)
}

func runUsersChangepwd(cmd *cobra.Command, args []string) {
	meth := "runUsersChangepwd"
	user, err := getUser(args)
	if err != nil {
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}

	if user == "" {
		logger.Error("Please log in before trying to update password.", "meth", meth)
		os.Exit(1)
	}

	logger.Debug(
		"Changing password",
		"user", user,
		"meth", meth,
	)

	oldPassword, err := helper.GetPasswd("Enter current password: ", false)
	if err != nil {
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}
	if oldPassword == "" {
		logger.Error("Password can not be empty", "meth", meth)
		os.Exit(1)
	}

	newPassword, err := helper.GetPasswd("Enter new password: ", false)
	if err != nil {
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}
	if newPassword == "" {
		logger.Error("Password can not be empty", "meth", meth)
		os.Exit(1)
	}
	if newPassword == oldPassword {
		logger.Error("The new and current passwords must be different", "meth", meth)
		os.Exit(1)
	}

	retypeNewPassword, err := helper.GetPasswd("Retype new password: ", false)
	if err != nil {
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}

	if newPassword != retypeNewPassword {
		logger.Error("Retyped pasword does not match new password", "meth", meth)
		os.Exit(1)
	}

	err = users.ChangePassword(user, oldPassword, newPassword)
	if err != nil {
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}
	fmt.Println("Password changed")
}

func getUser(args []string) (user string, err error) {
	if len(args) > 0 {
		user = args[0]
		return
	}
	token := viper.Global.Get("access-token").(string)
	if token != "" {
		user, err = helper.GetUserFromToken(token)
	}
	return
}
