/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package cmd

import (
	"cluster-manager/pkg/cue2json"
	"cluster-manager/pkg/logger"
	"cluster-manager/pkg/types"
	"cluster-manager/pkg/viper"
	"fmt"
	"os"
	"strings"

	"github.com/spf13/cobra"
	cuedm "gitlab.com/kumori-systems/community/libraries/cue-dependency-manager"
)

var generateClusterConfigFileCmd = &cobra.Command{
	Use:   "generate-cluster-config-file",
	Short: `Create two files containing bash variables representing a Kumori cluster configuration.`,
	Long: `
Using the configuration set in the distribution files, it creates two files (cluster-config.sh and cluster-addons-config.sh)
containing bash variables representing a Kumori cluster configuration.

It is required to have installed the CUE utility (https://cuelang.org/docs/install)`,
	Run: func(cmd *cobra.Command, args []string) { runGenerateClusterConfigFile(cmd, args) },
}

func init() {
	rootCmd.AddCommand(generateClusterConfigFileCmd)
	generateClusterConfigFileCmd.Flags().StringP("distribution-dir", "d", "./cluster", "Directory where the cluster distribution files are")
}

func runGenerateClusterConfigFile(cmd *cobra.Command, args []string) {
	meth := "create.runGenerateClusterConfigFile()"
	distributionDir, err := cmd.Flags().GetString("distribution-dir")
	if err != nil {
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}
	logger.Info("Creating cluster configuration variables files from directory "+distributionDir, "meth", meth)

	// Resolve dependencies of the cluster (cue) directory, using git
	logger.Debug("Resolving cue dependencies (kumori-model)", "meth", meth)
	ctx, err := cuedm.Resolve(distributionDir)
	if err != nil {
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}
	for _, module := range ctx.CUEModules {
		logger.Debug(
			"Cluster-Dependency",
			"Module", module.Name,
			"Resolved", module.Resolved,
			"meth", meth,
		)
	}

	// Determine the currently initialized distribution name (stored in config file)
	if !viper.Global.IsSet("current-distribution") {
		err := fmt.Errorf("unable to determine currently initialized distribution name")
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}
	distributionName := viper.Global.GetString("current-distribution")
	logger.Debug("Currently initialized distribution: " + distributionName)

	// Look for Kumori license and extract credentials
	dockerControllersUsername := ""
	dockerControllersPassword := ""

	if distributionName == "enterprise" {
		enterpriseDistribution := "cluster-configuration::enterprise"
		if viper.Global.IsSet(enterpriseDistribution) {
			// Extract credentials from license info
			credsConfig, err := NewCredentialsFromLicense(enterpriseDistribution)
			if err != nil {
				logger.Error(err.Error(), "meth", meth)
				os.Exit(1)
			}
			if credsConfig != nil {
				dockerControllersUsername = credsConfig["controllers"].Username
				dockerControllersPassword = *credsConfig["controllers"].Token
			}
		}
	}

	// Compiling cue directories
	logger.Debug("Compiling CUE files", "meth", meth)
	err = cue2json.ExportAll(distributionDir, "./all.json")
	if err != nil {
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}
	fmt.Println("CUE files compiled")

	// Load json into structs, and add inline parameters to it
	logger.Debug("Loading json files into structs", "meth", meth)
	clusterAndDistribution, err := types.NewClusterAndDistributionFromFile(distributionDir + "/all.json")
	if err != nil {
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}

	if dockerControllersUsername != "" && dockerControllersPassword != "" {
		clusterAndDistribution.InlineParams.ControllersRegistryUsername = strings.TrimSpace(dockerControllersUsername)
		clusterAndDistribution.InlineParams.ControllersRegistryPassword = strings.TrimSpace(dockerControllersPassword)
	}

	// Generates a distribution-variables.sh file used by the VM image generator
	logger.Debug(
		"Generating cluster and addons configuration variables for VM clusters",
		"content", clusterAndDistribution, "meth", meth,
	)
	// Generate the cluster configuration vars file
	resultFile := "./cluster-config.sh"
	err = prepareClusterConfigVarsFile(resultFile, clusterAndDistribution)
	if err != nil {
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}

	// Generate the cluster addons configuration vars file
	resultFile = "./cluster-addons-config.sh"
	err = prepareClusterAddonsConfigVarsFile(resultFile, clusterAndDistribution)
	if err != nil {
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}
}
