/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package cmd

import (
	"cluster-manager/pkg/alarms"
	"cluster-manager/pkg/helper"
	"cluster-manager/pkg/logger"
	"encoding/json"
	"fmt"
	"os"
	"sort"
	"strings"

	"github.com/spf13/cobra"
)

// alarmsListCmd represents the alarmsList command
var alarmsListCmd = &cobra.Command{
	Use:   "list",
	Short: "List the alarms on the platform.",
	Args:  cobra.NoArgs,
	Run:   func(cmd *cobra.Command, args []string) { runAlarmsList(cmd, args) },
}

func init() {
	alarmsCmd.AddCommand(alarmsListCmd)
	alarmsListCmd.Flags().StringP(
		"output", "o", "list",
		"Output format: list or json",
	)
	alarmsListCmd.Flags().BoolP(
		"include-definitions", "d", false,
		"Include alarm definitions",
	)
	alarmsListCmd.Flags().BoolP(
		"include-implementations", "i", false,
		"Include alarm implementations",
	)
}

func runAlarmsList(cmd *cobra.Command, args []string) {
	meth := "runAlarmsList"

	output, err := cmd.Flags().GetString("output")
	if err != nil {
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}
	includeDefinitions, err := cmd.Flags().GetBool("include-definitions")
	if err != nil {
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}
	includeImplementations, err := cmd.Flags().GetBool("include-implementations")
	if err != nil {
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}

	logger.Debug(
		"Listing alarms",
		"output", output,
		"includeDefinitions", includeDefinitions,
		"includeImplementations", includeImplementations,
		"meth", meth,
	)

	// Get the complete current alarms configuration from cluster
	currentAlarmsConfig, err := alarms.GetAlarmsConfig()
	if err != nil {
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}

	var alarmItems []string
	var alarmDefinitions []string
	var alarmImpls map[string][]string

	alarmItems = currentAlarmsConfig.GetAlarmNames()
	if includeDefinitions {
		alarmDefinitions = currentAlarmsConfig.GetAlarmDefinitionNames()
	}
	if includeImplementations {
		// For now, only Prometheus implementations are available
		alarmImpls = currentAlarmsConfig.GetAlarmImplementationNames()
	}

	// Sort all the lists
	sort.Strings(alarmItems)
	sort.Strings(alarmDefinitions)
	for implType := range alarmImpls {
		sort.Strings(alarmImpls[implType])
	}

	if output == "list" {
		err = printAlarmList(
			currentAlarmsConfig,
			alarmItems,
			includeDefinitions, alarmDefinitions,
			includeImplementations, alarmImpls,
		)
		if err != nil {
			logger.Error(err.Error(), "meth", meth)
			os.Exit(1)
		}
	} else if output == "json" {
		err = printAlarmJSON(
			alarmItems,
			includeDefinitions, alarmDefinitions,
			includeImplementations, alarmImpls,
		)
		if err != nil {
			logger.Error(err.Error(), "meth", meth)
			os.Exit(1)
		}
	} else {
		err = fmt.Errorf("invalid output format %s. Valid formats are 'list' and 'json'", output)
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}
}

func printAlarmList(
	currentAlarmsConfig alarms.AlarmsConfig,
	alarmItems []string,
	includeDefinitions bool, alarmDefinitions []string,
	includeImplementations bool, alarmImpls map[string][]string,
) error {
	alarmsWithoutImplementation := false
	fmt.Println()
	fmt.Println(helper.ToBold("ALARMS:"))
	for _, v := range alarmItems {
		// Check if exists an alarm implementation for the alarm
		exists := currentAlarmsConfig.ExistsImplementationForAlarm(v)
		if exists {
			fmt.Println(v)
		} else {
			alarmsWithoutImplementation = true
			fmt.Println(helper.ToRed(v + "*"))
		}
	}
	if alarmsWithoutImplementation {
		fmt.Println()
		fmt.Println(helper.ToRed("* Doesn't exist an implementation for the alarm"))
	}
	if includeDefinitions {
		fmt.Println()
		fmt.Println(helper.ToBold("ALARM DEFINITIONS:"))
		for _, v := range alarmDefinitions {
			fmt.Println(v)
		}
	}
	if includeImplementations {
		for implType, implList := range alarmImpls {
			fmt.Println()
			fmt.Println(helper.ToBold("ALARM " + strings.ToUpper(implType) + " IMPLEMENTATIONS:"))
			for _, v := range implList {
				fmt.Println(v)
			}
		}
	}
	return nil
}

func printAlarmJSON(
	alarmItems []string,
	includeDefinitions bool, alarmDefinitions []string,
	includeImplementations bool, alarmImpls map[string][]string,
) error {
	var alarmsConfig alarms.AlarmsConfig

	for _, v := range alarmItems {
		alarm, err := alarms.GetAlarm(v)
		if err != nil {
			return fmt.Errorf("error processing alarm %s: %s", v, err.Error())
		}
		alarmsConfig.Alarms = append(alarmsConfig.Alarms, alarm)
	}

	if includeDefinitions {
		for _, v := range alarmDefinitions {
			alarmDef, err := alarms.GetAlarmDefinition(v)
			if err != nil {
				return fmt.Errorf("error processing alarm definition %s: %s", v, err.Error())
			}
			alarmsConfig.AlarmDefinitions = append(alarmsConfig.AlarmDefinitions, alarmDef)
		}
	}

	if includeImplementations {
		alarmsConfig.AlarmImpls = map[string][]alarms.IAlarmImpl{}
		for implType, implList := range alarmImpls {
			for _, v := range implList {
				alarmImpl, err := alarms.GetAlarmImpl(v, implType)
				if err != nil {
					return fmt.Errorf("error processing alarm implementation %s: %s", v, err.Error())
				}
				alarmsConfig.AlarmImpls[implType] = append(alarmsConfig.AlarmImpls[implType], alarmImpl)
			}
		}
	}

	alarmsConfigJSON, err := json.Marshal(alarmsConfig)
	if err != nil {
		return err
	}
	alarmsConfigJSON, err = helper.PrettyJson(alarmsConfigJSON)
	if err != nil {
		return err
	}
	fmt.Println(string(alarmsConfigJSON))

	return nil
}
