/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package cmd

import (
	"cluster-manager/pkg/alarms"
	"cluster-manager/pkg/helper"
	"cluster-manager/pkg/logger"
	"fmt"
	"os"

	"github.com/spf13/cobra"
)

// alarmsGetCmd represents the alarmsGet command
var alarmsGetCmd = &cobra.Command{
	Use:   "get <alarm-name>",
	Short: "Get an alarm from the platform",
	Long: `Get an alarm from the platform.
<alarm-name> is the name of the alarm to get.`,
	Args: cobra.ExactArgs(1),
	Run:  func(cmd *cobra.Command, args []string) { runalarmsGet(cmd, args) },
}

func init() {
	alarmsCmd.AddCommand(alarmsGetCmd)
}

func runalarmsGet(cmd *cobra.Command, args []string) {
	meth := "runalarmsGet"

	alarmName := args[0]

	logger.Debug(
		"Getting alarm",
		"meth", meth,
		"alarm", alarmName,
	)

	var err error
	var alarmConfig alarms.AlarmConfig
	alarmConfig.Alarm, err = alarms.GetAlarm(alarmName)
	if err != nil {
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}

	alarmConfig.AlarmImpl, err = alarms.SearchAlarmImplByDefinition(alarmConfig.Alarm.AlarmDefinition)
	if err != nil {
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}

	alarmConfig.AlarmDefinition, err = alarms.GetAlarmDefinition(alarmConfig.Alarm.AlarmDefinition)
	if err != nil {
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}

	alarmConfigJSON, err := helper.JSONMarshal(alarmConfig)
	if err != nil {
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}
	alarmConfigJSON, err = helper.PrettyJson(alarmConfigJSON)
	if err != nil {
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}
	fmt.Println(string(alarmConfigJSON))
}
