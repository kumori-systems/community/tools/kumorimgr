/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package cmd

import (
	"cluster-manager/pkg/logger"
	"cluster-manager/pkg/users"
	"fmt"
	"os"

	"github.com/spf13/cobra"
)

var createIfNotExists bool

// usersUpdateCmd represents the add command
var usersUpdateCmd = &cobra.Command{
	Use:   "update <users-file>",
	Short: "Update a list of users on the platform.",
	Long: `Update a list of users on the platform.
<users-file> is a JSON file containing the list of users to be updated.
For example:
[
	{
		"username": "user-one",
		"email": "user-one@company.org",
		"firstName": "user-one",
		"lastName": "",
		"password": "123",
		"enabled": true,
		"groups": ["developers", "administrators"]
	},
	...
]`,
	Args: cobra.ExactArgs(1),
	Run:  func(cmd *cobra.Command, args []string) { runUsersUpdate(cmd, args) },
}

func init() {
	usersCmd.AddCommand(usersUpdateCmd)
	usersUpdateCmd.Flags().BoolVar(
		&createIfNotExists, "create-if-not-exists", false,
		"If a user not exists, create it instead of returning an error",
	)
}

func runUsersUpdate(cmd *cobra.Command, args []string) {
	meth := "runUsersUpdate"

	usersFile := args[0]

	logger.Debug(
		"Adding users",
		"usersFile", usersFile,
		"meth", meth,
	)

	usersList, err := users.NewUsersListFromFile(usersFile)
	if err != nil {
		logger.Error("Error reading the user lists file: "+err.Error(), "meth", meth)
		os.Exit(1)
	}

	for _, user := range *usersList {
		created, err := users.Update(user, createIfNotExists)
		if err != nil {
			fmt.Println(user.UserName + ": " + prettyUserAPIError(err))
		} else if created == true {
			fmt.Println(user.UserName + ": created")
		} else {
			fmt.Println(user.UserName + ": updated")
		}
	}
}
