/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package cmd

const KumoriMgrVersion = "1.3.5"
const AppConfigVersion = "0.1.0"
const AppConfigFilePerm = 0664
const CueFilePerm = 0664

// DefaultAppConfig is used to initialize the .kumori/kumorimgr.json file
const DefaultAppConfig = `{
  "config-version": "0.1.0",
  "log-level": "fatal",
  "admission": "",
  "admission-protocol": "https",
  "cluster-configuration": {
    "community": {
      "description": "Community Open-Source edition, free to use.",
      "distribution-url": "https://gitlab.com/kumori-systems/community/distribution.git#v1.5.3",
      "scripts-url": "https://gitlab.com/kumori-systems/community/libraries/kumorimgr-scripts#v1.5.3"
    },
    "development": {
      "description": "Local machine development Open-Source edition, free to use.",
      "distribution-url": "https://gitlab.com/kumori-systems/community/development-distribution.git#v1.5.3",
      "scripts-url": "https://gitlab.com/kumori-systems/community/libraries/kumorimgr-scripts#v1.5.3",
      "addons-installer": {
        "image": "docker.io/kumoripublic/kumori-addon-installer:v1.5.3"
      }
    },
    "enterprise": {
      "description": "Enterprise edition, requires a software license. Visit www.kumori.systems for more info.",
      "distribution-url": "https://gitlab.com/kumori/platform/distributions/enterprise-distribution.git#v1.5.3",
			"scripts-url": "https://gitlab.com/kumori/platform/distributions/tools/kumorimgr-scripts-enterprise.git#v1.5.3",
      "license": {
        "token": ""
      }
    }
  }
}`
