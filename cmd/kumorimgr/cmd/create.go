/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package cmd

import (
	"cluster-manager/pkg/alarms"
	"cluster-manager/pkg/cue2json"
	"cluster-manager/pkg/helper"
	"cluster-manager/pkg/logger"
	"cluster-manager/pkg/types"
	"cluster-manager/pkg/viper"
	"fmt"
	"os"
	"path/filepath"

	"github.com/spf13/cobra"
)

var etcdBackupFullPath string
var pkiBackupDir string
var skipScriptsCheck bool

// Create command:
// - Compiles cue project
// - Generates /scripts/config/scriptVars, used by the scripts, from template
//   /scripts/templates/scriptVars
// - Run scripts

// createCmd represents the create command
var createCmd = &cobra.Command{
	Use:   "create",
	Short: `Create a new cluster`,
	Long: `
Using the configuration stored in the './cluster' directory of the workspace, creates a new cluster.

It is required to have installed the CUE utility (https://cuelang.org/docs/install)`,
	Run: func(cmd *cobra.Command, args []string) { runCreate(cmd, args) },
}

func init() {
	rootCmd.AddCommand(createCmd)
	createCmd.Flags().StringVar(
		&etcdBackupFullPath, "from-etcd-backup", "",
		"Inject the state stored in the etcd backup into the cluster (so ./cluster/distribution.cue packages will not be used",
	)
	createCmd.Flags().StringVar(
		&pkiBackupDir, "from-pki-backup", "",
		"If an etcd backup is used to inject a stored state, use the root certificates files backup",
	)
	createCmd.Flags().BoolVar(&skipScriptsCheck, "skip-scripts-check", false, "skip validation of local scripts repository")
}

func runCreate(cmd *cobra.Command, args []string) {
	meth := "create.runCreate()"
	logger.Info("Creating cluster", "meth", meth)

	// Validate local Scripts repository
	if !skipScriptsCheck {
		err := checkScriptsRepository()
		if err != nil {
			msg := fmt.Sprintf("Local scripts repository validation failed. This validation can be by-passed with the '--skip-scripts-check' flag.")
			fmt.Println(helper.ToRed(msg))
			err = fmt.Errorf("Validation error: : %s", err.Error())
			logger.Error(err.Error())
			os.Exit(1)
		}
	} else {
		logger.Debug("Scripts local repository check skipped.")
	}

	// Determine the currently initialized distribution name (stored in config file)
	if !viper.Global.IsSet("current-distribution") {
		err := fmt.Errorf("Unable to determine currently initialized distribution name.")
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}
	distributionName := viper.Global.GetString("current-distribution")
	logger.Debug("Currently initialized distribution: " + distributionName)

	// Look for Kumori license and extract credentials
	dockerControllersUsername := ""
	dockerControllersPassword := ""

	if distributionName == "enterprise" {
		enterpriseDistribution := "cluster-configuration::enterprise"
		if viper.Global.IsSet(enterpriseDistribution) {
			// Extract credentials from license info
			credsConfig, err := NewCredentialsFromLicense(enterpriseDistribution)
			if err != nil {
				logger.Error(err.Error(), "meth", meth)
				os.Exit(1)
			}
			if credsConfig != nil {
				dockerControllersUsername = credsConfig["controllers"].Username
				dockerControllersPassword = *credsConfig["controllers"].Token
			}
		}
	}

	// About restoring a etcd backup:
	// - Remove etcdbackup directory from previous executions
	// - If a backup must be used:
	//   - The pki backup must be provided too
	//   - Check that pki directory exists and copy to installer directory
	//   - Check that .db file exists and copy to installer directory
	etcdbackupInstallDir := "./scripts/scripts/installer/etcdbackup"
	etcdBackupFile := ""
	if helper.DirExists(etcdbackupInstallDir) {
		helper.EmptyDir(etcdbackupInstallDir)
	}
	if etcdBackupFullPath != "" {
		if pkiBackupDir == "" {
			logger.Error("Flag --from-pki-backup must be used with --from-etcd-backup", "meth", meth)
			os.Exit(1)
		}
		etcdBackupFile = filepath.Base(etcdBackupFullPath)
		if !helper.FileExists(etcdBackupFullPath) {
			logger.Error("File "+etcdBackupFullPath+" not found", "meth", meth)
			os.Exit(1)
		}
		if !helper.DirExists(pkiBackupDir) {
			logger.Error("Directory "+pkiBackupDir+" not found", "meth", meth)
			os.Exit(1)
		}
		_, err := helper.Copy(
			etcdBackupFullPath,
			etcdbackupInstallDir+"/"+etcdBackupFile,
		)
		err = helper.CopyDirContent(
			pkiBackupDir,
			etcdbackupInstallDir,
		)
		if err != nil {
			logger.Error(err.Error(), "meth", meth)
			os.Exit(1)
		}
	}

	// Compiling cue directories
	logger.Info("Compiling CUE files", "meth", meth)
	err := cue2json.ExportAll("./cluster", "./all.json")
	if err != nil {
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}
	fmt.Println("CUE files compiled")

	// Load json into structs, and add inline parameters to it
	logger.Debug("Loading json files into structs", "meth", meth)
	clusterAndDistribution, err :=
		types.NewClusterAndDistributionFromFile("./cluster/all.json")
	if err != nil {
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}
	if etcdBackupFullPath != "" {
		clusterAndDistribution.InlineParams.UseEtcdBackup = "true"
		clusterAndDistribution.InlineParams.EtcdBackupFile = etcdBackupFile
	}
	if dockerControllersUsername != "" && dockerControllersPassword != "" {
		clusterAndDistribution.InlineParams.ControllersRegistryUsername = dockerControllersUsername
		clusterAndDistribution.InlineParams.ControllersRegistryPassword = dockerControllersPassword
	}

	// TODO : decoupling from the script structure

	// Generates /scripts/scripts/installer/variables.sh, used by the scripts
	logger.Debug(
		"Generating variables for scripts",
		"content", clusterAndDistribution, "meth", meth,
	)
	resultFile := "./scripts/scripts/installer/variables.sh"
	err = prepareVarsFile(resultFile, clusterAndDistribution)
	if err != nil {
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}

	// Generates alarms CR in /scripts/scripts/installer/addons/kumori/cr directory
	logger.Debug("Generating alarm related files for scripts", "meth", meth)
	alarmsDirectory := "./scripts/scripts/installer/addons/kumori/alarms"
	err = alarms.PrepareAlarmFiles(alarmsDirectory, clusterAndDistribution)
	if err != nil {
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}
	logger.Debug("Generated alarm related files for scripts", "meth", meth)

	// Launch scripts
	err = launchScript("./scripts/scripts/installer/installer.sh", "")
	if err != nil {
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}
}
