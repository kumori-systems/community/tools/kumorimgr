
run-help: fmt vet
	go run ./cmd/kumorimgr/main.go --help

run-version: fmt vet
	go run ./cmd/kumorimgr/main.go --version

run-login: fmt vet
	go run ./cmd/kumorimgr/main.go login

run-logout: fmt vet
	go run ./cmd/kumorimgr/main.go logout

build: fmt vet
	echo "Building for local OS and architecture"
	go build -o bin/kumorimgr ./cmd/kumorimgr/main.go

build-no-glibc: fmt vet
	echo "Building for local OS and architecture (no glibc dependencies)"
	CGO_ENABLED=0 go build -o bin/kumorimgr-no-glibc ./cmd/kumorimgr/main.go

build-linux-amd64: fmt vet
	echo "Building for Linux amd64"
	GOOS=linux GOARCH=amd64 go build -o bin/kumorimgr-linux-amd64 ./cmd/kumorimgr/main.go

build-macos-amd64: fmt vet
	echo "Building for Darwin amd64"
	GOOS=darwin GOARCH=amd64 go build -o bin/kumorimgr-darwin-amd64 ./cmd/kumorimgr/main.go

build-macos-arm64: fmt vet
	echo "Building for Darwin arm64"
	GOOS=darwin GOARCH=arm64 go build -o bin/kumorimgr-darwin-arm64 ./cmd/kumorimgr/main.go

fmt:
	go fmt ./...

vet:
	go vet ./...
