#!/bin/bash

# CI VARIABLES USED IN THIS SCRIPT
#
# CI_PROJECT_ID="18033405"  # kediku/testing-cluster-manager
# CI_JOB_TOKEN="..."
# CI_COMMIT_REF_NAME="v1.0.0"
# CI_PROJECT_URL="https://gitlab.com/kumori/platform/tools/kumorimgr"

GITLAB_API="https://gitlab.com/api/v4"

RELEASE_NAME="Release ${CI_COMMIT_REF_NAME}"
RELEASE_DESCRIPTION="#### RELEASE NOTES\n\nKumori Manager CLI ${CI_COMMIT_REF_NAME}\n#### CHANGELOG\n\n*TBD*"
RELEASE_BASE_TAG_NAME="${CI_COMMIT_REF_NAME}"


RELEASE_POST_DATA=$(cat <<EOF
{
  "name": "${RELEASE_NAME}",
  "tag_name": "${RELEASE_BASE_TAG_NAME}",
  "description": "${RELEASE_DESCRIPTION}",
  "assets": {
    "links": [
      {
        "name": "Kumori Manager CLI - kumorimgr-${RELEASE_BASE_TAG_NAME}-linux-amd64",
        "url": "${CI_PROJECT_URL}/-/jobs/artifacts/${CI_COMMIT_REF_NAME}/raw/kumorimgr-linux-amd64?job=build"
      },
      {
        "name": "Kumori Manager CLI - kumorimgr-${RELEASE_BASE_TAG_NAME}-darwin-amd64",
        "url": "${CI_PROJECT_URL}/-/jobs/artifacts/${CI_COMMIT_REF_NAME}/raw/kumorimgr-darwin-amd64?job=build"
      },
      {
        "name": "Kumori Manager CLI - kumorimgr-${RELEASE_BASE_TAG_NAME}-darwin-arm64",
        "url": "${CI_PROJECT_URL}/-/jobs/artifacts/${CI_COMMIT_REF_NAME}/raw/kumorimgr-darwin-arm64?job=build"
      }
    ]
  }
}
EOF
)

# IMPORTANT: Authentication must be done using the JOB-TOKEN headers instead
#            of PRIVATE-TOKEN. This is undocumented!
echo -e "Creating release \"${RELEASE_NAME}\" from tag \"${CI_COMMIT_REF_NAME}\"..."

curl -X POST \
  -H 'Content-Type: application/json' \
  -H "JOB-TOKEN: ${CI_JOB_TOKEN}" \
  --data "${RELEASE_POST_DATA}" \
  "${GITLAB_API}/projects/${CI_PROJECT_ID}/releases"

echo -e "Release \"${RELEASE_NAME}\" from tag \"${CI_COMMIT_REF_NAME}\" created."

# List Releases (requires jq)
# echo -e "\nListing releases:"
# curl -s -k -f -H "PRIVATE-TOKEN: ${CI_JOB_TOKEN}" "${GITLAB_API}/projects/${CI_PROJECT_ID}/releases" | jq .[].name
