module cluster-manager

go 1.16

// require (
//   github.com/Jeffail/gabs v1.4.0
//   github.com/fatih/color v1.12.0
//   github.com/ghodss/yaml v1.0.0
//   github.com/mitchellh/go-homedir v1.1.0
//   github.com/olekukonko/tablewriter v0.0.5
//   github.com/spf13/cobra v1.0.0
//   github.com/spf13/viper v1.7.0
//   gitlab.com/kumori-systems/community/libraries/cue-dependency-manager v0.1.4
//   go.uber.org/zap v1.13.0
//   golang.org/x/crypto v0.0.0-20191202143827-86a70503ff7e
//   gopkg.in/src-d/go-git.v4 v4.13.1
//   github.com/dgrijalva/jwt-go v3.2.0
//   github.com/otiai10/copy v1.7.0

// )

require (
	github.com/Jeffail/gabs v1.4.0
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/fatih/color v1.12.0
	github.com/ghodss/yaml v1.0.0
	github.com/mitchellh/go-homedir v1.1.0
	github.com/olekukonko/tablewriter v0.0.5
	github.com/otiai10/copy v1.7.0
	github.com/spf13/cobra v1.0.0
	github.com/spf13/viper v1.7.0
	gitlab.com/kumori-systems/community/libraries/cue-dependency-manager v0.1.4
	go.uber.org/zap v1.13.0
	golang.org/x/crypto v0.0.0-20191202143827-86a70503ff7e
	gopkg.in/src-d/go-git.v4 v4.13.1
)
