/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

// Package cue2json transform a cue source into a json destination.
//
// IMPORTANT: FOR NOW, cue tool must be installed to use this package. In the
// future it will not be necessary.
//
// IMPORTANT: FOR NOW, cue module dependencies are not resolved here; this package
// assumes that dependencies are already resolved.
//
// We can use cuejson package to validate ("vet") the source, and to generate
// ("export") the json files.
// The destination directory will be created, if not exists.
//
// For example:
//
//	err = cue2json.Vet("./src")
//	if err != nil {
//	  ...
//	}
//
//	err = cue2json.Export("./src", "./dst")
//	if err != nil {
//	  ...
//	}
package cue2json
