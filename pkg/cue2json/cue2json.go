/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package cue2json

import (
	"cluster-manager/pkg/helper"
	"cluster-manager/pkg/logger"
	"fmt"
	"os/exec"
	"path"
	"path/filepath"
	"strings"
)

// U is just an alias to the type allowing manipulate json without go-structs
type U = map[string]interface{}

// File permission
const filePerm = 0664
const dirPerm = 0777

// Export executes the "cue export" command in the source directory, processing
// all *.cue files, and generating the *.json files.
func Export(src string) (err error) {
	meth := "cue2json.Export"
	logger.Debug("Exporting", "meth", meth)

	logger.Debug("Changing workdir", "path", src, "meth", meth)
	currentWorkDir, err := helper.ChangeWorkDir(src)
	if err != nil {
		return
	}

	defer func() {
		logger.Debug("Returning to workdir", "path", currentWorkDir, "meth", meth)
		_, err2 := helper.ChangeWorkDir(currentWorkDir)
		if err2 != nil && err == nil {
			err = err2
		}
	}()

	// Compile each cue file in src
	cueFiles, err := filepath.Glob("*.cue")
	if err != nil {
		return
	}
	for _, cueFileName := range cueFiles {
		jsonFileName := strings.TrimSuffix(cueFileName, path.Ext(cueFileName)) + ".json"
		logger.Debug("Compiling", "src", cueFileName, "dst", jsonFileName, "meth", meth)
		cmd := exec.Command("cue", "export", cueFileName)
		var output []byte
		output, err = cmd.CombinedOutput()
		if err != nil {
			err = fmt.Errorf("%s (%s)", err.Error(), string(output))
			return
		}
		err = helper.CreateFile(jsonFileName, filePerm, output)
		if err != nil {
			return
		}
	}

	logger.Debug("Exporting succesful", "meth", meth)
	return
}

// ExportAll executes the "cue export" command in the source directory, processing
// all *.cue files at once, and generating just one destination json files.
func ExportAll(src string, dst string) (err error) {
	meth := "cue2json.ExportAll"
	logger.Debug("Exporting", "meth", meth)

	logger.Debug("Changing workdir", "path", src, "meth", meth)
	currentWorkDir, err := helper.ChangeWorkDir(src)
	if err != nil {
		return
	}

	defer func() {
		logger.Debug("Returning to workdir", "path", currentWorkDir, "meth", meth)
		_, err2 := helper.ChangeWorkDir(currentWorkDir)
		if err2 != nil && err == nil {
			err = err2
		}
	}()

	logger.Debug("Compiling", "src", src, "dst", dst, "meth", meth)
	cmd := exec.Command("cue", "export")
	var output []byte
	output, err = cmd.CombinedOutput()
	if err != nil {
		err = fmt.Errorf("%s (%s)", err.Error(), string(output))
		return
	}
	err = helper.CreateFile(dst, filePerm, output)
	if err != nil {
		return
	}

	logger.Debug("Exporting succesful", "meth", meth)
	return
}

// ExportOne executes the "cue export" command for only one cue file.
func ExportOne(srcFile string, dstFile string) (err error) {
	meth := "cue2json.ExportOne"
	logger.Debug("Exporting", "src", srcFile, "dst", dstFile, "meth", meth)

	srcDir := filepath.Dir(srcFile)
	logger.Debug("Changing workdir", "path", srcDir, "meth", meth)
	currentWorkDir, err := helper.ChangeWorkDir(srcDir)
	if err != nil {
		return
	}

	defer func() {
		logger.Debug("Returning to workdir", "path", currentWorkDir, "meth", meth)
		_, err2 := helper.ChangeWorkDir(currentWorkDir)
		if err2 != nil && err == nil {
			err = err2
		}
	}()

	// Compile cue file
	logger.Debug("Compiling", "src", srcFile, "dst", dstFile, "meth", meth)
	cmd := exec.Command("cue", "export", srcFile)
	var output []byte
	output, err = cmd.CombinedOutput()
	if err != nil {
		err = fmt.Errorf("%s (%s)", err.Error(), string(output))
		return
	}
	err = helper.CreateFile(dstFile, filePerm, output)
	if err != nil {
		return
	}

	logger.Debug("Exporting succesful", "meth", meth)
	return
}
