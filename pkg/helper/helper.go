/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package helper

import (
	"bufio"
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"os"
	"os/exec"
	"path"
	"path/filepath"
	"strings"
	"syscall"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/fatih/color"
	"golang.org/x/crypto/ssh/terminal"
)

const jwtTokenUsernameField = "preferred_username"

var bold = color.New(color.Bold).SprintFunc()
var blue = color.New(color.FgBlue).SprintFunc()
var yellow = color.New(color.FgYellow).SprintFunc()
var red = color.New(color.FgRed).SprintFunc()
var green = color.New(color.FgGreen).SprintFunc()
var boldBlue = color.New(color.FgBlue, color.Bold).SprintFunc()
var boldYellow = color.New(color.FgYellow, color.Bold).SprintFunc()
var boldRed = color.New(color.FgRed, color.Bold).SprintFunc()
var boldGreen = color.New(color.FgGreen, color.Bold).SprintFunc()

// FileExists checks if a file exists and is not a directory before we
// try using it to prevent further errors.
func FileExists(filename string) bool {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}

// DirExists checks if a dir exists and is directory before we
// try using it to prevent further errors.
func DirExists(dir string) bool {
	info, err := os.Stat(dir)
	if os.IsNotExist(err) {
		return false
	}
	return info.IsDir()
}

// ChangeWorkDir changes the workdir, and returns the current workdir
func ChangeWorkDir(newWorkDir string) (oldWorkDir string, err error) {
	oldWorkDir, err = os.Getwd()
	if err != nil {
		return
	}
	err = os.Chdir(newWorkDir)
	return
}

// EnsureDirExists creates a directory if not exists
func EnsureDirExists(dir string) error {
	return os.MkdirAll(dir, os.ModeDir|0777)
}

// CreateFile creates a file (ensuring that the directory is created, if it does
// not exist) with the provided content
func CreateFile(
	fileFullName string, filePerm os.FileMode, content []byte,
) (
	err error,
) {
	filePath := filepath.Dir(fileFullName)
	err = os.MkdirAll(filePath, os.ModeDir|0777)
	if err != nil {
		return
	}
	err = os.WriteFile(fileFullName, content, filePerm)
	if err != nil {
		return
	}
	return
}

// CreateFiles work like CreateFile, but for several files
func CreateFiles(
	fileFullNames []string, filePerm os.FileMode, contents [][]byte,
) (
	err error,
) {
	for k, fileFullName := range fileFullNames {
		filePath := filepath.Dir(fileFullName)
		content := contents[k]
		err = os.MkdirAll(filePath, os.ModeDir|0777)
		if err != nil {
			return
		}
		err = os.WriteFile(fileFullName, content, filePerm)
		if err != nil {
			return
		}
	}
	return
}

// RenameIfExists renames a directory if exists, ensuring that the destination
// exists.
func RenameIfExists(src string, dst string) (err error) {
	if DirExists(src) {
		err = os.MkdirAll(filepath.Dir(dst), os.ModeDir|0777)
		if err != nil {
			return
		}
		err = os.Rename(src, dst)
		if err != nil {
			return
		}
	}
	return
}

// EmptyDir removes all contents from a given folder but not the folder itself
func EmptyDir(dir string) (int, error) {

	// Checks if the dst directoty is or not empty
	file, err := os.Open(dir)
	if err != nil {
		fmt.Printf("\n\nError: %s %s\n\n", dir, err.Error())
		return 0, err
	}
	files, err := file.Readdir(0)
	if err != nil {
		fmt.Printf("\n\nError: %s %s\n\n", dir, err.Error())
		return 0, err
	}

	// If the folder is already empty, nothing to do.
	if len(files) == 0 {
		return 0, nil
	}

	// Remove all content in the folder
	removed := 0
	for _, fileInfo := range files {
		filePath := path.Join(dir, fileInfo.Name())
		if fileInfo.IsDir() {
			err := os.RemoveAll(filePath)
			if err != nil {
				fmt.Printf("\n\nError: %s %s\n\n", filePath, err.Error())
				return removed, err
			}
			removed++
			continue
		}
		err := os.Remove(filePath)
		if err != nil {
			fmt.Printf("\n\nError: %s %s\n\n", filePath, err.Error())
			return removed, err
		}
		removed++
	}

	return removed, nil
}

// Copy function copy src file to dst file, checking that is a regular
// file (ensuring that the directory is created, if it does not exist)
func Copy(src string, dst string) (nbytes int64, err error) {
	nbytes = 0
	sourceFileStat, err := os.Stat(src)
	if err != nil {
		return
	}
	if !sourceFileStat.Mode().IsRegular() {
		err = fmt.Errorf("%s is not a regular file", src)
		return
	}
	source, err := os.Open(src)
	if err != nil {
		return
	}
	defer source.Close()
	filePath := filepath.Dir(dst)
	err = os.MkdirAll(filePath, os.ModeDir|0777)
	if err != nil {
		return
	}
	destination, err := os.Create(dst)
	if err != nil {
		return
	}
	defer destination.Close()
	nbytes, err = io.Copy(destination, source)
	return
}

// CopyDirContent copy the content of "src" directory to "dst" directory
// If "dst" directory not exists, it will be created
//
// TODO: UGLY IMPLEMENTATION. TO BE IMPROVED
func CopyDirContent(src string, dst string) (err error) {
	err = os.MkdirAll(dst, os.ModeDir|0777)
	if err != nil {
		return
	}
	cmd := exec.Command("cp", "-r", src, dst)
	var output []byte
	output, err = cmd.CombinedOutput()
	if err != nil {
		err = fmt.Errorf("%s (%s)", err.Error(), string(output))
		return
	}
	return
}

// Copy function copy src file to dst file, checking that is a regular
// file (ensuring that the directory is created, if it does not exist)
func ReadFile(src string) (data string, err error) {

	sourceFileStat, err := os.Stat(src)
	if err != nil {
		return
	}
	if !sourceFileStat.Mode().IsRegular() {
		err = fmt.Errorf("%s is not a regular file", src)
		return
	}

	source, err := os.Open(src)
	if err != nil {
		return
	}
	defer source.Close()

	dataBytes, err := io.ReadAll(source)
	data = string(dataBytes)

	return
}

// Get password from user input. To allow a string to be retrieved from Stdin,
// the 'allowStdin' flag must be true.
func GetPasswd(prompt string, allowStdin bool) (password string, err error) {
	if prompt != "" {
		fmt.Print(prompt)
	} else {
		fmt.Print("Enter Password: ")
	}
	bytePassword, err := ReadPassword("", allowStdin)
	if err != nil {
		return
	}
	fmt.Println()
	password = strings.TrimSpace(string(bytePassword))
	return
}

// ReadPassword read password function that works for both for:
// - user types the password after being asked
// - password is passed via stdin without TTY (pipes, etc.)
//
// This is a known issue of this package and this method is taken from a
// proposed in the issue:
// See: https://github.com/golang/go/issues/19909
func ReadPassword(prompt string, allowStdin bool) ([]byte, error) {
	fmt.Fprint(os.Stderr, prompt)
	var fd int
	var pass []byte
	if terminal.IsTerminal(syscall.Stdin) {
		fd = syscall.Stdin
		inputPass, err := terminal.ReadPassword(fd)
		if err != nil {
			return nil, err
		}
		pass = inputPass
	} else if allowStdin {
		reader := bufio.NewReader(os.Stdin)
		s, err := reader.ReadString('\n')
		if err != nil {
			return nil, err
		}
		pass = []byte(s)
	}

	return pass, nil
}

// GetUserFromToken reads the username from the token.
// The "github.com/dgrijalva/jwt-go" is used, because is the mos popular in
// the https://jwt.io libraries list.
// In this case, we dont need verify the token, just read a field... so we can
// use the "dangerous" ParseUnverified function.
func GetUserFromToken(tokenstr string) (user string, err error) {
	token, _, err := new(jwt.Parser).ParseUnverified(tokenstr, jwt.MapClaims{})
	if err == nil {
		user = token.Claims.(jwt.MapClaims)[jwtTokenUsernameField].(string)
	}
	return
}

func ToBold(value string) (converted string) {
	converted = bold(value)
	return
}

func ToBlue(value string) (converted string) {
	converted = blue(value)
	return
}

func ToYellow(value string) (converted string) {
	converted = yellow(value)
	return
}

func ToRed(value string) (converted string) {
	converted = red(value)
	return
}

func ToGreen(value string) (converted string) {
	converted = green(value)
	return
}

func ToBoldBlue(value string) (converted string) {
	converted = boldBlue(value)
	return
}

func ToBoldYellow(value string) (converted string) {
	converted = boldYellow(value)
	return
}

func ToBoldRed(value string) (converted string) {
	converted = boldRed(value)
	return
}

func ToBoldGreen(value string) (converted string) {
	converted = boldGreen(value)
	return
}

func PrettyJson(src []byte) (dst []byte, err error) {
	var aux bytes.Buffer
	err = json.Indent(&aux, src, "", "  ")
	if err != nil {
		return
	}
	dst = aux.Bytes()
	return
}

// JSONMarshal take into account special characters like "<" or ">"
func JSONMarshal(t interface{}) ([]byte, error) {
	buffer := &bytes.Buffer{}
	encoder := json.NewEncoder(buffer)
	encoder.SetEscapeHTML(false)
	err := encoder.Encode(t)
	return buffer.Bytes(), err
}
