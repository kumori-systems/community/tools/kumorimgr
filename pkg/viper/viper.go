/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package viper

import (
	viperLib "github.com/spf13/viper"
)

// Why this module?
// Viper uses "." as key delimiter, so it's a problem work with config files
// keys are like "kumori.systems/kumori"... and we want this kind of keys.
// But: Viper allows you to change the delimiter... but not in the global
// instance of Viper. Therefore, it is necessary to create a new instance of
// Viper, which we expose globally through this package.

var Global *viperLib.Viper

func init() {
	Global = viperLib.NewWithOptions(viperLib.KeyDelimiter("::"))
}
