/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package logger

import (
	"fmt"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

var singleton *zap.SugaredLogger
var singletonCfg zap.Config

func Init(level string, output []string, encoding string, colorize bool) (err error) {

	singletonCfg.Encoding = encoding
	singletonCfg.OutputPaths = output
	singletonCfg.ErrorOutputPaths = output
	var encodeLevel zapcore.LevelEncoder
	if colorize == true {
		encodeLevel = zapcore.CapitalColorLevelEncoder
	} else {
		encodeLevel = zapcore.CapitalLevelEncoder
	}
	singletonCfg.EncoderConfig = zapcore.EncoderConfig{
		MessageKey:  "message",
		LevelKey:    "level",
		EncodeLevel: encodeLevel,
	}
	singletonCfg.Level = zap.NewAtomicLevel()
	SetLevel(level)

	logger, err := singletonCfg.Build()
	if err != nil {
		return
	}
	defer logger.Sync()

	zap.ReplaceGlobals(logger)
	singleton = zap.S()

	return
}

func SetLevel(level string) {
	switch level {
	case "debug":
		singletonCfg.Level.SetLevel(zap.DebugLevel)
	case "info":
		singletonCfg.Level.SetLevel(zap.InfoLevel)
	case "warn":
		singletonCfg.Level.SetLevel(zap.WarnLevel)
	case "error":
		singletonCfg.Level.SetLevel(zap.ErrorLevel)
	case "fatal":
		singletonCfg.Level.SetLevel(zap.FatalLevel)
	default:
		singletonCfg.Level.SetLevel(zap.InfoLevel)
	}
}

func Info(message string, fields ...interface{}) {
	singleton.Infow(message, fields...)
}

func Debug(message string, fields ...interface{}) {
	singleton.Debugw(message, fields...)
}

func Warn(message string, fields ...interface{}) {
	singleton.Warnw(message, fields...)
}

func Error(message string, fields ...interface{}) {
	singleton.Errorw(message, fields...)
	fmt.Println(message)
}

func Fatal(message string, fields ...interface{}) {
	singleton.Fatalw(message, fields...)
	fmt.Println(message)
}
