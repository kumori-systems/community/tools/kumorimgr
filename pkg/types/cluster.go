/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package types

import (
	"encoding/json"
	"os"
)

type Noderole = string

type Label struct {
	Noderole         string `json:"noderole"`
	DisableTxOffload bool   `json:"disabletxoffload,omitempty"`
	Maintenance      bool   `json:"maintenance,omitempty"`
}

type Machine struct {
	IP     string  `json:"ip"`
	Labels []Label `json:"labels"`
}

func (m Machine) IsRole(role string) bool {
	for _, label := range m.Labels {
		if label.Noderole == role {
			return true
		}
	}
	return false
}

func (m Machine) IsInMaintenance() bool {
	for _, label := range m.Labels {
		if label.Maintenance == true {
			return true
		}
	}
	return false
}

type SSH struct {
	User           string `json:"user"`
	PrivateKeyPath string `json:"privateKeyPath"`
}

type ClusterPki struct {
	RootCaFile string `json:"rootCaFile"`
}

type DNSProviderConfig struct {
	ConfigDir  *string `json:"configDir"`
	ConfigFile *string `json:"configFile"`
}

type DNSProvider struct {
	Name   string            `json:"name"`
	Config DNSProviderConfig `json:"config"`
}

type DNSManagement struct {
	Enabled  bool         `json:"enabled"`
	Provider *DNSProvider `json:"provider"`
}

type DNS struct {
	ReferenceDomain        string        `json:"referenceDomain"`
	ReferenceDomainCertDir string        `json:"referenceDomainCertDir"`
	Management             DNSManagement `json:"management"`
}

type Storage struct {
	Nodes []Machine `json:"nodes"`
}

type Ingress struct {
	Nodes            []Machine `json:"nodes"`
	InternalIP       *string   `json:"internalIP"`
	ExternalIP       *string   `json:"externalIP"`
	TcpPorts         *[]uint16 `json:"tcpPorts"`
	KeepalivedVRID   *uint16   `json:"keepalivedVRID"`
	CertHeaderFormat *string   `json:"certHeaderFormat"`
}

type APIServer struct {
	InternalIP     *string `json:"internalIP"`
	ExternalIP     *string `json:"externalIP"`
	Port           *uint16 `json:"port"`
	KeepalivedVRID *uint16 `json:"keepalivedVRID"`
}

type CustomScripts struct {
	Enabled   bool    `json:"enabled"`
	Directory *string `json:"directory"`
}

type ControllersHub struct {
	Hub      string  `json:"hub"`
	Username *string `json:"username"`
	Password *string `json:"password"`
}

type DockerHubCredentials struct {
	Username *string `json:"username"`
	Password *string `json:"password"`
}

type Zone struct {
	Nodes []Machine `json:"nodes"`
}

type Cluster struct {
	Name                 string               `json:"name"`
	Machines             []Machine            `json:"machines"`
	SSH                  SSH                  `json:"ssh"`
	DockerBridgeNet      string               `json:"dockerBridgeNet"`
	PodNet               string               `json:"podNet"`
	ClusterIPNet         string               `json:"clusterIPNet"`
	InternalDomain       string               `json:"internalDomain"`
	ClusterPki           ClusterPki           `json:"clusterPki"`
	DNS                  DNS                  `json:"dns"`
	Storage              Storage              `json:"storage"`
	Ingress              Ingress              `json:"ingress"`
	APIServer            APIServer            `json:"apiServer"`
	CustomScripts        CustomScripts        `json:"customScripts"`
	ControllersHub       ControllersHub       `json:"controllersHub"`
	DockerHubCredentials DockerHubCredentials `json:"dockerHubCredentials"`
	DNSResolvers         []string             `json:"dnsResolvers"`
	RegistryMirrors      []string             `json:"registryMirrors"`
	Zones                *map[string]Zone     `json:"zones,omitempty"`
}

func NewClusterFromFile(filePath string) (cluster *Cluster, err error) {
	cluster = &Cluster{}
	content, err := os.ReadFile(filePath)
	if err != nil {
		return
	}
	err = json.Unmarshal(content, cluster)
	return
}

func NewClusterFromContent(content []byte) (cluster *Cluster, err error) {
	err = json.Unmarshal(content, cluster)
	return
}
