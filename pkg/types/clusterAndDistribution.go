/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package types

import (
	"cluster-manager/pkg/helper"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"os"
	"sort"
	"strconv"
	"strings"
	"time"
)

const (
	MasterRole = "master"
	WorkerRole = "worker"
)

type InlineParams struct {
	UseEtcdBackup               string
	EtcdBackupFile              string
	ControllersRegistryUsername string
	ControllersRegistryPassword string
}

type UpdateParams struct {
	AddMastersIPs               []string
	AddWorkersIPs               []string
	RemoveMastersIPs            []string
	RemoveWorkersIPs            []string
	AddStorageIPs               []string
	RemoveStorageIPs            []string
	AddMaintenanceMastersIPs    []string
	AddMaintenanceWorkersIPs    []string
	RemoveMaintenanceMastersIPs []string
	RemoveMaintenanceWorkersIPs []string
}

type ClusterAndDistribution struct {
	Cluster      Cluster      `json:"c"`
	Distribution Distribution `json:"d"`
	InlineParams InlineParams
	UpdateParams UpdateParams
}

func NewClusterAndDistributionFromFile(
	filePath string,
) (
	clusterAndDistribution *ClusterAndDistribution, err error,
) {
	clusterAndDistribution = &ClusterAndDistribution{
		InlineParams: InlineParams{
			UseEtcdBackup:               "false",
			EtcdBackupFile:              "",
			ControllersRegistryUsername: "",
			ControllersRegistryPassword: "",
		},
		UpdateParams: UpdateParams{
			AddMastersIPs:               []string{},
			AddWorkersIPs:               []string{},
			RemoveMastersIPs:            []string{},
			RemoveWorkersIPs:            []string{},
			AddMaintenanceMastersIPs:    []string{},
			AddMaintenanceWorkersIPs:    []string{},
			RemoveMaintenanceMastersIPs: []string{},
			RemoveMaintenanceWorkersIPs: []string{},
		},
	}
	content, err := os.ReadFile(filePath)
	if err != nil {
		return
	}
	err = json.Unmarshal(content, clusterAndDistribution)
	if err != nil {
		return
	}

	// Machines must appear just once	!
	IPMap := map[string]string{}
	for _, machine := range clusterAndDistribution.Cluster.Machines {
		if _, ok := IPMap[machine.IP]; ok == true {
			err = fmt.Errorf("Duplicated IP: %s", machine.IP)
			return
		}
		IPMap[machine.IP] = machine.IP // We use a map just for convenience
	}

	// Either all nodes have been assigned to a single zone or all nodes have not been assigned to any zone.
	if (clusterAndDistribution.Cluster.Zones != nil) && (len(*clusterAndDistribution.Cluster.Zones) > 0) {

		zones := map[string]string{}
		for zoneName, zone := range *clusterAndDistribution.Cluster.Zones {
			for _, machine := range zone.Nodes {
				if existingZone, ok := zones[machine.IP]; ok && (zoneName != existingZone) {
					err = fmt.Errorf("node %s assigned to more than one zone", machine.IP)
					return
				}
				zones[machine.IP] = zoneName
			}
		}

		if len(clusterAndDistribution.Cluster.Machines) != len(zones) {
			for _, machine := range clusterAndDistribution.Cluster.Machines {
				if _, ok := zones[machine.IP]; !ok {
					err = fmt.Errorf("node %s not assigned to any zone", machine.IP)
					return
				}
			}
		}
	}

	return
}

// -----------------------------------------------------------------------------
//
// Generic functions used from scripts go template.
// Values are returned raw (no quoting)
//
// -----------------------------------------------------------------------------

func (c *ClusterAndDistribution) GetDistributionVersion() string {

	versionStr := IntArrayToString(c.Distribution.Ref.Version, ".")
	return versionStr
}

// IMPORTANT: This method always returns an unquoted string
// Properties sub-levels are supported if they are of type [string]value, so a
// list of properties can be provided
func (c *ClusterAndDistribution) GetConfigurationParameter(packageName string, properties ...string) string {

	// fmt.Println("packageName: ", packageName)
	// fmt.Println("properties: ", properties)
	for _, packageDep := range c.Distribution.PackageList {
		if packageDep.Package.Name == packageName {
			var aux map[string]interface{} = packageDep.Configuration
			for _, property := range properties {
				if aux == nil {
					return ""
				}
				prop := aux[property]
				// If property doesn't exist, <nil> is returned and switch enters the default case
				// If property has sub-properties, the "for" loop continues with the next level
				// fmt.Println("prop: ", prop)
				// fmt.Println("prop: ", reflect.TypeOf(prop))
				switch v := prop.(type) {
				case int:
					return fmt.Sprintf("%v", v)
				case float64:
					return fmt.Sprintf("%v", v)
				case bool:
					return fmt.Sprintf("%v", v)
				case string:
					return fmt.Sprintf("%v", v)
				case []interface{}:
					// Returns a string like ["thing1", "thing2"]
					aux := ""
					for _, item := range v {
						if aux != "" {
							aux += ","
						}
						itemStr := fmt.Sprintf("\"%v\"", item)
						aux += itemStr
					}
					return "[" + aux + "]"
				case interface{}:
					aux = prop.(map[string]interface{})
				default:
					return ""
				}
			}
		}
	}
	return ""
}

// Returns a sorted list of the keys of an object of the configuration specified
// by a path. If path points to a simple type, this function returns nil.
// Sub-levels are supported if they are of type [string]value, so a list of
// properties can be provided.
func (c *ClusterAndDistribution) GetConfigurationParameterKeys(packageName string, properties ...string) []string {

	// fmt.Println("packageName: ", packageName)
	// fmt.Println("properties: ", properties)
	for _, packageDep := range c.Distribution.PackageList {
		if packageDep.Package.Name == packageName {
			var aux map[string]interface{} = packageDep.Configuration
			for _, property := range properties {
				if aux == nil {
					return nil
				}
				prop := aux[property]
				// If property doesn't exist, <nil> is returned and switch enters the default case
				// If property has sub-properties, the "for" loop continues with the next level
				// fmt.Println("prop: ", prop)
				// fmt.Println("prop: ", reflect.TypeOf(prop))
				switch prop.(type) {
				case int:
					return nil
				case float64:
					return nil
				case bool:
					return nil
				case string:
					return nil
				case []interface{}:
					return nil
				case interface{}:
					// Build the list of keys and sort it
					aux = prop.(map[string]interface{})
					keys := make([]string, 0, len(aux))
					for k, _ := range aux {
						keys = append(keys, k)
					}
					sort.Strings(keys)
					return keys
				default:
					return nil
				}
			}
		}
	}
	return nil
}

// IMPORTANT: This method always returns an unquoted string
// Properties sub-levels are supported if they are of type [string]value, so a
// list of properties can be provided.
// This method is equivalent to GetConfigurationParameter except that if the final
// element is an object, it will try to convert it to JSON and return a stringified
// representation.
func (c *ClusterAndDistribution) GetStringifiedConfigurationParameter(packageName string, properties ...string) (string, error) {

	// fmt.Println("packageName: ", packageName)
	// fmt.Println("properties: ", properties)
	var lastObject map[string]interface{}
	for _, packageDep := range c.Distribution.PackageList {
		if packageDep.Package.Name == packageName {
			var aux map[string]interface{} = packageDep.Configuration
			for _, property := range properties {
				if aux == nil {
					return "", nil
				}
				prop := aux[property]
				// If property doesn't exist, <nil> is returned and switch enters the default case
				// If property has sub-properties, the "for" loop continues with the next level
				// fmt.Println("prop: ", prop)
				// fmt.Println("prop: ", reflect.TypeOf(prop))
				switch v := prop.(type) {
				case int:
					return fmt.Sprintf("%v", v), nil
				case float64:
					return fmt.Sprintf("%v", v), nil
				case bool:
					return fmt.Sprintf("%v", v), nil
				case string:
					return fmt.Sprintf("%v", v), nil
				case []interface{}:
					// Returns a string like ["thing1", "thing2"]
					aux := ""
					for _, item := range v {
						if aux != "" {
							aux += ","
						}
						itemStr := fmt.Sprintf("\"%v\"", item)
						aux += itemStr
					}
					return "[" + aux + "]", nil
				case interface{}:
					aux = prop.(map[string]interface{})
					lastObject = aux
				default:
					return "", nil
				}
			}

			stringifiedObject, err := json.Marshal(lastObject)
			if err != nil {
				err = fmt.Errorf("unable to convert property %s %s to JSON: %s", packageName, properties, err.Error())
				fmt.Println("ERROR: ", err)
				return "", err
			}
			return string(stringifiedObject), nil
		}
	}
	return "", nil
}

// This function is equivalent to GetConfigurationParameter function, but
// is used for parameters using the GO duration sintax (for example: "2min") and
// convert it to milliseconds
func (c *ClusterAndDistribution) GetConfigurationParameterAsMsec(packageName string, properties ...string) string {
	value := c.GetConfigurationParameter(packageName, properties...)

	// Convert the value to time.Duration
	duration, err := time.ParseDuration(value)
	if err != nil { // For example: if value == ""
		return "0"
	} else {
		// Convert the time.Duration value to milliseconds
		// (time.Duration is the number of nanoseconds)
		return fmt.Sprintf("%d", int64(duration/time.Millisecond))
	}
}

func (c *ClusterAndDistribution) GetResourceParameter(packageName string, container string, property string) string {

	for _, packageDep := range c.Distribution.PackageList {
		if packageDep.Package.Name == packageName {
			containerResources := packageDep.Resources[container]

			// Workaround gor Golang not allowing access to properties by name (and to avoid using reflection)
			switch strings.ToLower(property) {
			case "cpu":
				return containerResources.Cpu
			case "cpulimit":
				return containerResources.CpuLimit
			case "mem":
				return containerResources.Mem
			case "memlimit":
				return containerResources.MemLimit
			default:
				return ""
			}
		}
	}
	return ""
}

func (c *ClusterAndDistribution) GetPackageVersion(packageName string) string {

	for _, packageDep := range c.Distribution.PackageList {
		if packageDep.Package.Name == packageName {
			return packageDep.Package.Version
		}
	}
	return ""
}

// -----------------------------------------------------------------------------
//
// Functions used from scripts go template.
// Values are returned in the format expected by the scripts
//
// -----------------------------------------------------------------------------

func (c *ClusterAndDistribution) GetAPIServerExternalIP() string {
	if c.Cluster.DNS.Management.Enabled && c.Cluster.APIServer.ExternalIP != nil {
		return "\"" + *c.Cluster.APIServer.ExternalIP + "\""
	}
	return "\"\""
}

func (c *ClusterAndDistribution) GetAPIServerInternalBalancing() string {
	if c.Cluster.APIServer.InternalIP != nil && *c.Cluster.APIServer.InternalIP != "" {
		return "\"true\""
	}
	return "\"false\""
}

func (c *ClusterAndDistribution) GetAPIServerHandleFloatingIP() string {
	if c.Cluster.APIServer.InternalIP != nil && *c.Cluster.APIServer.InternalIP != "" {
		return "\"true\""
	}
	return "\"false\""
}

func (c *ClusterAndDistribution) GetAPIServerInternalIP() string {
	if c.Cluster.APIServer.InternalIP != nil {
		return "\"" + *c.Cluster.APIServer.InternalIP + "\""
	}
	return "\"\""
}

func (c *ClusterAndDistribution) GetAPIServerPort() string {
	if c.Cluster.APIServer.Port != nil {
		return fmt.Sprint(*c.Cluster.APIServer.Port)
	}
	return "\"\""
}

func (c *ClusterAndDistribution) GetAPIServerVRID() string {
	if c.Cluster.APIServer.KeepalivedVRID != nil {
		return fmt.Sprint(*c.Cluster.APIServer.KeepalivedVRID)
	}
	return "\"\""
}

func (c *ClusterAndDistribution) GetIngressCertHeaderFormat() string {
	if c.Cluster.Ingress.CertHeaderFormat != nil {
		return fmt.Sprint(*c.Cluster.Ingress.CertHeaderFormat)
	}
	return "\"PEM\""
}

func (c *ClusterAndDistribution) GetIngressExternalIP() string {
	if c.Cluster.DNS.Management.Enabled && c.Cluster.Ingress.ExternalIP != nil {
		return "\"" + *c.Cluster.Ingress.ExternalIP + "\""
	}
	return "\"\""
}

func (c *ClusterAndDistribution) GetIngressInternalBalancing() string {
	if c.Cluster.Ingress.InternalIP != nil && *c.Cluster.Ingress.InternalIP != "" {
		return "\"true\""
	}
	return "\"false\""
}

func (c *ClusterAndDistribution) GetIngressHandleFloatingIP() string {
	if c.Cluster.Ingress.InternalIP != nil && *c.Cluster.Ingress.InternalIP != "" {
		return "\"true\""
	}
	return "\"false\""
}

func (c *ClusterAndDistribution) GetIngressInternalIP() string {
	if c.Cluster.Ingress.InternalIP != nil {
		return "\"" + *c.Cluster.Ingress.InternalIP + "\""
	}
	return "\"\""
}

func (c *ClusterAndDistribution) GetIngressVRID() string {
	if c.Cluster.Ingress.KeepalivedVRID != nil {
		return fmt.Sprint(*c.Cluster.Ingress.KeepalivedVRID)
	}
	return "\"\""
}

func (c *ClusterAndDistribution) GetCustomScriptsDirectory() string {
	if c.Cluster.CustomScripts.Enabled && c.Cluster.CustomScripts.Directory != nil {
		return "\"" + *c.Cluster.CustomScripts.Directory + "\""
	}
	return "\"\""
}

func (c *ClusterAndDistribution) GetDNSProviderName() string {
	if c.Cluster.DNS.Management.Enabled {
		if c.Cluster.DNS.Management.Provider != nil {
			provider := *c.Cluster.DNS.Management.Provider
			return "\"" + provider.Name + "\""
		}
	}
	return "\"\""
}

func (c *ClusterAndDistribution) GetDNSRoute53ConfigDir() string {
	if c.Cluster.DNS.Management.Enabled {
		if (c.Cluster.DNS.Management.Provider != nil) &&
			(c.Cluster.DNS.Management.Provider.Name == "route53") {

			provider := *c.Cluster.DNS.Management.Provider
			return "\"" + *provider.Config.ConfigDir + "\""
		}
	}
	return "\"\""
}

func (c *ClusterAndDistribution) GetDNSOVHConfigFile() string {
	if c.Cluster.DNS.Management.Enabled {
		if (c.Cluster.DNS.Management.Provider != nil) &&
			(c.Cluster.DNS.Management.Provider.Name == "ovh") {

			provider := *c.Cluster.DNS.Management.Provider
			return "\"" + *provider.Config.ConfigFile + "\""
		}
	}
	return "\"\""
}

func (c *ClusterAndDistribution) GetControllersHubRegistry() string {
	return "\"" + c.Cluster.ControllersHub.Hub + "\""
}

func (c *ClusterAndDistribution) GetControllersHubUsername() string {
	if c.Cluster.ControllersHub.Username != nil && *c.Cluster.ControllersHub.Username != "" {
		return "\"" + *c.Cluster.ControllersHub.Username + "\""
	} else if c.InlineParams.ControllersRegistryUsername != "" {
		return "\"" + c.InlineParams.ControllersRegistryUsername + "\""
	}
	return "\"\""
}

func (c *ClusterAndDistribution) GetControllersHubPassword() string {
	if c.Cluster.ControllersHub.Password != nil && *c.Cluster.ControllersHub.Password != "" {
		return "\"" + *c.Cluster.ControllersHub.Password + "\""
	} else if c.InlineParams.ControllersRegistryPassword != "" {
		return "\"" + c.InlineParams.ControllersRegistryPassword + "\""
	}
	return "\"\""
}

func (c *ClusterAndDistribution) GetDockerHubUsername() string {
	if c.Cluster.DockerHubCredentials.Username != nil {
		return "\"" + *c.Cluster.DockerHubCredentials.Username + "\""
	}
	return "\"\""
}

func (c *ClusterAndDistribution) GetDockerHubPassword() string {
	if c.Cluster.DockerHubCredentials.Password != nil {
		return "\"" + *c.Cluster.DockerHubCredentials.Password + "\""
	}
	return "\"\""
}

func (c *ClusterAndDistribution) GetEventStoreType() string {

	var eventExporterConfig PackageConfiguration

	for _, packageDep := range c.Distribution.PackageList {
		if packageDep.Package.Name == "eventexporter" {
			eventExporterConfig = packageDep.Configuration
			break
		}
	}

	enabled, _ := strconv.ParseBool(eventExporterConfig["enabled"].(string))
	if enabled {
		return eventExporterConfig["eventStoreType"].(string)
	} else {
		return "k8s"
	}
}

func (c *ClusterAndDistribution) GetStorageProvider() string {
	// Look for 'enabled' property in Storage provider packages (currently only
	// OpenEBS package).
	enabledString := c.GetConfigurationParameter("openebs", "enabled")

	if enabledString == "true" {
		return "openebs"
	}

	return ""
}

func (c *ClusterAndDistribution) GetStorageClassForType(storeType string) string {
	provider := c.GetConfigurationParameter("kumori-volume-controller", "volumeTypes", storeType, "provider")
	class := c.GetConfigurationParameter("kumori-volume-controller", "volumeTypes", storeType, "class")
	storageClass := provider + "-" + class

	if storageClass == "-" {
		return ""
	} else {
		return storageClass
	}

}

// Returns the list (an array) of Volume type names
func (c *ClusterAndDistribution) GetVolumeTypeNames() []string {
	volumeTypeNames := c.GetConfigurationParameterKeys("kumori-volume-controller", "volumeTypes")
	return volumeTypeNames
}

// Returns the StorageClass associated to a Volume type
func (c *ClusterAndDistribution) GetVolumeTypeStorageClass(volumeTypeName string) string {
	provider := c.GetConfigurationParameter("kumori-volume-controller", "volumeTypes", volumeTypeName, "provider")
	class := c.GetConfigurationParameter("kumori-volume-controller", "volumeTypes", volumeTypeName, "class")
	storageClass := provider + "-" + class

	if storageClass == "-" {
		return ""
	} else {
		return storageClass
	}
}

// Returns the Properties string associated to a Volume type
func (c *ClusterAndDistribution) GetVolumeTypeProperties(volumeTypeName string) string {
	volumeTypeProperties := c.GetConfigurationParameter("kumori-volume-controller", "volumeTypes", volumeTypeName, "properties")
	return volumeTypeProperties
}

// Converts a comma-separeted list passed as a string (unquoted elements) to
// a string containing a space-separated list (quoted elements).
//
// Example:
//
// Input: "/dev/vda,/dev/vdb"
// Output: "\"/dev/vda\" \"/dev/vdb\"""
func (c *ClusterAndDistribution) CommaSeparatedToSpaceSeparated(csStr string) string {

	arr := strings.Split(csStr, ",")

	spStr := ""

	isFisrt := true
	for _, item := range arr {
		item = strings.TrimSpace(item)
		if isFisrt {
			spStr = spStr + "\"" + item + "\""
			isFisrt = false
		} else {
			spStr = spStr + " \"" + item + "\""
		}
	}
	return spStr
}

// Converts a comma-separeted list passed as a string (unquoted elements) to
// a Bash array format (quoted elements).
//
// Example:
//
// Input: "/dev/vda,/dev/vdb"
// Output:
//
//	"(
//	   \"/dev/vda\"
//	   \"/dev/vdb\"
//	 )"
func (c *ClusterAndDistribution) CommaSeparatedToBashArray(csStr string) string {

	arr := strings.Split(csStr, ",")

	spStr := "("

	isFisrt := true
	for _, item := range arr {
		item = strings.TrimSpace(item)
		if isFisrt {
			spStr = spStr + "\n" + "  " + "\"" + item + "\"" + "\n"
			isFisrt = false
		} else {
			spStr = spStr + "  " + "\"" + item + "\"" + "\n"
		}
	}
	spStr = spStr + ")"
	return spStr
}

func IntArrayToString(intArr []int, separator string) string {
	if len(intArr) == 0 {
		return ""
	}

	strArr := make([]string, len(intArr))
	for i, v := range intArr {
		strArr[i] = strconv.Itoa(v)
	}
	return strings.Join(strArr, separator)
}

// Converts a file path to a base64-encoded string with the contents of the file.
// If the file path is empty, it returns an empty string.
func (c *ClusterAndDistribution) FilePathToBase64String(filePath string) string {

	// fmt.Printf("FilePathToBase64String - filePath = %s\n", filePath)
	if filePath == "" {
		return ""
	}

	// Get HOME variable in case the path uses it
	homeDir := os.Getenv("HOME")

	// Resolve HOME references in the file path (both $HOME and ${HOME})
	resolvedFilePath := strings.Replace(filePath, "$HOME", homeDir, -1)
	resolvedFilePath = strings.Replace(resolvedFilePath, "${HOME}", homeDir, -1)

	// Check if file exists
	if !helper.FileExists(resolvedFilePath) {
		err := fmt.Errorf("file not found: %s", resolvedFilePath)
		fmt.Println("ERROR: ", err)
		os.Exit(1)
	}

	// Read file contents
	data, err := helper.ReadFile(resolvedFilePath)
	if err != nil {
		errStr := fmt.Errorf("error reading file %s: %s", resolvedFilePath, err.Error())
		fmt.Println("ERROR: ", errStr)
		os.Exit(1)
	}

	// Base64-encode the file contents
	base64Data := base64.StdEncoding.EncodeToString([]byte(data))

	return base64Data
}
