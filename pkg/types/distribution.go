/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package types

import (
	"encoding/json"
	"fmt"
	"os"
)

type Version = []int

type Ref struct {
	Version Version `json:"version"`
	Domain  string  `json:"domain"`
	Kind    string  `json:"kind"`
	Name    string  `json:"name"`
}

type Resources struct {
	Cpu      string `json:"cpu"`
	CpuLimit string `json:"cpuLimit"`
	Mem      string `json:"mem"`
	MemLimit string `json:"memLimit"`
}

type PackageConfiguration map[string]interface{}

type PackageResources map[string]Resources

type Package struct {
	Name         string    `json:"name"`
	Version      string    `json:"version"`
	Dependencies []Package `json:"dependencies"`
}

type PackageDeployment struct {
	Package          Package              `json:"package"`
	Configuration    PackageConfiguration `json:"configuration"`
	Resources        PackageResources     `json:"resources"`
	DeploymentMethod interface{}          `json:"deploymentMethod"`
}

type Integration map[string][]PackageDeployment

type OSVersion struct {
	Flavour string `json:"flavour"`
	Version string `json:"version"`
	Kernel  string `json:"kernel"`
	Systemd string `json:"systemd"`
}

type Distribution struct {
	Ref         Ref                 `json:"ref"`
	KubeVersion string              `json:"kubeVersion"`
	OSVersion   OSVersion           `json:"osVersion"`
	Packages    Integration         `json:"packages"`
	PackageList []PackageDeployment `json:"packageList"`
}

func NewDistributionFromFile(filePath string) (distribution *Distribution, err error) {
	distribution = &Distribution{}
	content, err := os.ReadFile(filePath)
	if err != nil {
		return
	}
	err = json.Unmarshal(content, distribution)
	return
}

func NewDistributionFromContent(content []byte) (distribution *Distribution, err error) {
	err = json.Unmarshal(content, distribution)
	return
}

func (d *Distribution) GetPackageConfig(name string) (cfg PackageConfiguration, err error) {
	for _, v := range d.PackageList {
		if v.Package.Name == name {
			cfg = v.Configuration
			return
		}
	}
	err = fmt.Errorf("Package %s not found", name)
	return
}
