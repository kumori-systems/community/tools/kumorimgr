/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package users

import (
	"cluster-manager/pkg/admission"
	"encoding/json"
	"fmt"
	"os"
	"strings"

	"github.com/Jeffail/gabs"
)

const (
	errorUserExists      = "user already exists"
	errorUpdateNotExists = "user not found"
)

// User struct contains the properties of an user
type User struct {
	UserName  string   `json:"username"`
	Email     string   `json:"email"`
	FirstName string   `json:"firstName"`
	LastName  string   `json:"lastName"`
	Password  string   `json:"password"`
	Enabled   bool     `json:"enabled"`
	Groups    []string `json:"groups"`
}

// ToString returns the user object as a string
func (u User) ToString() (strUser string) {
	jsonUser, err := json.Marshal(u)
	if err == nil {
		strUser = string(jsonUser)
	}
	return
}

// ToJSON returns the user object as a json []byte
func (u User) ToJSON() (strJSONUser []byte, err error) {
	strJSONUser, err = json.Marshal(u)
	return
}

// UsersList struct contains the properties of a list of users
type UsersList []User

// NewUsersListFromFile creates an UsersList object from a JSON file
func NewUsersListFromFile(filePath string) (usersList *UsersList, err error) {
	content, err := os.ReadFile(filePath)
	if err != nil {
		return
	}
	usersList, err = NewUsersListFromJSON(string(content), false)
	return
}

// NewUsersListFromJSON creates an UsersList object from JSON
func NewUsersListFromJSON(usersJSON string, allowNoPassword bool) (usersList *UsersList, err error) {
	usersList = &UsersList{}
	err = json.Unmarshal([]byte(usersJSON), usersList)

	// User properties can't be empty strings
	// Special case: password will be empty if the list is retrieved from cluster
	for _, user := range *usersList {
		if (user.UserName == "") ||
			(user.Email == "") ||
			(user.FirstName == "") ||
			(user.LastName == "") ||
			((user.Password == "") && (allowNoPassword == false)) {
			err = fmt.Errorf("User properties can't be empty: %s", user.ToString())
		}
	}

	return
}

// Create creates a new user using Admission API.
func Create(user User, updateIfExists bool) (updated bool, err error) {
	jsonUser, err := user.ToJSON()
	if err != nil {
		return
	}
	err = admission.CreateUser(jsonUser)
	if err != nil {
		strErr := err.Error()
		if strings.Contains(strErr, errorUserExists) {
			// Error due a already-exists user
			if updateIfExists == true {
				// Update the user
				updated = true
				_, err = Update(user, false)
			}
		}
	}
	return
}

// Update updates a new user using Admission API.
func Update(user User, createIfNotExists bool) (created bool, err error) {
	jsonUser, err := user.ToJSON()
	if err != nil {
		return
	}
	err = admission.UpdateUser(jsonUser)
	if err != nil {
		strErr := err.Error()
		if strings.Contains(strErr, errorUpdateNotExists) {
			// Error due a not-exists user
			if createIfNotExists == true {
				// Create the user
				created = true
				_, err = Create(user, false)
			}
		}
	}
	return
}

// DeleteUser deletes a user using Admission API.
func DeleteUser(userName string) (err error) {
	admErr := admission.DeleteUser(userName)
	if admErr != nil {
		// Simplify the most common error
		commonErr := "User " + userName + " not found"
		strErr := admErr.Error()
		if strings.Contains(strErr, commonErr) {
			err = fmt.Errorf(commonErr)
		}
	}
	return
}

// GetUsers retrieves the current list of users using Admission API
func GetUsers() (userJSON *gabs.Container, err error) {
	return admission.GetUsers()
}

// ChangePassword changes the password of a user
func ChangePassword(user, oldPassword, newPassword string) (err error) {
	jsonChange := []byte(`{
	  "oldPassword": "` + oldPassword + `",
		"newPassword": "` + newPassword + `"
	}`)
	err = admission.ChangeUserPassword(user, jsonChange)
	return
}
