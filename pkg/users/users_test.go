/*
* Copyright 2022 Kumori systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package users

import (
	"strings"
	"testing"
)

const (
	errorInvalidJSON     = "invalid character '}' looking for beginning of object key string"
	errorPropertiesEmpty = "User properties can't be empty"
	errorFileNotExists   = "open ./testdata/users/not_exists: no such file or directory"
)

// Test a right list of users
func TestRightList(t *testing.T) {
	fileName := "./testdata/users/right_list.json"
	_, err := NewUsersListFromFile(fileName)
	if err != nil {
		t.Error("Unexpected error:" + err.Error())
	}
}

// Test an invalid json formt list of users
func TestNotJson(t *testing.T) {
	fileName := "./testdata/users/invalid_json.json"
	_, err := NewUsersListFromFile(fileName)
	if err == nil {
		t.Error("An error was expected")
	}
	if err.Error() != errorInvalidJSON {
		t.Error("Unexpected error: ", err.Error())
	}
}

// Test a list of users with empty properties
func TestEmptyProperties(t *testing.T) {
	fileName := "./testdata/users/empty_properties.json"
	_, err := NewUsersListFromFile(fileName)
	if err == nil {
		t.Error("An error was expected")
	}
	if !strings.HasPrefix(err.Error(), errorPropertiesEmpty) {
		t.Error("Unexpected error: ", err.Error())
	}
}

// Test a list of users with nil properties
func TestNilProperties(t *testing.T) {
	fileName := "./testdata/users/nil_properties.json"
	_, err := NewUsersListFromFile(fileName)
	if err == nil {
		t.Error("An error was expected")
	}
	if !strings.HasPrefix(err.Error(), errorPropertiesEmpty) {
		t.Error("Unexpected error: ", err.Error())
	}
}

// Test a not-exists file with the list of users
func TestNotExistsFile(t *testing.T) {
	fileName := "./testdata/users/not_exists"
	_, err := NewUsersListFromFile(fileName)
	if err == nil {
		t.Error("An error was expected")
	}
	if err.Error() != errorFileNotExists {
		t.Error("Unexpected error: ", err.Error())
	}
}
