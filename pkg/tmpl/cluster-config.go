/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package tmpl

const ClusterConfigVarsTemplate = `
#!/bin/bash
################################################################################
## CLUSTER CONFIGURATION                                                      ##
##                                                                            ##
##   The following configuration settings are specific for one cluster.       ##
##                                                                            ##
##   This file has been automatically generated from the cluster              ##
##   configuration CUE files. Do not edit manually.                           ##
##                                                                            ##
################################################################################

####################################
##  KUMORI CLUSTER CONFIGURATION  ##
####################################

# Cluster name
CLUSTER_NAME="{{.Cluster.Name}}"
RELEASE_NAME="${CLUSTER_NAME}"


# Linux user, used to determine the installation files path
SSH_USER="ubuntu"
MAIN_USER="${SSH_USER}"

# Cluster reference domain and wildcard certificate files
REFERENCE_DOMAIN="{{.Cluster.DNS.ReferenceDomain}}"
REFERENCE_DOMAIN_CERT_DIR="{{.Cluster.DNS.ReferenceDomainCertDir}}"
REF_DOMAIN_WILDCARD_CERTIFICATE_CRT_FILE="${REFERENCE_DOMAIN_CERT_DIR}/wildcard.${REFERENCE_DOMAIN}.crt"
REF_DOMAIN_WILDCARD_CERTIFICATE_KEY_FILE="${REFERENCE_DOMAIN_CERT_DIR}/wildcard.${REFERENCE_DOMAIN}.key"

# Cluster PKI
# - Admission server certificate (public and private key)
# - Cluster root CA (public key)
ADMISSION_SERVER_CERT_DIR="{{ .GetConfigurationParameter "kumori-admission" "clientCertAuthParams" "serverCertDir" }}"
ADMISSION_SERVER_CERTIFICATE_CRT_FILE="${ADMISSION_SERVER_CERT_DIR}/cert.crt"
ADMISSION_SERVER_CERTIFICATE_KEY_FILE="${ADMISSION_SERVER_CERT_DIR}/cert.key"
CLUSTER_PKI_ROOT_CA_FILE="{{.Cluster.ClusterPki.RootCaFile}}"
CLUSTER_PKI_ROOT_CA_FILENAME=$(basename ${CLUSTER_PKI_ROOT_CA_FILE})


# Kumori controllers registry
KUMORI_IMAGES_REGISTRY={{.GetControllersHubRegistry}}
KUMORI_IMAGES_REGISTRY_USERNAME={{.GetControllersHubUsername}}
KUMORI_IMAGES_REGISTRY_PASSWORD={{.GetControllersHubPassword}}

# Kumori CoreDNS image registry, necessary for configuring CoreDNS image
KUMORI_COREDNS_IMAGE_REGISTRY="${KUMORI_IMAGES_REGISTRY}"
# if [ "${KUMORI_IMAGES_REGISTRY}" == "registry.gitlab.com/kumori/platform/images/dev" ]; then
#   KUMORI_COREDNS_IMAGE_REGISTRY="${KUMORI_IMAGES_REGISTRY}/kumoridns"
# fi

# CoreDNS image versions (Kubernetes and Kumori)
# TODO: this could be obtained from the distribution versions file
KUMORI_COREDNS_VERSION="{{ .GetPackageVersion "kumori-coredns" }}"

# Determine CoreDNS version expected by Kubernetes, based on versioning table:
# https://github.com/coredns/deployment/blob/master/kubernetes/CoreDNS-k8s_version.md
if [[ "${KUBERNETES_VERSION}" =~ ^1.16.* ]]; then
  KUBERNETES_COREDNS_VERSION="1.6.2"
elif [[ "${KUBERNETES_VERSION}" =~ ^1.17.* ]]; then
  KUBERNETES_COREDNS_VERSION="1.6.5"
elif [[ "${KUBERNETES_VERSION}" =~ ^1.18.* ]]; then
  KUBERNETES_COREDNS_VERSION="1.6.7"
elif [[ "${KUBERNETES_VERSION}" =~ ^1.19.* ]]; then
  KUBERNETES_COREDNS_VERSION="1.7.0"
elif [[ "${KUBERNETES_VERSION}" =~ ^1.20.* ]]; then
  KUBERNETES_COREDNS_VERSION="1.7.0"
elif [[ "${KUBERNETES_VERSION}" =~ ^1.21.* ]]; then
  KUBERNETES_COREDNS_VERSION="1.8.0"
elif [[ "${KUBERNETES_VERSION}" =~ ^1.25.* ]]; then
  KUBERNETES_COREDNS_VERSION="1.9.3"
else
  # Unsupported Kubernetes version! Set a default value
  KUBERNETES_COREDNS_VERSION="1.9.3"
fi

DOCKERHUB_USERNAME={{.GetDockerHubUsername}}
DOCKERHUB_PASSWORD={{.GetDockerHubPassword}}

# Optional Docker registry mirror (applies to the whole cluster)
DOCKER_REGISTRY_MIRRORS=(
  {{- range $mkey, $mirror := .Cluster.RegistryMirrors}}
  "{{$mirror}}"
  {{- end }}
)

# If list of mirrors isn't empty, pick the first one
if [[ "${#DOCKER_REGISTRY_MIRRORS[@]}" -eq 0 ]]
then
  DOCKER_REGISTRY_MIRROR=""
else
  DOCKER_REGISTRY_MIRROR="${DOCKER_REGISTRY_MIRRORS[0]}"
fi


#
# CLUSTER NETWORK
#
DOCKER_BRIDGE_CIDR="{{.Cluster.DockerBridgeNet}}"
PODS_NETWORK_CIDR="{{.Cluster.PodNet}}"
SERVICES_NETWORK_CIDR="{{.Cluster.ClusterIPNet}}"


#
# APISERVER CONFIGURATION
#
APISERVER_REGISTER_DOMAIN="{{.Cluster.DNS.Management.Enabled}}"
APISERVER_DOMAIN="apiserver-${CLUSTER_NAME}.${REFERENCE_DOMAIN}"
APISERVER_VIRTUAL_IP={{.GetAPIServerInternalIP}}
APISERVER_VIRTUAL_IP_EXTERNAL={{.GetAPIServerExternalIP}}
APISERVER_DNS_IP="${APISERVER_VIRTUAL_IP_EXTERNAL}"
APISERVER_DNS_TTL="300"


#
# INGRESS CONFIGURATION
#
INGRESS_REGISTER_DOMAIN="{{.Cluster.DNS.Management.Enabled}}"
INGRESS_DOMAIN="ingress-${CLUSTER_NAME}.${REFERENCE_DOMAIN}"
INGRESS_VIRTUAL_IP={{.GetIngressInternalIP}}
INGRESS_VIRTUAL_IP_EXTERNAL={{.GetIngressExternalIP}}
INGRESS_DNS_IP="${INGRESS_VIRTUAL_IP_EXTERNAL}"
INGRESS_DNS_TTL="300"
INGRESS_HANDLE_FLOATING_IP={{.GetIngressHandleFloatingIP}}
# TODO: current calculation of this flag won't work for VM scenario
# INGRESS_INTERNAL_BALANCING={{.GetIngressInternalBalancing}}
INGRESS_INTERNAL_BALANCING="false"
if [ "${INGRESS_INTERNAL_BALANCING}" = "true" ]; then
  INGRESS_EXTERNAL_PORT="443"
  INGRESS_INTERNAL_PORT="8443"
else
  INGRESS_EXTERNAL_PORT="443"
  INGRESS_INTERNAL_PORT="443"
fi
INGRESS_TCP_PORTS=(
  {{- range $mkey, $tcpPort := .Cluster.Ingress.TcpPorts}}
  {{$tcpPort}}
  {{- end }}
)
INGRESS_TCP_INTERNAL_PORTS=()
if [ "${INGRESS_INTERNAL_BALANCING}" = "true" ]; then
  for PORT in ${INGRESS_TCP_PORTS[@]}; do
    # Currently allowed TCP ports are in the range 9000-9999, so we will just add
    # 10000 to convert them to internal: 19000-19999
    INGRESS_TCP_INTERNAL_PORTS+=("1${PORT}")
  done
else
  # Simply copy the original TCP port list
  INGRESS_TCP_INTERNAL_PORTS=( "${INGRESS_TCP_PORTS[@]}" )
fi
INGRESS_VRID={{.GetIngressVRID}}
INGRESS_NAMESPACE="kumori"
INGRESS_CERTHEADER_FORMAT="{{ .GetIngressCertHeaderFormat }}"


#
# DNS CONFIGURATION
#
MANAGED_DNS="{{.Cluster.DNS.Management.Enabled}}"
MANAGED_DNS_PROVIDER={{.GetDNSProviderName}}
MANAGED_DNS_ROUTE53_CLI_IMAGE="amazon/aws-cli:2.0.8"
MANAGED_DNS_ROUTE53_AWS_CONFIG_DIR={{.GetDNSRoute53ConfigDir}}
AWS_CREDENTIALS_FILE="${MANAGED_DNS_ROUTE53_AWS_CONFIG_DIR}/credentials"
AWS_CONFIGURATION_FILE="${MANAGED_DNS_ROUTE53_AWS_CONFIG_DIR}/configuration"

# OVH DNS specific configuration
MANAGED_DNS_OVH_CLI_IMAGE="kumoripublic/kovhcli:v1.2.0"
MANAGED_DNS_OVH_CONFIG_FILE={{.GetDNSOVHConfigFile}}

`
