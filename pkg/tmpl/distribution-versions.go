/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package tmpl

// TODO: TO IMPROVE!!! How manage better? Gitlab repository with templates?

const DistributionVersionsVarsTemplate = `
#!/bin/bash

################################################################################
## CLUSTER DISTRIBUTION CONFIGURATION                                         ##
##                                                                            ##
##   The following configuration settings are common to all clusters of the   ##
##   Kumori distribution version. The values are fixed by distribution, not   ##
##   by cluster.                                                              ##
##                                                                            ##
##   This file has been automatically generated from the cluster              ##
##   configuration CUE files. Do not edit manually.                           ##
##                                                                            ##
################################################################################

CLUSTER_VERSION="{{.GetDistributionVersion}}"

KUMORICTL_SUPPORTED_VERSIONS={{ (.GetConfigurationParameter "kumori-admission" "kumorictlSupportedVersions") | .CommaSeparatedToBashArray }}

KUMORIMGR_SUPPORTED_VERSIONS={{ (.GetConfigurationParameter "kumori-admission" "kumorimgrSupportedVersions") | .CommaSeparatedToBashArray }}

#
# MACHINE SOFTWARE VERSIONS
#
OS_FLAVOUR="{{.Distribution.OSVersion.Flavour}}"
OS_VERSION="{{.Distribution.OSVersion.Version}}"
KERNEL_VERSION="{{.Distribution.OSVersion.Kernel}}"
SYSTEMD_VERSION="{{.Distribution.OSVersion.Systemd}}"
KUBERNETES_VERSION="{{.Distribution.KubeVersion}}"
HELM_VERSION="v2.17.0"
DOCKER_VERSION="{{ .GetPackageVersion "docker" }}"
CONTAINERD_VERSION="{{ .GetPackageVersion "containerd" }}"
CRI_DOCKERD_VERSION="{{ .GetPackageVersion "cridockerd" }}"
KEEPALIVED_VERSION="{{ .GetPackageVersion "keepalived" }}"
ENVOY_VERSION="{{ .GetPackageVersion "envoy" }}"
YQ_VERSION="3.3.2"


#
# ADDONS VERSIONS
#
CALICO_VERSION="{{ .GetPackageVersion "calico" }}"
AMBASSADOR_VERSION="{{ .GetPackageVersion "ambassador" }}"
KUBE_PROMETHEUS_VERSION="{{ .GetPackageVersion "kube-prometheus" }}"
ELASTICSEARCH_VERSION="{{ .GetPackageVersion "elasticsearch" }}"
FILEBEAT_VERSION="{{ .GetPackageVersion "filebeat" }}"
KIBANA_VERSION="{{ .GetPackageVersion "kibana" }}"
K8S_DASHBOARD_VERSION="{{ .GetPackageVersion "kubernetes-dashboard" }}"
KEYCLOAK_VERSION="{{ .GetPackageVersion "keycloak" }}"
MINIO_VERSION="{{ .GetPackageVersion "minio" }}"
MINIO_MC_VERSION="{{ .GetConfigurationParameter "minio" "minioMcVersion" }}"
ETCD_BACKUP_VERSION="{{ .GetPackageVersion "etcdbackup" }}"
ETCD_RESTORE_IMAGE="registry.k8s.io/etcd:3.5.6-0"
EXTERNALDNS_VERSION="{{ .GetPackageVersion "externaldns" }}"
EVENT_EXPORTER_VERSION="{{ .GetPackageVersion "eventexporter" }}"
OPENEBS_VERSION="{{ .GetPackageVersion "openebs" }}"
DESCHEDULER_VERSION="{{ .GetPackageVersion "descheduler" }}"
CADVISOR_VERSION="{{ .GetConfigurationParameter "kube-prometheus" "cadvisor" "version" }}"
CSI_CINDER_PROVIDER_VERSION="{{ .GetPackageVersion "csi-cinder" }}"
CSI_NFS_PROVIDER_VERSION="{{ .GetPackageVersion "csi-nfs" }}"
CSI_CEPH_RBD_PROVIDER_VERSION="{{ .GetPackageVersion "csi-ceph-rbd" }}"
CERT_MANAGER_VERSION="{{ .GetPackageVersion "certmanager" }}"
CERT_MANAGER_WEBHOOK_OVH_VERSION="{{ .GetPackageVersion "certmanager-webhook-ovh" }}"
CLUSTERAPI_VERSION="{{ .GetPackageVersion "clusterapi" }}"
CLUSTERAPI_OPENSTACK_PROVIDER_VERSION="{{ .GetConfigurationParameter "clusterapi" "providers" "infrastructureOpenstack" "version" }}"
CLUSTERAPI_AWS_PROVIDER_VERSION="{{ .GetConfigurationParameter "clusterapi" "providers" "infrastructureAws" "version" }}"
OPENSTACK_CLOUD_CONTROLLER_VERSION="{{ .GetPackageVersion "openstackcloudcontroller" }}"
AWS_CLOUD_CONTROLLER_VERSION="{{ .GetPackageVersion "awscloudcontroller" }}"

#
# KUMORI CONTROLLERS VERSIONS
#
KUMORI_IMAGES_PULL_POLICY="Always"
KUMORI_CRD_VERSION="{{ .GetPackageVersion "kumori-crd" }}"
KUMORI_KUVOLUME_VERSION="{{ .GetPackageVersion "kumori-volume" }}"
KUMORI_COREDNS_VERSION="{{ .GetPackageVersion "kumori-coredns" }}"
KUMORI_TOPOLOGY_CONTROLLER_VERSION="{{ .GetPackageVersion "kumori-topology" }}"
KUMORI_KUCONTROLLER_VERSION="{{ .GetPackageVersion "kumori-kucontroller" }}"
KUMORI_KUINBOUND_VERSION="{{ .GetPackageVersion "kumori-kuinbound" }}"
KUMORI_SOLUTION_CONTROLLER_VERSION="{{ .GetPackageVersion "kumori-solution-controller" }}"
KUMORI_ADMISSION_VERSION="{{ .GetPackageVersion "kumori-admission" }}"
KUMORI_KUALARM_VERSION="{{ .GetPackageVersion "kumori-kualarm" }}"
KUMORI_VOLUME_CONTROLLER_VERSION="{{ .GetPackageVersion "kumori-volume-controller" }}"
KUMORI_IPFILTERING_VERSION="{{ .GetPackageVersion "kumori-ipfiltering" }}"
KUMORI_AUTHSERVICE_VERSION="{{ .GetPackageVersion "kumori-authservice" }}"


# Determine CoreDNS version expected by Kubernetes, based on versioning table:
# https://github.com/coredns/deployment/blob/master/kubernetes/CoreDNS-k8s_version.md
if [[ "${KUBERNETES_VERSION}" =~ ^1.16.* ]]; then
  KUBERNETES_COREDNS_VERSION="1.6.2"
elif [[ "${KUBERNETES_VERSION}" =~ ^1.17.* ]]; then
  KUBERNETES_COREDNS_VERSION="1.6.5"
elif [[ "${KUBERNETES_VERSION}" =~ ^1.18.* ]]; then
  KUBERNETES_COREDNS_VERSION="1.6.7"
elif [[ "${KUBERNETES_VERSION}" =~ ^1.19.* ]]; then
  KUBERNETES_COREDNS_VERSION="1.7.0"
elif [[ "${KUBERNETES_VERSION}" =~ ^1.20.* ]]; then
  KUBERNETES_COREDNS_VERSION="1.7.0"
elif [[ "${KUBERNETES_VERSION}" =~ ^1.21.* ]]; then
  KUBERNETES_COREDNS_VERSION="1.8.0"
elif [[ "${KUBERNETES_VERSION}" =~ ^1.25.* ]]; then
  KUBERNETES_COREDNS_VERSION="1.9.3"
else
  # Unsupported Kubernetes version! Set a default value
  KUBERNETES_COREDNS_VERSION="1.9.3"
fi

`
