/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package tmpl

const ClusterAddonsConfigVarsTemplate = `
#!/bin/bash
################################################################################
## CLUSTER ADDONS CONFIGURATION                                               ##
##                                                                            ##
##   The following configuration settings are specific for one cluster.       ##
##                                                                            ##
##   This file has been automatically generated from the cluster              ##
##   configuration CUE files. Do not edit manually.                           ##
##                                                                            ##
################################################################################

################################
##     KUMORI CONTROLLERS     ##
################################

INSTALL_KUALARM="{{ .GetConfigurationParameter "kumori-kualarm" "enabled" }}"

INSTALL_VOLUME_CONTROLLER="{{ .GetConfigurationParameter "kumori-volume-controller" "enabled" }}"

ADMISSION_AUTHENTICATION_TYPE="{{ .GetConfigurationParameter "kumori-admission" "authenticationType" }}"
# Admission server cert is configured at cluster level (with PKIs)
# ADMISSION_SERVER_CERT_DIR="{{ .GetConfigurationParameter "kumori-admission" "clientCertAuthParams" "serverCertDir" }}"
ADMISSION_CLIENT_CERT_REQUIRED="{{ .GetConfigurationParameter "kumori-admission" "clientCertAuthParams" "clientCertRequired" }}"
ADMISSION_SERVER_CLIENT_CERT_VALIDATE_WITH_TRUSTED_CA="{{ .GetConfigurationParameter "kumori-admission" "clientCertAuthParams" "clientCertValidateWithTrustedCA" }}"
ADMISSION_ADMIN_PASSWORD="{{ .GetConfigurationParameter "kumori-admission" "tokenAuthParams" "adminPassword" }}"
ADMISSION_DEVEL_PASSWORD="{{ .GetConfigurationParameter "kumori-admission" "tokenAuthParams" "develPassword" }}"
ADMISSION_ALLOWEDIP='{{ .GetConfigurationParameter "kumori-admission" "allowedIP" }}'

KUINBOUND_MINTLSVERSION="{{ .GetConfigurationParameter "kumori-kuinbound" "minTLSVersion" }}"
KUINBOUND_MAXTLSVERSION="{{ .GetConfigurationParameter "kumori-kuinbound" "maxTLSVersion" }}"
KUINBOUND_CIPHERSUITETLS="{{ .GetConfigurationParameter "kumori-kuinbound" "cipherSuitesTLS" }}"
KUINBOUND_ECDHCURVESTLS="{{ .GetConfigurationParameter "kumori-kuinbound" "ecdhCurvesTLS" }}"
KUINBOUND_HSTS="{{ .GetConfigurationParameter "kumori-kuinbound" "hsts" }}"

KUCONTROLLER_DEFAULT_LIVENESS_TIMEOUT="{{ .GetConfigurationParameter "kumori-kucontroller" "defaultLivenessTimeout" }}"
KUCONTROLLER_DEFAULT_READINESS_TIMEOUT="{{ .GetConfigurationParameter "kumori-kucontroller" "defaultReadinessTimeout" }}"
KUCONTROLLER_TSC_MAX_SKEW="{{ .GetConfigurationParameter "kumori-kucontroller" "topologySpreadConstraintsMaxSkew" }}"
KUCONTROLLER_REVISION_HISTORY_LIMIT="{{ .GetConfigurationParameter "kumori-kucontroller" "revisionHistoryLimit" }}"
KUCONTROLLER_FORCE_REBOOT_ON_UPDATE="{{ .GetConfigurationParameter "kumori-kucontroller" "forceRebootOnUpdate" }}"
KUCONTROLLER_DEFAULT_DISK_REQUEST_SIZE="{{ .GetConfigurationParameter "kumori-kucontroller" "defaultDiskRequestSize" }}"
KUCONTROLLER_DEFAULT_DISK_REQUEST_UNIT="{{ .GetConfigurationParameter "kumori-kucontroller" "defaultDiskRequestUnit" }}"
KUCONTROLLER_DEFAULT_VOLATILE_VOLUMES_TYPE="{{ .GetConfigurationParameter "kumori-kucontroller" "defaultVolatileVolumesType" }}"
KUCONTROLLER_EXTERNAL_SERVICES_ACCESS="{{ .GetConfigurationParameter "kumori-kucontroller" "externalServicesAccess" }}"

INSTALL_IPFILTERING="{{ .GetConfigurationParameter "kumori-ipfiltering" "enabled" }}"
IPFILTERING_REJECTIFFAILURE="{{ .GetConfigurationParameter "kumori-ipfiltering" "rejectIfFailure" }}"
IPFILTERING_TIMEOUTMSEC="{{ .GetConfigurationParameterAsMsec "kumori-ipfiltering" "timeout" }}"
IPFILTERING_MAXLISTSIZE="{{ .GetConfigurationParameter "kumori-ipfiltering" "maxListSize" }}"
IPFILTERING_LOGLEVEL="{{ .GetConfigurationParameter "kumori-ipfiltering" "loglevel" }}"

INSTALL_AUTHSERVICE="{{ .GetConfigurationParameter "kumori-authservice" "enabled" }}"


##############################
##  STORAGE CONFIGURATION   ##
##############################
#
# The following variables describe the different volume types that will be
# available to users. For each volumetype we specify:
# - a name
# - a StorageClass (used to provide volumes of that type)
# - a list of properties (a comma-separated list of words)

VOLUME_TYPE_NAMES=(
  {{- range $volumeTypeName := .GetVolumeTypeNames }}
  "{{$volumeTypeName}}"
  {{- end }}
)

VOLUME_TYPE_STORAGECLASSES=(
  {{- range $volumeTypeName := .GetVolumeTypeNames }}
  "{{ $.GetVolumeTypeStorageClass $volumeTypeName }}"
  {{- end }}
)

VOLUME_TYPE_PROPERTIES=(
  {{- range $volumeTypeName := .GetVolumeTypeNames }}
  {{- $volumeTypeProvider := $.GetConfigurationParameter "kumori-volume-controller" "volumeTypes" $volumeTypeName "provider" }}
  {{- $volumeTypeClass :=  $.GetConfigurationParameter "kumori-volume-controller" "volumeTypes" $volumeTypeName "class" }}
  {{- $volumeTypeProperties :=  $.GetConfigurationParameter $volumeTypeProvider "classes" $volumeTypeClass "properties" }}
  "{{ $volumeTypeProperties }}"
  {{- end }}
)


##############################################
##  OPENEBS STORAGE PROVIDER CONFIGURATION  ##
##############################################

# Install OpenEBS storage provider or not
INSTALL_OPENEBS_PROVIDER="{{ .GetConfigurationParameter "openebs" "enabled" }}"

# Node Device Manager: list of devices to ignore (don't manage these devices).
OPENEBS_STORAGE_IGNORED_DEVICES={{ (.GetConfigurationParameter "openebs" "ignoreDevices") | .CommaSeparatedToBashArray }}

#
# OPENEBS REPLICATED CSTOR CONFIGURATION
#
OPENEBS_STORAGE_REPLICATED_ENABLED="{{ .GetConfigurationParameter "openebs" "classes" "replicated" "enabled" }}"
# Replication level
OPENEBS_STORAGE_REPLICATED_REPLICATION_LEVEL="{{ .GetConfigurationParameter "openebs" "classes" "replicated" "configuration" "replicationLevel" }}"
# Disk devices to use
#
# For each Storage Node, a dedicated disk (clean, with no partitions)
OPENEBS_STORAGE_REPLICATED_DEVICES=(
  {{- $value := .GetConfigurationParameter "openebs" "classes" "replicated" "configuration" "device" }}
  {{- range $mkey, $machine := .Cluster.Storage.Nodes}}
  "{{ $value -}}"
  {{- end }}
)

#
# OPENEBS LOCAL PV HOSTPATH CONFIGURATION
#
OPENEBS_STORAGE_HOSTPATH_ENABLED="{{ .GetConfigurationParameter "openebs" "classes" "localhostpath" "enabled" }}"
# Directory tobe used as base dir in all the Storage Nodes.
# If the directory doesn't exist it will be created.
OPENEBS_STORAGE_HOSTPATH_DIR="{{ .GetConfigurationParameter "openebs" "classes" "localhostpath" "configuration" "hostBaseDir" }}"


#
# OPENEBS LOCAL PV LVM CONFIGURATION
#
OPENEBS_STORAGE_LVM_ENABLED="{{ .GetConfigurationParameter "openebs" "classes" "locallvm" "enabled" }}"
# LVM VolumeGroup to use. The LVM VolumeGroup must exist in all Storage Nodes.
OPENEBS_STORAGE_LVM_VOLUMEGROUP="{{ .GetConfigurationParameter "openebs" "classes" "locallvm" "configuration" "lvmVolumeGroup" }}"


###########################################################
##  OPENSTACK CSI CINDER STORAGE PROVIDER CONFIGURATION  ##
###########################################################
INSTALL_CSI_CINDER_PROVIDER="{{ .GetConfigurationParameter "csi-cinder" "enabled" }}"

CSI_CINDER_OPENSTACK_CLOUDS_YAML_BASE64="{{ .GetConfigurationParameter "csi-cinder" "cloudsYamlFile" | .FilePathToBase64String }}"
CSI_CINDER_OPENSTACK_CLOUDS_YAML_KEY="{{ .GetConfigurationParameter "csi-cinder" "user" }}"

# CSI Openstack Cinder classes details
CSI_CINDER_OPENSTACK_CLASS_NAMES=(
  {{- range $className := .GetConfigurationParameterKeys "csi-cinder" "classes" }}
  "{{ $className }}"
  {{- end }}
)
CSI_CINDER_OPENSTACK_CLASS_CONFIGURATIONS=(
  {{- range $className := .GetConfigurationParameterKeys "csi-cinder" "classes" }}
  '{{ $.GetStringifiedConfigurationParameter "csi-cinder" "classes" $className "configuration" }}'
  {{- end }}
)
CSI_CINDER_OPENSTACK_CLASS_PROPERTIES=(
  {{- range $className := .GetConfigurationParameterKeys "csi-cinder" "classes" }}
  '{{ $.GetStringifiedConfigurationParameter "csi-cinder" "classes" $className "properties" }}'
  {{- end }}
)


###########################################################
##       CSI NFS STORAGE PROVIDER CONFIGURATION          ##
###########################################################
INSTALL_CSI_NFS_PROVIDER="{{ .GetConfigurationParameter "csi-nfs" "enabled" }}"

# CSI NFS classes details
CSI_NFS_CLASS_NAMES=(
  {{- range $className := .GetConfigurationParameterKeys "csi-nfs" "classes" }}
  "{{ $className }}"
  {{- end }}
)
CSI_NFS_CLASS_CONFIGURATIONS=(
  {{- range $className := .GetConfigurationParameterKeys "csi-nfs" "classes" }}
  '{{ $.GetStringifiedConfigurationParameter "csi-nfs" "classes" $className "configuration" }}'
  {{- end }}
)
CSI_NFS_CLASS_PROPERTIES=(
  {{- range $className := .GetConfigurationParameterKeys "csi-nfs" "classes" }}
  '{{ $.GetStringifiedConfigurationParameter "csi-nfs" "classes" $className "properties" }}'
  {{- end }}
)


###########################################################
##      CSI CEPH RBD STORAGE PROVIDER CONFIGURATION      ##
###########################################################
INSTALL_CSI_CEPH_RBD_PROVIDER="{{ .GetConfigurationParameter "csi-ceph-rbd" "enabled" }}"

CSI_CEPH_RBD_HA="{{ .GetConfigurationParameter "csi-ceph-rbd" "HA" }}"

# Ceph clusters configuration
CSI_CEPH_RBD_CLUSTER_NAMES=(
  {{- range $cephClusterName := .GetConfigurationParameterKeys "csi-ceph-rbd" "cephClusters" }}
  "{{ $cephClusterName }}"
  {{- end }}
)
CSI_CEPH_RBD_CLUSTER_IDS=(
  {{- range $cephClusterName := .GetConfigurationParameterKeys "csi-ceph-rbd" "cephClusters" }}
  "{{ $.GetConfigurationParameter "csi-ceph-rbd" "cephClusters" $cephClusterName "clusterID" }}"
  {{- end }}
)
CSI_CEPH_RBD_CLUSTER_MONITORS=(
  {{- range $cephClusterName := .GetConfigurationParameterKeys "csi-ceph-rbd" "cephClusters" }}
  '{{ $.GetConfigurationParameter "csi-ceph-rbd" "cephClusters" $cephClusterName "monitors" }}'
  {{- end }}
)

# CSI Ceph RBD classes details
CSI_CEPH_RBD_CLASS_NAMES=(
  {{- range $className := .GetConfigurationParameterKeys "csi-ceph-rbd" "classes" }}
  "{{ $className }}"
  {{- end }}
)

CSI_CEPH_RBD_CLASS_CONFIGURATIONS=(
  {{- range $className := .GetConfigurationParameterKeys "csi-ceph-rbd" "classes" }}
  '{{ $.GetStringifiedConfigurationParameter "csi-ceph-rbd" "classes" $className "configuration" }}'
  {{- end }}
)

CSI_CEPH_RBD_CLASS_PROPERTIES=(
  {{- range $className := .GetConfigurationParameterKeys "csi-ceph-rbd" "classes" }}
  '{{ $.GetStringifiedConfigurationParameter "csi-ceph-rbd" "classes" $className "properties" }}'
  {{- end }}
)


####################
##     ADDONS     ##
####################
ADDONS_STORAGE_CLASS="openebs-localhostpath"

INSTALL_CALICO="{{ .GetConfigurationParameter "calico" "enabled" }}"

AMBASSADOR_XFF_TRUSTED_HOPS="{{ .GetConfigurationParameter "ambassador" "XFFTrustedHops" }}"
AMBASSADOR_APIEXT_HA="{{ .GetConfigurationParameter "ambassador" "apiext" "HA" }}"
AMBASSADOR_APIEXT_REPLICAS="{{ .GetConfigurationParameter "ambassador" "apiext" "HAReplicas" }}"

INSTALL_KUBE_PROMETHEUS="{{ .GetConfigurationParameter "kube-prometheus" "enabled" }}"
MONITORING_REQUIRE_CLIENTCERT="{{ .GetConfigurationParameter "kube-prometheus" "secureEndpoints" }}"
MONITORING_CLIENTCERT_TRUSTED_CA_FILE="{{ .GetConfigurationParameter "kube-prometheus" "trustedCACertFile" }}"
PROMETHEUS_PERSISTENCE="{{ .GetConfigurationParameter "kube-prometheus" "prometheus" "persistence" }}"
PROMETHEUS_REMOTE_READ_URL="{{ .GetConfigurationParameter "kube-prometheus" "prometheus" "remoteReadURL" }}"
PROMETHEUS_REMOTE_WRITE_URL="{{ .GetConfigurationParameter "kube-prometheus" "prometheus" "remoteWriteURL" }}"
PROMETHEUS_REMOTE_AUTH_TYPE="{{ .GetConfigurationParameter "kube-prometheus" "prometheus" "remoteAuthType" }}"
PROMETHEUS_REMOTE_CLIENT_CERTIFICATE_CERT_FILE="{{ .GetConfigurationParameter "kube-prometheus" "prometheus" "remoteClientCertificateCertFile" }}"
PROMETHEUS_REMOTE_CLIENT_CERTIFICATE_KEY_FILE="{{ .GetConfigurationParameter "kube-prometheus" "prometheus" "remoteClientCertificateKeyFile" }}"
GRAFANA_REMOTE_DATASOURCE_NAME="{{ .GetConfigurationParameter "kube-prometheus" "prometheus" "grafanaRemoteDatasourceName" }}"
PROMETHEUS_RETENTION_TIME="{{ .GetConfigurationParameter "kube-prometheus" "prometheus" "retentionTime" }}"
PROMETHEUS_ALLOWEDIP='{{ .GetConfigurationParameter "kube-prometheus" "prometheus" "allowedIP" }}'
PROMETHEUS_HA="{{ .GetConfigurationParameter "kube-prometheus" "prometheus" "HA" }}"
PROMETHEUS_HA_REPLICAS="{{ .GetConfigurationParameter "kube-prometheus" "prometheus" "HAReplicas" }}"

INSTALL_CADVISOR="{{ .GetConfigurationParameter "kube-prometheus" "cadvisor" "enabled" }}"

ALERTMANAGER_HA="{{ .GetConfigurationParameter "kube-prometheus" "alertmanager" "HA" }}"
ALERTMANAGER_HA_REPLICAS="{{ .GetConfigurationParameter "kube-prometheus" "alertmanager" "HAReplicas" }}"
ALERTMANAGER_ALLOWEDIP='{{ .GetConfigurationParameter "kube-prometheus" "alertmanager" "allowedIP" }}'

GRAFANA_HA="{{ .GetConfigurationParameter "kube-prometheus" "grafana" "HA" }}"
GRAFANA_HA_REPLICAS="{{ .GetConfigurationParameter "kube-prometheus" "grafana" "HAReplicas" }}"
GRAFANA_ADMIN_USERNAME="{{ .GetConfigurationParameter "kube-prometheus" "grafana" "adminUsername" }}"
GRAFANA_ADMIN_PASSWORD="{{ .GetConfigurationParameter "kube-prometheus" "grafana" "adminPassword" }}"
GRAFANA_VIEWER_USERNAME="{{ .GetConfigurationParameter "kube-prometheus" "grafana" "viewerUsername" }}"
GRAFANA_VIEWER_PASSWORD="{{ .GetConfigurationParameter "kube-prometheus" "grafana" "viewerPassword" }}"
# Configure OAuth role mapping to be strict or not (forbid access if user has no Grafana role)
GRAFANA_OAUTH_ROLE_STRICT="{{ .GetConfigurationParameter "kube-prometheus" "grafana" "strictRoles" }}"
# Enable 'cluster' selector in grafana Dasboards
GRAFANA_ENABLE_MULTICLUSTER_SUPPORT="{{ .GetConfigurationParameter "kube-prometheus" "grafana" "enableMultiClusterDashboards" }}"
GRAFANA_ALLOWEDIP='{{ .GetConfigurationParameter "kube-prometheus" "grafana" "allowedIP" }}'

INSTALL_ELASTICSEARCH="{{ .GetConfigurationParameter "elasticsearch" "enabled" }}"
ELASTICSEARCH_REPLICAS="{{ .GetConfigurationParameter "elasticsearch" "replicas" }}"
ELASTICSEARCH_VOLUME_SIZE="{{ .GetConfigurationParameter "elasticsearch" "volumeSize" }}"

INSTALL_KIBANA="{{ .GetConfigurationParameter "kibana" "enabled" }}"
KIBANA_ELASTICSEARCH_URL="{{ .GetConfigurationParameter "kibana" "elasticsearchURL" }}"
KIBANA_ELASTICSEARCH_USERNAME="{{ .GetConfigurationParameter "kibana" "elasticsearchUsername" }}"
KIBANA_ELASTICSEARCH_PASSWORD="{{ .GetConfigurationParameter "kibana" "elasticsearchPassword" }}"
KIBANA_ALLOWEDIP='{{ .GetConfigurationParameter "kibana" "allowedIP" }}'

INSTALL_FILEBEAT="{{ .GetConfigurationParameter "filebeat" "enabled" }}"
FILEBEAT_ELASTICSEARCH_URL="{{ .GetConfigurationParameter "filebeat" "elasticsearchURL" }}"
FILEBEAT_ELASTICSEARCH_USERNAME="{{ .GetConfigurationParameter "filebeat" "elasticsearchUsername" }}"
FILEBEAT_ELASTICSEARCH_PASSWORD="{{ .GetConfigurationParameter "filebeat" "elasticsearchPassword" }}"
FILEBEAT_INDEX_PATTERN="{{ .GetConfigurationParameter "filebeat" "indexPattern" }}"
FILEBEAT_ENABLE_USER_SERVICES_LOGS="{{ .GetConfigurationParameter "filebeat" "includeUserServicesLogs" }}"

INSTALL_KEYCLOAK="{{ .GetConfigurationParameter "keycloak" "enabled" }}"
KEYCLOAK_ADMIN_USERNAME="{{ .GetConfigurationParameter "keycloak" "adminUsername" }}"
KEYCLOAK_ADMIN_PASSWORD="{{ .GetConfigurationParameter "keycloak" "adminPassword" }}"
KEYCLOAK_ALLOWEDIP='{{ .GetConfigurationParameter "keycloak" "allowedIP" }}'
# Create UUIDs for configuring Keycloak Admission and Grafana clients secrets
KEYCLOAK_ADMISSION_CLIENT_SECRET="$(cat /proc/sys/kernel/random/uuid)"
KEYCLOAK_GRAFANA_CLIENT_SECRET="$(cat /proc/sys/kernel/random/uuid)"

INSTALL_KUBERNETES_DASHBOARD="false"

INSTALL_MINIO="{{ .GetConfigurationParameter "minio" "enabled" }}"
MINIO_ACCESS_KEY="{{ .GetConfigurationParameter "minio" "accessKey" }}"
MINIO_SECRET_KEY="{{ .GetConfigurationParameter "minio" "secretKey" }}"
MINIO_ALLOWEDIP='{{ .GetConfigurationParameter "minio" "allowedIP" }}'

INSTALL_ETCD_BACKUP="{{ .GetConfigurationParameter "etcdbackup" "enabled" }}"
ETCD_BACKUP_ETCD_ENDPOINT="{{ .GetConfigurationParameter "etcdbackup" "etcdEndpoint" }}"
ETCD_BACKUP_SCHEDULE="{{ .GetConfigurationParameter "etcdbackup" "schedule" }}"
ETCD_BACKUP_DELTA_PERIOD="{{ .GetConfigurationParameter "etcdbackup" "deltaPeriod" }}"
ETCD_BACKUP_S3_ENDPOINT="{{ .GetConfigurationParameter "etcdbackup" "s3Endpoint" }}"
ETCD_BACKUP_S3_REGION="{{ .GetConfigurationParameter "etcdbackup" "s3Region" }}"
ETCD_BACKUP_S3_ACCESS_KEY="{{ .GetConfigurationParameter "etcdbackup" "s3AccessKey" }}"
ETCD_BACKUP_S3_SECRET_KEY="{{ .GetConfigurationParameter "etcdbackup" "s3SecretKey" }}"
ETCD_BACKUP_S3_BUCKET="{{ .GetConfigurationParameter "etcdbackup" "s3Bucket" }}"
ETCD_BACKUP_STORAGE_PREFIX="{{ .GetConfigurationParameter "etcdbackup" "s3Prefix" }}"
ETCD_BACKUP_TRUSTED_CA_CONFIGMAP="{{ .GetConfigurationParameter "etcdbackup" "trustedCAConfigmap" }}"

PKI_BACKUP="{{ .GetConfigurationParameter "pkibackup" "enabled" }}"
PKI_BACKUP_S3_ENDPOINT="{{ .GetConfigurationParameter "pkibackup" "s3Endpoint" }}"
PKI_BACKUP_S3_REGION="{{ .GetConfigurationParameter "pkibackup" "s3Region" }}"
PKI_BACKUP_S3_ACCESS_KEY="{{ .GetConfigurationParameter "pkibackup" "s3AccessKey" }}"
PKI_BACKUP_S3_SECRET_KEY="{{ .GetConfigurationParameter "pkibackup" "s3SecretKey" }}"
PKI_BACKUP_S3_BUCKET="{{ .GetConfigurationParameter "pkibackup" "s3Bucket" }}"
PKI_BACKUP_STORAGE_PREFIX="{{ .GetConfigurationParameter "pkibackup" "s3Prefix" }}"

# ExternalDNS will only be installed if platform is in charge of DNS management.
# It will be configured according to the MANAGED_DNS_PROVIDER.
INSTALL_EXTERNALDNS="${MANAGED_DNS}"
EXTERNALDNS_OWNER_ID="kumori-cluster-${RELEASE_NAME}"
EXTERNALDNS_INTERVAL="{{ .GetConfigurationParameter "externaldns" "interval" }}"
# AWS Route 53 provider specific settings
EXTERNALDNS_ROUTE53_AWSBATCHCHANGESIZE="{{ .GetConfigurationParameter "externaldns" "route53" "awsBatchChangeSize" }}"
EXTERNALDNS_ROUTE53_AWSBATCHCHANGEINTERVAL="{{ .GetConfigurationParameter "externaldns" "route53" "awsBatchChangeInterval" }}"

OUTOFSERVICE_REGISTRY="{{ .GetConfigurationParameter "outofservice" "hub" }}"
OUTOFSERVICE_IMAGE="{{ .GetConfigurationParameter "outofservice" "image" }}"
OUTOFSERVICE_USERNAME="{{ .GetConfigurationParameter "outofservice" "username" }}"
OUTOFSERVICE_PASSWORD="{{ .GetConfigurationParameter "outofservice" "password" }}"


# Configure where platform events are stored.
# Supported store types:
# - 'k8s': events are stored in the Kubernetes cluster (no configuration required)
# - 'elasticsearch': events are stored in an external Elastcsearch database.
#
# Selecting an Elasticsearch store, will have the following effect:
# - Event-Exporter will be installed and configured to send all platform events to the
#   configured Elasticsearch server
# - Admission will be configured for using Elasticsearch as the source for platform Events
EVENTS_STORE_TYPE="{{ .GetEventStoreType }}"

# Deploy Event-Exporter in High Availability mode
EVENTS_EXPORTER_HA="{{ .GetConfigurationParameter "eventexporter" "HA" }}"
EVENTS_EXPORTER_HA_REPLICAS="{{ .GetConfigurationParameter "eventexporter" "HAReplicas" }}"
# Elasticsearch backend configuration used by Event-Exporter
EVENTS_ELASTICSEARCH_URL="{{ .GetConfigurationParameter "eventexporter" "eventStoreElasticsearchURL" }}"
EVENTS_ELASTICSEARCH_USERNAME="{{ .GetConfigurationParameter "eventexporter" "eventStoreElasticsearchUsername" }}"
EVENTS_ELASTICSEARCH_PASSWORD="{{ .GetConfigurationParameter "eventexporter" "eventStoreElasticsearchPassword" }}"
EVENTS_ELASTICSEARCH_INDEX_PREFIX="{{ .GetConfigurationParameter "eventexporter" "eventStoreElasticsearchIndexPrefix" }}"
EVENTS_ELASTICSEARCH_INDEX_DATE_PATTERN="{{ .GetConfigurationParameter "eventexporter" "eventStoreElasticsearchDatePattern" }}"

# Calculated from the above, do not edit
EVENTS_ELASTICSEARCH_INDEX="${EVENTS_ELASTICSEARCH_INDEX_PREFIX}-${EVENTS_ELASTICSEARCH_INDEX_DATE_PATTERN}"
EVENTS_ELASTICSEARCH_INDEX_ADMISSION="${EVENTS_ELASTICSEARCH_INDEX_PREFIX}-*"


# Determine if Helm must be installed. Currently only needed for installing EFK addon.
if [ "${INSTALL_FILEBEAT}" = "true" ] || [ "${INSTALL_ELASTICSEARCH}" = "true" ] || [ "${INSTALL_KIBANA}" = "true" ]
then
  INSTALL_HELM="true"
else
  INSTALL_HELM="false"
fi

# Configure the descheduler strategies
INSTALL_DESCHEDULER="{{ .GetConfigurationParameter "descheduler" "enabled" }}"
DESCHEDULER_NODE_SELECTOR="{{ .GetConfigurationParameter "descheduler" "nodeSelector" }}"
DESCHEDULER_INTERVAL="{{ .GetConfigurationParameter "descheduler" "interval" }}"
DESCHEDULER_SPREAD_ENABLED="{{ .GetConfigurationParameter "descheduler" "spreadConstraintsViolation" "enabled" }}"
DESCHEDULER_SPREAD_PRIORITY_CLASS="{{ .GetConfigurationParameter "descheduler" "spreadConstraintsViolation" "priorityClass" }}"
DESCHEDULER_LOW_ENABLED="{{ .GetConfigurationParameter "descheduler" "lowNodeUtilization" "enabled" }}"
DESCHEDULER_LOW_PRIORITY_CLASS="{{ .GetConfigurationParameter "descheduler" "lowNodeUtilization" "priorityClass" }}"
DESCHEDULER_LOW_THRESHOLD_CPU="{{ .GetConfigurationParameter "descheduler" "lowNodeUtilization" "threshold" "cpu" }}"
DESCHEDULER_LOW_THRESHOLD_MEM="{{ .GetConfigurationParameter "descheduler" "lowNodeUtilization" "threshold" "memory" }}"
DESCHEDULER_LOW_THRESHOLD_POD="{{ .GetConfigurationParameter "descheduler" "lowNodeUtilization" "threshold" "pods" }}"
DESCHEDULER_LOW_TARGET_CPU="{{ .GetConfigurationParameter "descheduler" "lowNodeUtilization" "target" "cpu" }}"
DESCHEDULER_LOW_TARGET_MEM="{{ .GetConfigurationParameter "descheduler" "lowNodeUtilization" "target" "memory" }}"
DESCHEDULER_LOW_TARGET_POD="{{ .GetConfigurationParameter "descheduler" "lowNodeUtilization" "target" "pods" }}"

# Install CertManager
INSTALL_CERT_MANAGER="{{ .GetConfigurationParameter "certmanager" "enabled" }}"

# Install CertManager Webhook OVH
INSTALL_CERT_MANAGER_WEBHOOK_OVH="{{ .GetConfigurationParameter "certmanager-webhook-ovh" "enabled" }}"
CERT_MANAGER_WEBHOOK_OVH_CREDENTIALS_BASE64="{{ .GetConfigurationParameter "certmanager-webhook-ovh" "ovhDnsCredentialsFile" | .FilePathToBase64String }}"
CERT_MANAGER_WEBHOOK_OVH_EMAIL="{{ .GetConfigurationParameter "certmanager-webhook-ovh" "certNotificationsEmail" }}"
CERT_MANAGER_WEBHOOK_OVH_ISSUER_LETSENCRYPT_ENABLED="{{ .GetConfigurationParameter "certmanager-webhook-ovh" "issuers" "letsEncrypt" "enabled" }}"
CERT_MANAGER_WEBHOOK_OVH_ISSUER_LETSENCRYPT_USE_STAGING="{{ .GetConfigurationParameter "certmanager-webhook-ovh" "issuers" "letsEncrypt" "useStaging" }}"
CERT_MANAGER_WEBHOOK_OVH_ISSUER_ZEROSSL_ENABLED="{{ .GetConfigurationParameter "certmanager-webhook-ovh" "issuers" "zeroSsl" "enabled" }}"
CERT_MANAGER_WEBHOOK_OVH_ISSUER_ZEROSSL_KEY_ID="{{ .GetConfigurationParameter "certmanager-webhook-ovh" "issuers" "zeroSsl" "keyId" }}"
CERT_MANAGER_WEBHOOK_OVH_ISSUER_ZEROSSL_EABKEYHMAC="{{ .GetConfigurationParameter "certmanager-webhook-ovh" "issuers" "zeroSsl" "eabKeyHmac" }}"

################################
##     ADDONS - CLUSTERAPI    ##
################################
INSTALL_CLUSTERAPI="{{ .GetConfigurationParameter "clusterapi" "enabled" }}"
if [ "${INSTALL_CLUSTERAPI}" = "true" ]
then
  # CertManager is a requirement for running ClusterAPI
  INSTALL_CERT_MANAGER="true"
fi

INSTALL_CLUSTERAPI_INFRASTRUCTURE_PROVIDER_OPENSTACK="{{ .GetConfigurationParameter "clusterapi" "providers" "infrastructureOpenstack" "enabled" }}"
INSTALL_CLUSTERAPI_INFRASTRUCTURE_PROVIDER_AWS="{{ .GetConfigurationParameter "clusterapi" "providers" "infrastructureAws" "enabled" }}"
if [ "${INSTALL_CLUSTERAPI}" = "false" ]
then
  # ClusterAPI providers require ClusterAPI to be installed
  INSTALL_CLUSTERAPI_INFRASTRUCTURE_PROVIDER_OPENSTACK="false"
	INSTALL_CLUSTERAPI_INFRASTRUCTURE_PROVIDER_AWS="false"
fi

##################################################
##     ADDONS - IAAS-SPECIFIC CLOUD-MANAGERS    ##
##################################################
INSTALL_OPENSTACK_CLOUD_CONTROLLER="{{ .GetConfigurationParameter "openstackcloudcontroller" "enabled" }}"
if [ "${KUMORI_IAAS}" = "openstack" ]
then
  INSTALL_OPENSTACK_CLOUD_CONTROLLER="true"
fi
OPENSTACK_CLOUD_CONTROLLER_CLOUDS_YAML_FILE="{{ .GetConfigurationParameter "openstackcloudcontroller" "cloudsYamlFile" }}"
OPENSTACK_CLOUD_CONTROLLER_CLOUDS_YAML_KEY="{{ .GetConfigurationParameter "openstackcloudcontroller" "cloudsYamlKey" }}"

INSTALL_AWS_CLOUD_CONTROLLER="{{ .GetConfigurationParameter "awscloudcontroller" "enabled" }}"
if [ "${KUMORI_IAAS}" = "aws" ]
then
  INSTALL_AWS_CLOUD_CONTROLLER="true"
fi

################################################################################
## RESOURCES ASSIGNED TO PLATFORM ELEMENTS                                     #
################################################################################

# CoreDNS
COREDNS_CPU_REQUEST="{{ .GetResourceParameter "kumori-coredns" "coredns" "cpu" }}"
COREDNS_CPU_LIMIT="{{ .GetResourceParameter "kumori-coredns" "coredns" "cpuLimit" }}"
COREDNS_MEM_REQUEST="{{ .GetResourceParameter "kumori-coredns" "coredns" "mem" }}"
COREDNS_MEM_LIMIT="{{ .GetResourceParameter "kumori-coredns" "coredns" "memLimit" }}"

# Ambassador
AMBASSADOR_CPU_REQUEST="{{ .GetResourceParameter "ambassador" "ambassador" "cpu" }}"
AMBASSADOR_CPU_LIMIT="{{ .GetResourceParameter "ambassador" "ambassador" "cpuLimit" }}"
AMBASSADOR_MEM_REQUEST="{{ .GetResourceParameter "ambassador" "ambassador" "mem" }}"
AMBASSADOR_MEM_LIMIT="{{ .GetResourceParameter "ambassador" "ambassador" "memLimit" }}"
AMBASSADOR_APIEXT_CPU_REQUEST="{{ .GetResourceParameter "ambassador" "apiext" "cpu" }}"
AMBASSADOR_APIEXT_CPU_LIMIT="{{ .GetResourceParameter "ambassador" "apiext" "cpuLimit" }}"
AMBASSADOR_APIEXT_MEM_REQUEST="{{ .GetResourceParameter "ambassador" "apiext" "mem" }}"
AMBASSADOR_APIEXT_MEM_LIMIT="{{ .GetResourceParameter "ambassador" "apiext" "memLimit" }}"

# Prometheus
PROMETHEUS_CPU_REQUEST="{{ .GetResourceParameter "kube-prometheus" "prometheus" "cpu" }}"
PROMETHEUS_CPU_LIMIT="{{ .GetResourceParameter "kube-prometheus" "prometheus" "cpuLimit" }}"
PROMETHEUS_MEM_REQUEST="{{ .GetResourceParameter "kube-prometheus" "prometheus" "mem" }}"
PROMETHEUS_MEM_LIMIT="{{ .GetResourceParameter "kube-prometheus" "prometheus" "memLimit" }}"

# Prometheus Config Reloader
PROMETHEUS_CONFIG_RELOADER_CPU_REQUEST="{{ .GetResourceParameter "kube-prometheus" "configreloader" "cpu" }}"
PROMETHEUS_CONFIG_RELOADER_CPU_LIMIT="{{ .GetResourceParameter "kube-prometheus" "configreloader" "cpuLimit" }}"
PROMETHEUS_CONFIG_RELOADER_MEM_REQUEST="{{ .GetResourceParameter "kube-prometheus" "configreloader" "mem" }}"
PROMETHEUS_CONFIG_RELOADER_MEM_LIMIT="{{ .GetResourceParameter "kube-prometheus" "configreloader" "memLimit" }}"

# AlertManager
ALERTMANAGER_CPU_REQUEST="{{ .GetResourceParameter "kube-prometheus" "alertmanager" "cpu" }}"
ALERTMANAGER_CPU_LIMIT="{{ .GetResourceParameter "kube-prometheus" "alertmanager" "cpuLimit" }}"
ALERTMANAGER_MEM_REQUEST="{{ .GetResourceParameter "kube-prometheus" "alertmanager" "mem" }}"
ALERTMANAGER_MEM_LIMIT="{{ .GetResourceParameter "kube-prometheus" "alertmanager" "memLimit" }}"

# Grafana
GRAFANA_CPU_REQUEST="{{ .GetResourceParameter "kube-prometheus" "grafana" "cpu" }}"
GRAFANA_CPU_LIMIT="{{ .GetResourceParameter "kube-prometheus" "grafana" "cpuLimit" }}"
GRAFANA_MEM_REQUEST="{{ .GetResourceParameter "kube-prometheus" "grafana" "mem" }}"
GRAFANA_MEM_LIMIT="{{ .GetResourceParameter "kube-prometheus" "grafana" "memLimit" }}"

# CAdvisor
CADVISOR_CPU_REQUEST="{{ .GetResourceParameter "kube-prometheus" "cadvisor" "cpu" }}"
CADVISOR_CPU_LIMIT="{{ .GetResourceParameter "kube-prometheus" "cadvisor" "cpuLimit" }}"
CADVISOR_MEM_REQUEST="{{ .GetResourceParameter "kube-prometheus" "cadvisor" "mem" }}"
CADVISOR_MEM_LIMIT="{{ .GetResourceParameter "kube-prometheus" "cadvisor" "memLimit" }}"

# Filebeat
FILEBEAT_CPU_REQUEST="{{ .GetResourceParameter "filebeat" "filebeat" "cpu" }}"
FILEBEAT_CPU_LIMIT="{{ .GetResourceParameter "filebeat" "filebeat" "cpuLimit" }}"
FILEBEAT_MEM_REQUEST="{{ .GetResourceParameter "filebeat" "filebeat" "mem" }}"
FILEBEAT_MEM_LIMIT="{{ .GetResourceParameter "filebeat" "filebeat" "memLimit" }}"

# Keycloak
KEYCLOAK_CPU_REQUEST="{{ .GetResourceParameter "keycloak" "keycloak" "cpu" }}"
KEYCLOAK_CPU_LIMIT="{{ .GetResourceParameter "keycloak" "keycloak" "cpuLimit" }}"
KEYCLOAK_MEM_REQUEST="{{ .GetResourceParameter "keycloak" "keycloak" "mem" }}"
KEYCLOAK_MEM_LIMIT="{{ .GetResourceParameter "keycloak" "keycloak" "memLimit" }}"

# OPENEBS
OPENEBS_REPLICATED_CPU_REQUEST="{{ .GetResourceParameter "openebs" "replicated" "cpu" }}"
OPENEBS_REPLICATED_CPU_LIMIT="{{ .GetResourceParameter "openebs" "replicated" "cpuLimit" }}"
OPENEBS_REPLICATED_MEM_REQUEST="{{ .GetResourceParameter "openebs" "replicated" "mem" }}"
OPENEBS_REPLICATED_MEM_LIMIT="{{ .GetResourceParameter "openebs" "replicated" "memLimit" }}"
OPENEBS_REPLICATED_AUX_CPU_REQUEST="{{ .GetResourceParameter "openebs" "auxReplicated" "cpu" }}"
OPENEBS_REPLICATED_AUX_CPU_LIMIT="{{ .GetResourceParameter "openebs" "auxReplicated" "cpuLimit" }}"
OPENEBS_REPLICATED_AUX_MEM_REQUEST="{{ .GetResourceParameter "openebs" "auxReplicated" "mem" }}"
OPENEBS_REPLICATED_AUX_MEM_LIMIT="{{ .GetResourceParameter "openebs" "auxReplicated" "memLimit" }}"

# DESCHEDULER
DESCHEDULER_CPU_REQUEST="{{ .GetResourceParameter "descheduler" "descheduler" "cpu" }}"
DESCHEDULER_CPU_LIMIT="{{ .GetResourceParameter "descheduler" "descheduler" "cpuLimit" }}"
DESCHEDULER_MEM_REQUEST="{{ .GetResourceParameter "descheduler" "descheduler" "mem" }}"
DESCHEDULER_MEM_LIMIT="{{ .GetResourceParameter "descheduler" "descheduler" "memLimit" }}"

# IpFiltering (ingress plugin)
IPFILTERING_CPU_REQUEST="{{ .GetResourceParameter "kumori-ipfiltering" "ipfiltering" "cpu" }}"
IPFILTERING_CPU_LIMIT="{{ .GetResourceParameter "kumori-ipfiltering" "ipfiltering" "cpuLimit" }}"
IPFILTERING_MEM_REQUEST="{{ .GetResourceParameter "kumori-ipfiltering" "ipfiltering" "mem" }}"
IPFILTERING_MEM_LIMIT="{{ .GetResourceParameter "kumori-ipfiltering" "ipfiltering" "memLimit" }}"

# AuthService (ingress plugin)
AUTHSERVICE_CPU_REQUEST="{{ .GetResourceParameter "kumori-authservice" "authservice" "cpu" }}"
AUTHSERVICE_CPU_LIMIT="{{ .GetResourceParameter "kumori-authservice" "authservice" "cpuLimit" }}"
AUTHSERVICE_MEM_REQUEST="{{ .GetResourceParameter "kumori-authservice" "authservice" "mem" }}"
AUTHSERVICE_MEM_LIMIT="{{ .GetResourceParameter "kumori-authservice" "authservice" "memLimit" }}"

`
