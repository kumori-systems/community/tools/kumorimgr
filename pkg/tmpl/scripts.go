/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package tmpl

// TODO: TO IMPROVE!!! How manage better? Gitlab repository with templates?

const ScriptsVarsTemplate = `
#!/bin/bash

################################################################################
## CLUSTER MACHINES IP ADDRESSES AND CONNECTIVITY                             ##
################################################################################

# MASTERS
MASTERS_IPS=(
  {{- range $mkey, $machine := .Cluster.Machines}}
    {{- range $lkey, $label := $machine.Labels}}
      {{- if eq $label.Noderole "master"}}
  "{{$machine.IP}}"
      {{- end }}
    {{- end }}
  {{- end }}
)

# MASTERS ZONES
MASTERS_ZONES=(
  {{- $zones := .Cluster.Zones}}
  {{- $numzones := 0}}
  {{- if .Cluster.Zones}}
    {{- $numzones = len .Cluster.Zones}}
  {{- end}}
  {{- range $mkey, $machine := .Cluster.Machines}}
    {{- range $lkey, $label := $machine.Labels}}
      {{- if eq $label.Noderole "master"}}
        {{- $thezone := ""}}
        {{- if gt $numzones 0}}
          {{- range $zkey, $zone := $zones}}
            {{- range $mkey2, $machine2 := $zone.Nodes}}
              {{- if eq $machine2.IP $machine.IP}}
                {{- $thezone = $zkey}}
                {{- break}}
              {{- end}}
            {{- end}}
          {{- end}}
        {{- end}}
  "{{$thezone}}"
      {{- end }}
    {{- end }}
  {{- end }}
)

# WORKERS
WORKERS_IPS=(
  {{- range $mkey, $machine := .Cluster.Machines}}
    {{- range $lkey, $label := $machine.Labels}}
      {{- if eq $label.Noderole "worker"}}
  "{{$machine.IP}}"
      {{- end }}
    {{- end }}
  {{- end }}
)

# WORKERS ZONES
WORKERS_ZONES=(
  {{- $zones := .Cluster.Zones}}
  {{- $numzones := 0}}
  {{- if .Cluster.Zones}}
    {{- $numzones = len .Cluster.Zones}}
  {{- end}}
  {{- range $mkey, $machine := .Cluster.Machines}}
    {{- range $lkey, $label := $machine.Labels}}
      {{- if eq $label.Noderole "worker"}}
        {{- $thezone := ""}}
        {{- if gt $numzones 0}}
          {{- range $zkey, $zone := $zones}}
            {{- range $mkey2, $machine2 := $zone.Nodes}}
              {{- if eq $machine2.IP $machine.IP}}
                {{- $thezone = $zkey}}
                {{- break}}
              {{- end}}
            {{- end}}
          {{- end}}
        {{- end}}
  "{{$thezone}}"
      {{- end }}
    {{- end }}
  {{- end }}
)

# Nodes that will be Ingress entrypoints
INGRESS_NODES_IPS=(
  {{- range $mkey, $machine := .Cluster.Ingress.Nodes}}
  "{{$machine.IP}}"
  {{- end }}
)

# Nodes that will be providing Storage
STORAGE_NODES_IPS=(
  {{- range $mkey, $machine := .Cluster.Storage.Nodes}}
  "{{$machine.IP}}"
  {{- end }}
)

# Nodes in maintenance mode
MAINTENANCE_MASTERS_IPS=(
  {{- range $mkey, $machine := .Cluster.Machines}}
    {{- range $lkey, $label := $machine.Labels}}
      {{- if and ($label.Maintenance) (eq $label.Noderole "master")}}
  "{{$machine.IP}}"
      {{- end }}
    {{- end }}
  {{- end }}
)

MAINTENANCE_WORKERS_IPS=(
  {{- range $mkey, $machine := .Cluster.Machines}}
    {{- range $lkey, $label := $machine.Labels}}
      {{- if and ($label.Maintenance) (eq $label.Noderole "worker")}}
  "{{$machine.IP}}"
      {{- end }}
    {{- end }}
  {{- end }}
)

# CLUSTER UPDATE DEFINITIONS
ADD_MASTERS_IPS=(
  {{- range .UpdateParams.AddMastersIPs}}
  "{{.}}"
  {{- end }}
)
ADD_WORKERS_IPS=(
  {{- range .UpdateParams.AddWorkersIPs}}
  "{{.}}"
  {{- end }}
)
REMOVE_MASTERS_IPS=(
  {{- range .UpdateParams.RemoveMastersIPs}}
  "{{.}}"
  {{- end }}
)
REMOVE_WORKERS_IPS=(
  {{- range .UpdateParams.RemoveWorkersIPs}}
  "{{.}}"
  {{- end }}
)

# Disable TX offload on these nodes
TX_OFF_NODES_IPS=(
  {{- range $mkey, $machine := .Cluster.Machines}}
    {{- range $lkey, $label := $machine.Labels}}
      {{- if $label.DisableTxOffload}}
  "{{$machine.IP}}"
      {{- end }}
    {{- end }}
  {{- end }}
)

# List of Storage nodes to add
ADD_STORAGE_IPS=(
  {{- range .UpdateParams.AddStorageIPs}}
  "{{.}}"
  {{- end }}
)

# List of Storage nodes to remove
REMOVE_STORAGE_IPS=(
  {{- range .UpdateParams.RemoveStorageIPs}}
  "{{.}}"
  {{- end }}
)

# List of Master nodes to put into maintenance
ADD_MAINTENANCE_MASTERS_IPS=(
  {{- range .UpdateParams.AddMaintenanceMastersIPs}}
  "{{.}}"
  {{- end }}
)

# List of Worker nodes to put into maintenance
ADD_MAINTENANCE_WORKERS_IPS=(
  {{- range .UpdateParams.AddMaintenanceWorkersIPs}}
  "{{.}}"
  {{- end }}
)

# List of Master nodes to get back from maintenance
REMOVE_MAINTENANCE_MASTERS_IPS=(
  {{- range .UpdateParams.RemoveMaintenanceMastersIPs}}
  "{{.}}"
  {{- end }}
)

# List of Worker nodes to get back from maintenance
REMOVE_MAINTENANCE_WORKERS_IPS=(
  {{- range .UpdateParams.RemoveMaintenanceWorkersIPs}}
  "{{.}}"
  {{- end }}
)

# SSH CONNECTION DETAILS
SSH_USER="{{.Cluster.SSH.User}}"
SSH_KEY="{{.Cluster.SSH.PrivateKeyPath}}"

# BASIC CONFIGURATION
DEFAULT_LOCALE="es_ES.UTF-8"
TIMEZONE="CET"

################################################################################
## INSTALLATION CUSTOMIZATION PROPERTIES                                      ##
################################################################################

CLUSTER_VERSION="{{.GetDistributionVersion}}"
RELEASE_NAME="{{.Cluster.Name}}"
REFERENCE_DOMAIN="{{.Cluster.DNS.ReferenceDomain}}"
REFERENCE_DOMAIN_CERT_DIR="{{.Cluster.DNS.ReferenceDomainCertDir}}"

# Kumori PKI: RootCA
CLUSTER_PKI_ROOT_CA_FILE="{{.Cluster.ClusterPki.RootCaFile}}"

KUMORICTL_SUPPORTED_VERSIONS={{ (.GetConfigurationParameter "kumori-admission" "kumorictlSupportedVersions") | .CommaSeparatedToBashArray }}

KUMORIMGR_SUPPORTED_VERSIONS={{ (.GetConfigurationParameter "kumori-admission" "kumorimgrSupportedVersions") | .CommaSeparatedToBashArray }}

#######################
##     APISERVER     ##
#######################

# APISERVER - GENERAL SETTINGS
#
# ApiServer Endpoint configuration (ApiServer domain, LB IP or master IP)
APISERVER_DOMAIN="apiserver-${RELEASE_NAME}.${REFERENCE_DOMAIN}"
APISERVER_EXTERNAL_PORT={{.GetAPIServerPort}}
# Domains or IPs to be added as accepted in ApiServer TLS certificate
# If several, set as comma separated string list (no spaces)
APISERVER_CERT_EXTRA_SANS=""

# APISERVER - MANAGE APISERVER DNS DOMAIN
#
# If enabled, the folowing configuration settings are mandatory:
# - APISERVER_DNS_IP / APISERVER_DNS_CNAME : the IP or CNAME to be registered in DNS for ApiServer domain
# - APISERVER_DOMAIN_TTL
APISERVER_REGISTER_DOMAIN="{{.Cluster.DNS.Management.Enabled}}"
APISERVER_DNS_IP={{.GetAPIServerExternalIP}}
APISERVER_DNS_CNAME=""
APISERVER_DNS_TTL="300"

# APISERVER - INTERNAL BALANCING
#
# Install an internal load-balancer in every Master node to balance ApiServer
# requests among all endpoints.
# Internal load-balancers are automatically configured to listen on APISERVER_EXTERNAL_PORT
# and their target list includes all Master nodes at ApiServer on APISERVER_INTERNAL_PORT.
APISERVER_INTERNAL_BALANCING={{.GetAPIServerInternalBalancing}}
APISERVER_AUTO_BALANCING="false"

# ApiServer external port (used by clients) depends on wether internal load
# balancing is enabled or not.
# ApiServer internal port is fixed and currently not configurable (6443).
if [ "${APISERVER_INTERNAL_BALANCING}" = "true" ]; then
  # If balancing is internal, ignore external port set by operator
  APISERVER_EXTERNAL_PORT="8443"
  APISERVER_INTERNAL_PORT="6443"
else
  # If balancing isn't internal use external port set by operator or default
  APISERVER_EXTERNAL_PORT="${APISERVER_EXTERNAL_PORT:-6443}"
  APISERVER_INTERNAL_PORT="6443"
fi

# APISERVER - HANDLE FLOATING IP
#
# User provides a Virtual IP for ApiServer, and the platform is in charge of ensuring
# that IP is always assigned to a healthy Master node.
# If enabled, the folowing configuration settings are mandatory:
# - APISERVER_VIRTUAL_IP           (user provided)
APISERVER_HANDLE_FLOATING_IP={{.GetAPIServerHandleFloatingIP}}
APISERVER_VIRTUAL_IP={{.GetAPIServerInternalIP}}

# APISERVER URL - used for publishing ApiServer to the outside
APISERVER_URL="${APISERVER_DOMAIN}:${APISERVER_EXTERNAL_PORT}"

# Virtual Router ID for ApiServer Keepalived configuration (optional because we use unicast)
APISERVER_VRID={{.GetAPIServerVRID}}

#####################
##     INGRESS     ##
#####################
# INGRESS - GENERAL SETTINGS
#
# Ingress domain
INGRESS_DOMAIN="ingress-${RELEASE_NAME}.${REFERENCE_DOMAIN}"
# Namespace where the Ingress controller will be deployed
INGRESS_NAMESPACE="kumori"

# Format (PEM/XFFC) for the client-certificate header
INGRESS_CERTHEADER_FORMAT="{{ .GetIngressCertHeaderFormat }}"

# INGRESS - INTERNAL BALANCING
#
# Install an internal load-balancer in every Ingress node to balance Ingress
# requests among all endpoints.
# Internal load-balancers are automatically configured to listen on ports INGRESS_EXTERNAL_PORT
# (always 443) and their target list includes all Ingress nodes on INGRESS_INTERNAL_PORT (8443).
INGRESS_INTERNAL_BALANCING={{.GetIngressInternalBalancing}}
INGRESS_AUTO_BALANCING="false"

# Internal Ingress HTTPS port (where Ingress pods listen on host network) depends on
# whether internal load balancing is enabled or not.
# External Ingress HTTPS port will always be 443.
if [ "${INGRESS_INTERNAL_BALANCING}" = "true" ]; then
  INGRESS_EXTERNAL_PORT="443"
  INGRESS_INTERNAL_PORT="8443"
else
  INGRESS_EXTERNAL_PORT="443"
  INGRESS_INTERNAL_PORT="443"
fi

# List of ports that ingress will listen to for TCP connections.
INGRESS_TCP_PORTS=(
  {{- range $mkey, $tcpPort := .Cluster.Ingress.TcpPorts}}
  {{$tcpPort}}
  {{- end }}
)

# Assign an internal port to each TCP port. If internal load balancing is disabled, the internal
# and external ports will be the same.
INGRESS_TCP_INTERNAL_PORTS=()
if [ "${INGRESS_INTERNAL_BALANCING}" = "true" ]; then
  for PORT in ${INGRESS_TCP_PORTS[@]}; do
    # Currently allowed TCP ports are in the range 9000-9999, so we will just add
    # 10000 to convert them to internal: 19000-19999
    INGRESS_TCP_INTERNAL_PORTS+=("1${PORT}")
  done
else
  # Simply copy the original TCP port list
  INGRESS_TCP_INTERNAL_PORTS=( "${INGRESS_TCP_PORTS[@]}" )
fi

# INGRESS - HANDLE FLOATING IP
#
# User provides a Virtual IP for Ingress, and the platform is in charge of ensuring
# that IP is always assigned to a healthy Ingress node.
# If enabled, the folowing configuration settings are mandatory:
# - INGRESS_VIRTUAL_IP           (user provided)
INGRESS_HANDLE_FLOATING_IP={{.GetIngressHandleFloatingIP}}
INGRESS_VIRTUAL_IP={{.GetIngressInternalIP}}

# INGRESS - MANAGE INGRESS DNS DOMAIN
#
# If enabled, the folowing configuration settings are mandatory:
# - INGRESS_DOMAIN
# - INGRESS_DNS_IP / INGRESS_DNS_CNAME: the IP or CNAME to be registered in DNS for ingress domain
# - INGRESS_DOMAIN_TTL
INGRESS_REGISTER_DOMAIN="{{.Cluster.DNS.Management.Enabled}}"
INGRESS_DNS_IP={{.GetIngressExternalIP}}
INGRESS_DNS_CNAME=""
INGRESS_DNS_TTL="300"

# Virtual Router ID for Ingress Keepalived configuration (optional because we use unicast)
INGRESS_VRID={{.GetIngressVRID}}

##############################
##  STORAGE CONFIGURATION   ##
##############################
#
# The following variables describe the different volume types that will be
# available to users. For each volumetype we specify:
# - a name
# - a StorageClass (used to provide volumes of that type)
# - a list of properties (a comma-separated list of words)

VOLUME_TYPE_NAMES=(
  {{- range $volumeTypeName := .GetVolumeTypeNames }}
  "{{$volumeTypeName}}"
  {{- end }}
)

VOLUME_TYPE_STORAGECLASSES=(
  {{- range $volumeTypeName := .GetVolumeTypeNames }}
  "{{ $.GetVolumeTypeStorageClass $volumeTypeName }}"
  {{- end }}
)

VOLUME_TYPE_PROPERTIES=(
  {{- range $volumeTypeName := .GetVolumeTypeNames }}
  {{- $volumeTypeProvider := $.GetConfigurationParameter "kumori-volume-controller" "volumeTypes" $volumeTypeName "provider" }}
  {{- $volumeTypeClass :=  $.GetConfigurationParameter "kumori-volume-controller" "volumeTypes" $volumeTypeName "class" }}
  {{- $volumeTypeProperties :=  $.GetConfigurationParameter $volumeTypeProvider "classes" $volumeTypeClass "properties" }}
  "{{ $volumeTypeProperties }}"
  {{- end }}
)

##############################################
##  OPENEBS STORAGE PROVIDER CONFIGURATION  ##
##############################################

# Install OpenEBS storage provider or not
INSTALL_OPENEBS_PROVIDER="{{ .GetConfigurationParameter "openebs" "enabled" }}"

# Node Device Manager: list of devices to ignore (don't manage these devices).
OPENEBS_STORAGE_IGNORED_DEVICES={{ (.GetConfigurationParameter "openebs" "ignoreDevices") | .CommaSeparatedToBashArray }}

#
# OPENEBS REPLICATED CSTOR CONFIGURATION
#
OPENEBS_STORAGE_REPLICATED_ENABLED="{{ .GetConfigurationParameter "openebs" "classes" "replicated" "enabled" }}"
# Replication level
OPENEBS_STORAGE_REPLICATED_REPLICATION_LEVEL="{{ .GetConfigurationParameter "openebs" "classes" "replicated" "configuration" "replicationLevel" }}"
# Disk devices to use
#
# For each Storage Node, a dedicated disk (clean, with no partitions)
OPENEBS_STORAGE_REPLICATED_DEVICES=(
  {{- $value := .GetConfigurationParameter "openebs" "classes" "replicated" "configuration" "device" }}
  {{- range $mkey, $machine := .Cluster.Storage.Nodes}}
  "{{ $value -}}"
  {{- end }}
)

#
# OPENEBS LOCAL PV HOSTPATH CONFIGURATION
#
OPENEBS_STORAGE_HOSTPATH_ENABLED="{{ .GetConfigurationParameter "openebs" "classes" "localhostpath" "enabled" }}"
# Directory tobe used as base dir in all the Storage Nodes.
# If the directory doesn't exist it will be created.
OPENEBS_STORAGE_HOSTPATH_DIR="{{ .GetConfigurationParameter "openebs" "classes" "localhostpath" "configuration" "hostBaseDir" }}"


#
# OPENEBS LOCAL PV LVM CONFIGURATION
#
OPENEBS_STORAGE_LVM_ENABLED="{{ .GetConfigurationParameter "openebs" "classes" "locallvm" "enabled" }}"
# LVM VolumeGroup to use. The LVM VolumeGroup must exist in all Storage Nodes.
OPENEBS_STORAGE_LVM_VOLUMEGROUP="{{ .GetConfigurationParameter "openebs" "classes" "locallvm" "configuration" "lvmVolumeGroup" }}"


###########################################################
##  OPENSTACK CSI CINDER STORAGE PROVIDER CONFIGURATION  ##
###########################################################
INSTALL_CSI_CINDER_PROVIDER="{{ .GetConfigurationParameter "csi-cinder" "enabled" }}"

CSI_CINDER_OPENSTACK_CLOUDS_YAML_BASE64="{{ .GetConfigurationParameter "csi-cinder" "cloudsYamlFile" | .FilePathToBase64String }}"
CSI_CINDER_OPENSTACK_CLOUDS_YAML_KEY="{{ .GetConfigurationParameter "csi-cinder" "cloudsYamlKey" }}"

# CSI Openstack Cinder classes details
CSI_CINDER_OPENSTACK_CLASS_NAMES=(
  {{- range $className := .GetConfigurationParameterKeys "csi-cinder" "classes" }}
  "{{ $className }}"
  {{- end }}
)
CSI_CINDER_OPENSTACK_CLASS_CONFIGURATIONS=(
  {{- range $className := .GetConfigurationParameterKeys "csi-cinder" "classes" }}
  '{{ $.GetStringifiedConfigurationParameter "csi-cinder" "classes" $className "configuration" }}'
  {{- end }}
)
CSI_CINDER_OPENSTACK_CLASS_PROPERTIES=(
  {{- range $className := .GetConfigurationParameterKeys "csi-cinder" "classes" }}
  '{{ $.GetStringifiedConfigurationParameter "csi-cinder" "classes" $className "properties" }}'
  {{- end }}
)


###########################################################
##       CSI NFS STORAGE PROVIDER CONFIGURATION          ##
###########################################################
INSTALL_CSI_NFS_PROVIDER="{{ .GetConfigurationParameter "csi-nfs" "enabled" }}"

# CSI NFS classes details
CSI_NFS_CLASS_NAMES=(
  {{- range $className := .GetConfigurationParameterKeys "csi-nfs" "classes" }}
  "{{ $className }}"
  {{- end }}
)
CSI_NFS_CLASS_CONFIGURATIONS=(
  {{- range $className := .GetConfigurationParameterKeys "csi-nfs" "classes" }}
  '{{ $.GetStringifiedConfigurationParameter "csi-nfs" "classes" $className "configuration" }}'
  {{- end }}
)
CSI_NFS_CLASS_PROPERTIES=(
  {{- range $className := .GetConfigurationParameterKeys "csi-nfs" "classes" }}
  '{{ $.GetStringifiedConfigurationParameter "csi-nfs" "classes" $className "properties" }}'
  {{- end }}
)


###########################################################
##      CSI CEPH RBD STORAGE PROVIDER CONFIGURATION      ##
###########################################################
INSTALL_CSI_CEPH_RBD_PROVIDER="{{ .GetConfigurationParameter "csi-ceph-rbd" "enabled" }}"

CSI_CEPH_RBD_HA="{{ .GetConfigurationParameter "csi-ceph-rbd" "HA" }}"

# Ceph clusters configuration
CSI_CEPH_RBD_CLUSTER_NAMES=(
  {{- range $cephClusterName := .GetConfigurationParameterKeys "csi-ceph-rbd" "cephClusters" }}
  "{{ $cephClusterName }}"
  {{- end }}
)
CSI_CEPH_RBD_CLUSTER_IDS=(
  {{- range $cephClusterName := .GetConfigurationParameterKeys "csi-ceph-rbd" "cephClusters" }}
  "{{ $.GetConfigurationParameter "csi-ceph-rbd" "cephClusters" $cephClusterName "clusterID" }}"
  {{- end }}
)
CSI_CEPH_RBD_CLUSTER_MONITORS=(
  {{- range $cephClusterName := .GetConfigurationParameterKeys "csi-ceph-rbd" "cephClusters" }}
  '{{ $.GetConfigurationParameter "csi-ceph-rbd" "cephClusters" $cephClusterName "monitors" }}'
  {{- end }}
)

# CSI Ceph RBD classes details
CSI_CEPH_RBD_CLASS_NAMES=(
  {{- range $className := .GetConfigurationParameterKeys "csi-ceph-rbd" "classes" }}
  "{{ $className }}"
  {{- end }}
)

CSI_CEPH_RBD_CLASS_CONFIGURATIONS=(
  {{- range $className := .GetConfigurationParameterKeys "csi-ceph-rbd" "classes" }}
  '{{ $.GetStringifiedConfigurationParameter "csi-ceph-rbd" "classes" $className "configuration" }}'
  {{- end }}
)

CSI_CEPH_RBD_CLASS_PROPERTIES=(
  {{- range $className := .GetConfigurationParameterKeys "csi-ceph-rbd" "classes" }}
  '{{ $.GetStringifiedConfigurationParameter "csi-ceph-rbd" "classes" $className "properties" }}'
  {{- end }}
)


#################################################################
## PERIODICALLY FLUSH PAGE CACHE (fix for Ambassador Ingress)  ##
#################################################################
PERIODICALLY_DROP_CACHES="false"
DROP_CACHES_SCHEDULE="00 11 * * *"

############################
##     DNS MANAGEMENT     ##
############################
# If true, the platform is in charge of managing *all* cluster related DNS domains.
# If false, the operator is in charge of managing *all* cluster related DNS domains.
MANAGED_DNS="{{.Cluster.DNS.Management.Enabled}}"
MANAGED_DNS_PROVIDER={{.GetDNSProviderName}}

# AWS Route53 specific configuration
MANAGED_DNS_ROUTE53_CLI_IMAGE="amazon/aws-cli:2.0.8"
MANAGED_DNS_ROUTE53_AWS_CONFIG_DIR={{.GetDNSRoute53ConfigDir}}

# OVH DNS specific configuration
MANAGED_DNS_OVH_CLI_IMAGE="kumoripublic/kovhcli:v1.2.0"
MANAGED_DNS_OVH_CONFIG_FILE={{.GetDNSOVHConfigFile}}

########################
##     NETWORKING     ##
########################
# IMPORTANT WARNINGS:
# - these CIDRs should never overlap
# - these CIDRs should never overlap with other clusters on the same network
# - Docker CIDR must be based on a valid IP, since that IP will be set for the
#   docker0 interface. This means the broadcast address (x.x.x.0) IS NOT valid.
# - CIDR syntax (reminder):
#     - 172.20.0.1/24  -->  172.20.0.x
#     - 172.20.0.1/16  -->  172.20.x.x
#     - 172.20.0.1/8   -->  172.x.x.x
# - Valid private CIDR (reminder)
#     - 10.0.0.0    - 10.255.255.255
#     - 172.16.0.0  - 172.31.255.255
#     - 192.168.0.0 - 192.168.255.255
DOCKER_BRIDGE_CIDR="{{.Cluster.DockerBridgeNet}}"
PODS_NETWORK_CIDR="{{.Cluster.PodNet}}"
SERVICE_CIDR="{{.Cluster.ClusterIPNet}}"

# Optional: DNS resolvers for replacing DHCP configuration.
# This is *very* recommended for clusters deployed on ITI's Openstack.
DNS_RESOLVERS=(
  {{- range $mkey, $ip := .Cluster.DNSResolvers}}
  "{{$ip}}"
  {{- end }}
)


################################
##     KUMORI CONTROLLERS     ##
################################

INSTALL_KUALARM="{{ .GetConfigurationParameter "kumori-kualarm" "enabled" }}"

INSTALL_VOLUME_CONTROLLER="{{ .GetConfigurationParameter "kumori-volume-controller" "enabled" }}"

ADMISSION_AUTHENTICATION_TYPE="{{ .GetConfigurationParameter "kumori-admission" "authenticationType" }}"
ADMISSION_SERVER_CERT_DIR="{{ .GetConfigurationParameter "kumori-admission" "clientCertAuthParams" "serverCertDir" }}"
ADMISSION_CLIENT_CERT_REQUIRED="{{ .GetConfigurationParameter "kumori-admission" "clientCertAuthParams" "clientCertRequired" }}"
ADMISSION_SERVER_CLIENT_CERT_VALIDATE_WITH_TRUSTED_CA="{{ .GetConfigurationParameter "kumori-admission" "clientCertAuthParams" "clientCertValidateWithTrustedCA" }}"
ADMISSION_ADMIN_PASSWORD="{{ .GetConfigurationParameter "kumori-admission" "tokenAuthParams" "adminPassword" }}"
ADMISSION_DEVEL_PASSWORD="{{ .GetConfigurationParameter "kumori-admission" "tokenAuthParams" "develPassword" }}"
ADMISSION_ALLOWEDIP='{{ .GetConfigurationParameter "kumori-admission" "allowedIP" }}'

KUINBOUND_MINTLSVERSION="{{ .GetConfigurationParameter "kumori-kuinbound" "minTLSVersion" }}"
KUINBOUND_MAXTLSVERSION="{{ .GetConfigurationParameter "kumori-kuinbound" "maxTLSVersion" }}"
KUINBOUND_CIPHERSUITETLS="{{ .GetConfigurationParameter "kumori-kuinbound" "cipherSuitesTLS" }}"
KUINBOUND_ECDHCURVESTLS="{{ .GetConfigurationParameter "kumori-kuinbound" "ecdhCurvesTLS" }}"
KUINBOUND_HSTS="{{ .GetConfigurationParameter "kumori-kuinbound" "hsts" }}"

KUCONTROLLER_DEFAULT_LIVENESS_TIMEOUT="{{ .GetConfigurationParameter "kumori-kucontroller" "defaultLivenessTimeout" }}"
KUCONTROLLER_DEFAULT_READINESS_TIMEOUT="{{ .GetConfigurationParameter "kumori-kucontroller" "defaultReadinessTimeout" }}"
KUCONTROLLER_TSC_MAX_SKEW="{{ .GetConfigurationParameter "kumori-kucontroller" "topologySpreadConstraintsMaxSkew" }}"
KUCONTROLLER_REVISION_HISTORY_LIMIT="{{ .GetConfigurationParameter "kumori-kucontroller" "revisionHistoryLimit" }}"
KUCONTROLLER_FORCE_REBOOT_ON_UPDATE="{{ .GetConfigurationParameter "kumori-kucontroller" "forceRebootOnUpdate" }}"
KUCONTROLLER_DEFAULT_DISK_REQUEST_SIZE="{{ .GetConfigurationParameter "kumori-kucontroller" "defaultDiskRequestSize" }}"
KUCONTROLLER_DEFAULT_DISK_REQUEST_UNIT="{{ .GetConfigurationParameter "kumori-kucontroller" "defaultDiskRequestUnit" }}"
KUCONTROLLER_DEFAULT_VOLATILE_VOLUMES_TYPE="{{ .GetConfigurationParameter "kumori-kucontroller" "defaultVolatileVolumesType" }}"
KUCONTROLLER_EXTERNAL_SERVICES_ACCESS="{{ .GetConfigurationParameter "kumori-kucontroller" "externalServicesAccess" }}"

INSTALL_IPFILTERING="{{ .GetConfigurationParameter "kumori-ipfiltering" "enabled" }}"
IPFILTERING_REJECTIFFAILURE="{{ .GetConfigurationParameter "kumori-ipfiltering" "rejectIfFailure" }}"
IPFILTERING_TIMEOUTMSEC="{{ .GetConfigurationParameterAsMsec "kumori-ipfiltering" "timeout" }}"
IPFILTERING_MAXLISTSIZE="{{ .GetConfigurationParameter "kumori-ipfiltering" "maxListSize" }}"
IPFILTERING_LOGLEVEL="{{ .GetConfigurationParameter "kumori-ipfiltering" "loglevel" }}"

INSTALL_AUTHSERVICE="{{ .GetConfigurationParameter "kumori-authservice" "enabled" }}"

####################
##     ADDONS     ##
####################

ADDONS_STORAGE_CLASS="openebs-localhostpath"

INSTALL_CALICO="{{ .GetConfigurationParameter "calico" "enabled" }}"

AMBASSADOR_XFF_TRUSTED_HOPS="{{ .GetConfigurationParameter "ambassador" "XFFTrustedHops" }}"
AMBASSADOR_APIEXT_HA="{{ .GetConfigurationParameter "ambassador" "apiext" "HA" }}"
AMBASSADOR_APIEXT_REPLICAS="{{ .GetConfigurationParameter "ambassador" "apiext" "HAReplicas" }}"

INSTALL_KUBE_PROMETHEUS="{{ .GetConfigurationParameter "kube-prometheus" "enabled" }}"
MONITORING_REQUIRE_CLIENTCERT="{{ .GetConfigurationParameter "kube-prometheus" "secureEndpoints" }}"
MONITORING_CLIENTCERT_TRUSTED_CA_FILE="{{ .GetConfigurationParameter "kube-prometheus" "trustedCACertFile" }}"
PROMETHEUS_PERSISTENCE="{{ .GetConfigurationParameter "kube-prometheus" "prometheus" "persistence" }}"
PROMETHEUS_REMOTE_READ_URL="{{ .GetConfigurationParameter "kube-prometheus" "prometheus" "remoteReadURL" }}"
PROMETHEUS_REMOTE_WRITE_URL="{{ .GetConfigurationParameter "kube-prometheus" "prometheus" "remoteWriteURL" }}"
PROMETHEUS_REMOTE_AUTH_TYPE="{{ .GetConfigurationParameter "kube-prometheus" "prometheus" "remoteAuthType" }}"
PROMETHEUS_REMOTE_CLIENT_CERTIFICATE_CERT_FILE="{{ .GetConfigurationParameter "kube-prometheus" "prometheus" "remoteClientCertificateCertFile" }}"
PROMETHEUS_REMOTE_CLIENT_CERTIFICATE_KEY_FILE="{{ .GetConfigurationParameter "kube-prometheus" "prometheus" "remoteClientCertificateKeyFile" }}"
GRAFANA_REMOTE_DATASOURCE_NAME="{{ .GetConfigurationParameter "kube-prometheus" "prometheus" "grafanaRemoteDatasourceName" }}"
PROMETHEUS_RETENTION_TIME="{{ .GetConfigurationParameter "kube-prometheus" "prometheus" "retentionTime" }}"
PROMETHEUS_ALLOWEDIP='{{ .GetConfigurationParameter "kube-prometheus" "prometheus" "allowedIP" }}'
PROMETHEUS_HA="{{ .GetConfigurationParameter "kube-prometheus" "prometheus" "HA" }}"
PROMETHEUS_HA_REPLICAS="{{ .GetConfigurationParameter "kube-prometheus" "prometheus" "HAReplicas" }}"

INSTALL_CADVISOR="{{ .GetConfigurationParameter "kube-prometheus" "cadvisor" "enabled" }}"

ALERTMANAGER_HA="{{ .GetConfigurationParameter "kube-prometheus" "alertmanager" "HA" }}"
ALERTMANAGER_HA_REPLICAS="{{ .GetConfigurationParameter "kube-prometheus" "alertmanager" "HAReplicas" }}"
ALERTMANAGER_ALLOWEDIP='{{ .GetConfigurationParameter "kube-prometheus" "alertmanager" "allowedIP" }}'

GRAFANA_HA="{{ .GetConfigurationParameter "kube-prometheus" "grafana" "HA" }}"
GRAFANA_HA_REPLICAS="{{ .GetConfigurationParameter "kube-prometheus" "grafana" "HAReplicas" }}"
GRAFANA_ADMIN_USERNAME="{{ .GetConfigurationParameter "kube-prometheus" "grafana" "adminUsername" }}"
GRAFANA_ADMIN_PASSWORD="{{ .GetConfigurationParameter "kube-prometheus" "grafana" "adminPassword" }}"
GRAFANA_VIEWER_USERNAME="{{ .GetConfigurationParameter "kube-prometheus" "grafana" "viewerUsername" }}"
GRAFANA_VIEWER_PASSWORD="{{ .GetConfigurationParameter "kube-prometheus" "grafana" "viewerPassword" }}"
# Configure OAuth role mapping to be strict or not (forbid access if user has no Grafana role)
GRAFANA_OAUTH_ROLE_STRICT="{{ .GetConfigurationParameter "kube-prometheus" "grafana" "strictRoles" }}"
# Enable 'cluster' selector in grafana Dasboards
GRAFANA_ENABLE_MULTICLUSTER_SUPPORT="{{ .GetConfigurationParameter "kube-prometheus" "grafana" "enableMultiClusterDashboards" }}"
GRAFANA_ALLOWEDIP='{{ .GetConfigurationParameter "kube-prometheus" "grafana" "allowedIP" }}'

INSTALL_ELASTICSEARCH="{{ .GetConfigurationParameter "elasticsearch" "enabled" }}"
ELASTICSEARCH_REPLICAS="{{ .GetConfigurationParameter "elasticsearch" "replicas" }}"
ELASTICSEARCH_VOLUME_SIZE="{{ .GetConfigurationParameter "elasticsearch" "volumeSize" }}"

INSTALL_KIBANA="{{ .GetConfigurationParameter "kibana" "enabled" }}"
KIBANA_ELASTICSEARCH_URL="{{ .GetConfigurationParameter "kibana" "elasticsearchURL" }}"
KIBANA_ELASTICSEARCH_USERNAME="{{ .GetConfigurationParameter "kibana" "elasticsearchUsername" }}"
KIBANA_ELASTICSEARCH_PASSWORD="{{ .GetConfigurationParameter "kibana" "elasticsearchPassword" }}"
KIBANA_ALLOWEDIP='{{ .GetConfigurationParameter "kibana" "allowedIP" }}'

INSTALL_FILEBEAT="{{ .GetConfigurationParameter "filebeat" "enabled" }}"
FILEBEAT_ELASTICSEARCH_URL="{{ .GetConfigurationParameter "filebeat" "elasticsearchURL" }}"
FILEBEAT_ELASTICSEARCH_USERNAME="{{ .GetConfigurationParameter "filebeat" "elasticsearchUsername" }}"
FILEBEAT_ELASTICSEARCH_PASSWORD="{{ .GetConfigurationParameter "filebeat" "elasticsearchPassword" }}"
FILEBEAT_INDEX_PATTERN="{{ .GetConfigurationParameter "filebeat" "indexPattern" }}"
FILEBEAT_ENABLE_USER_SERVICES_LOGS="{{ .GetConfigurationParameter "filebeat" "includeUserServicesLogs" }}"

INSTALL_KEYCLOAK="{{ .GetConfigurationParameter "keycloak" "enabled" }}"
KEYCLOAK_ADMIN_USERNAME="{{ .GetConfigurationParameter "keycloak" "adminUsername" }}"
KEYCLOAK_ADMIN_PASSWORD="{{ .GetConfigurationParameter "keycloak" "adminPassword" }}"
KEYCLOAK_ALLOWEDIP='{{ .GetConfigurationParameter "keycloak" "allowedIP" }}'
# Create UUIDs for configuring Keycloak Admission and Grafana clients secrets
KEYCLOAK_ADMISSION_CLIENT_SECRET="$(cat /proc/sys/kernel/random/uuid)"
KEYCLOAK_GRAFANA_CLIENT_SECRET="$(cat /proc/sys/kernel/random/uuid)"

INSTALL_KUBERNETES_DASHBOARD="false"

INSTALL_MINIO="{{ .GetConfigurationParameter "minio" "enabled" }}"
MINIO_ACCESS_KEY="{{ .GetConfigurationParameter "minio" "accessKey" }}"
MINIO_SECRET_KEY="{{ .GetConfigurationParameter "minio" "secretKey" }}"
MINIO_ALLOWEDIP='{{ .GetConfigurationParameter "minio" "allowedIP" }}'

INSTALL_ETCD_BACKUP="{{ .GetConfigurationParameter "etcdbackup" "enabled" }}"
ETCD_BACKUP_ETCD_ENDPOINT="{{ .GetConfigurationParameter "etcdbackup" "etcdEndpoint" }}"
ETCD_BACKUP_SCHEDULE="{{ .GetConfigurationParameter "etcdbackup" "schedule" }}"
ETCD_BACKUP_DELTA_PERIOD="{{ .GetConfigurationParameter "etcdbackup" "deltaPeriod" }}"
ETCD_BACKUP_S3_ENDPOINT="{{ .GetConfigurationParameter "etcdbackup" "s3Endpoint" }}"
ETCD_BACKUP_S3_REGION="{{ .GetConfigurationParameter "etcdbackup" "s3Region" }}"
ETCD_BACKUP_S3_ACCESS_KEY="{{ .GetConfigurationParameter "etcdbackup" "s3AccessKey" }}"
ETCD_BACKUP_S3_SECRET_KEY="{{ .GetConfigurationParameter "etcdbackup" "s3SecretKey" }}"
ETCD_BACKUP_S3_BUCKET="{{ .GetConfigurationParameter "etcdbackup" "s3Bucket" }}"
ETCD_BACKUP_STORAGE_PREFIX="{{ .GetConfigurationParameter "etcdbackup" "s3Prefix" }}"
ETCD_BACKUP_TRUSTED_CA_CONFIGMAP="{{ .GetConfigurationParameter "etcdbackup" "trustedCAConfigmap" }}"

PKI_BACKUP="{{ .GetConfigurationParameter "pkibackup" "enabled" }}"
PKI_BACKUP_S3_ENDPOINT="{{ .GetConfigurationParameter "pkibackup" "s3Endpoint" }}"
PKI_BACKUP_S3_REGION="{{ .GetConfigurationParameter "pkibackup" "s3Region" }}"
PKI_BACKUP_S3_ACCESS_KEY="{{ .GetConfigurationParameter "pkibackup" "s3AccessKey" }}"
PKI_BACKUP_S3_SECRET_KEY="{{ .GetConfigurationParameter "pkibackup" "s3SecretKey" }}"
PKI_BACKUP_S3_BUCKET="{{ .GetConfigurationParameter "pkibackup" "s3Bucket" }}"
PKI_BACKUP_STORAGE_PREFIX="{{ .GetConfigurationParameter "pkibackup" "s3Prefix" }}"

# ExternalDNS will only be installed if platform is in charge of DNS management.
# It will be configured according to the MANAGED_DNS_PROVIDER.
INSTALL_EXTERNALDNS="${MANAGED_DNS}"
EXTERNALDNS_OWNER_ID="kumori-cluster-${RELEASE_NAME}"
EXTERNALDNS_INTERVAL="{{ .GetConfigurationParameter "externaldns" "interval" }}"
EXTERNALDNS_ROUTE53_AWSBATCHCHANGESIZE="{{ .GetConfigurationParameter "externaldns" "route53" "awsBatchChangeSize" }}"
EXTERNALDNS_ROUTE53_AWSBATCHCHANGEINTERVAL="{{ .GetConfigurationParameter "externaldns" "route53" "awsBatchChangeInterval" }}"

OUTOFSERVICE_REGISTRY="{{ .GetConfigurationParameter "outofservice" "hub" }}"
OUTOFSERVICE_IMAGE="{{ .GetConfigurationParameter "outofservice" "image" }}"
OUTOFSERVICE_USERNAME="{{ .GetConfigurationParameter "outofservice" "username" }}"
OUTOFSERVICE_PASSWORD="{{ .GetConfigurationParameter "outofservice" "password" }}"


# Configure where platform events are stored.
# Supported store types:
# - 'k8s': events are stored in the Kubernetes cluster (no configuration required)
# - 'elasticsearch': events are stored in an external Elastcsearch database.
#
# Selecting an Elasticsearch store, will have the following effect:
# - Event-Exporter will be installed and configured to send all platform events to the
#   configured Elasticsearch server
# - Admission will be configured for using Elasticsearch as the source for platform Events
EVENTS_STORE_TYPE="{{ .GetEventStoreType }}"

# Deploy Event-Exporter in High Availability mode
EVENTS_EXPORTER_HA="{{ .GetConfigurationParameter "eventexporter" "HA" }}"
EVENTS_EXPORTER_HA_REPLICAS="{{ .GetConfigurationParameter "eventexporter" "HAReplicas" }}"
# Elasticsearch backend configuration used by Event-Exporter
EVENTS_ELASTICSEARCH_URL="{{ .GetConfigurationParameter "eventexporter" "eventStoreElasticsearchURL" }}"
EVENTS_ELASTICSEARCH_USERNAME="{{ .GetConfigurationParameter "eventexporter" "eventStoreElasticsearchUsername" }}"
EVENTS_ELASTICSEARCH_PASSWORD="{{ .GetConfigurationParameter "eventexporter" "eventStoreElasticsearchPassword" }}"
EVENTS_ELASTICSEARCH_INDEX_PREFIX="{{ .GetConfigurationParameter "eventexporter" "eventStoreElasticsearchIndexPrefix" }}"
EVENTS_ELASTICSEARCH_INDEX_DATE_PATTERN="{{ .GetConfigurationParameter "eventexporter" "eventStoreElasticsearchDatePattern" }}"

# Calculated from the above, do not edit
EVENTS_ELASTICSEARCH_INDEX="${EVENTS_ELASTICSEARCH_INDEX_PREFIX}-${EVENTS_ELASTICSEARCH_INDEX_DATE_PATTERN}"
EVENTS_ELASTICSEARCH_INDEX_ADMISSION="${EVENTS_ELASTICSEARCH_INDEX_PREFIX}-*"


# Determine if Helm must be installed. Currently only needed for installing EFK addon.
if [ "${INSTALL_FILEBEAT}" = "true" ] || \
   [ "${INSTALL_ELASTICSEARCH}" = "true" ] || \
   [ "${INSTALL_KIBANA}" = "true" ]
then
  INSTALL_HELM="true"
else
  INSTALL_HELM="false"
fi

# Configure the descheduler strategies
INSTALL_DESCHEDULER="{{ .GetConfigurationParameter "descheduler" "enabled" }}"
DESCHEDULER_NODE_SELECTOR="{{ .GetConfigurationParameter "descheduler" "nodeSelector" }}"
DESCHEDULER_INTERVAL="{{ .GetConfigurationParameter "descheduler" "interval" }}"
DESCHEDULER_SPREAD_ENABLED="{{ .GetConfigurationParameter "descheduler" "spreadConstraintsViolation" "enabled" }}"
DESCHEDULER_SPREAD_PRIORITY_CLASS="{{ .GetConfigurationParameter "descheduler" "spreadConstraintsViolation" "priorityClass" }}"
DESCHEDULER_LOW_ENABLED="{{ .GetConfigurationParameter "descheduler" "lowNodeUtilization" "enabled" }}"
DESCHEDULER_LOW_PRIORITY_CLASS="{{ .GetConfigurationParameter "descheduler" "lowNodeUtilization" "priorityClass" }}"
DESCHEDULER_LOW_THRESHOLD_CPU="{{ .GetConfigurationParameter "descheduler" "lowNodeUtilization" "threshold" "cpu" }}"
DESCHEDULER_LOW_THRESHOLD_MEM="{{ .GetConfigurationParameter "descheduler" "lowNodeUtilization" "threshold" "memory" }}"
DESCHEDULER_LOW_THRESHOLD_POD="{{ .GetConfigurationParameter "descheduler" "lowNodeUtilization" "threshold" "pods" }}"
DESCHEDULER_LOW_TARGET_CPU="{{ .GetConfigurationParameter "descheduler" "lowNodeUtilization" "target" "cpu" }}"
DESCHEDULER_LOW_TARGET_MEM="{{ .GetConfigurationParameter "descheduler" "lowNodeUtilization" "target" "memory" }}"
DESCHEDULER_LOW_TARGET_POD="{{ .GetConfigurationParameter "descheduler" "lowNodeUtilization" "target" "pods" }}"

# Install CertManager
INSTALL_CERT_MANAGER="{{ .GetConfigurationParameter "certmanager" "enabled" }}"

# Install CertManager Webhook OVH
INSTALL_CERT_MANAGER_WEBHOOK_OVH="{{ .GetConfigurationParameter "certmanager-webhook-ovh" "enabled" }}"
CERT_MANAGER_WEBHOOK_OVH_CREDENTIALS_BASE64="{{ .GetConfigurationParameter "certmanager-webhook-ovh" "ovhDnsCredentialsFile" | .FilePathToBase64String }}"
CERT_MANAGER_WEBHOOK_OVH_EMAIL="{{ .GetConfigurationParameter "certmanager-webhook-ovh" "certNotificationsEmail" }}"
CERT_MANAGER_WEBHOOK_OVH_ISSUER_LETSENCRYPT_ENABLED="{{ .GetConfigurationParameter "certmanager-webhook-ovh" "issuers" "letsEncrypt" "enabled" }}"
CERT_MANAGER_WEBHOOK_OVH_ISSUER_LETSENCRYPT_USE_STAGING="{{ .GetConfigurationParameter "certmanager-webhook-ovh" "issuers" "letsEncrypt" "useStaging" }}"
CERT_MANAGER_WEBHOOK_OVH_ISSUER_ZEROSSL_ENABLED="{{ .GetConfigurationParameter "certmanager-webhook-ovh" "issuers" "zeroSsl" "enabled" }}"
CERT_MANAGER_WEBHOOK_OVH_ISSUER_ZEROSSL_KEY_ID="{{ .GetConfigurationParameter "certmanager-webhook-ovh" "issuers" "zeroSsl" "keyId" }}"
CERT_MANAGER_WEBHOOK_OVH_ISSUER_ZEROSSL_EABKEYHMAC="{{ .GetConfigurationParameter "certmanager-webhook-ovh" "issuers" "zeroSsl" "eabKeyHmac" }}"


################################
##     ADDONS - CLUSTERAPI    ##
################################
INSTALL_CLUSTERAPI="{{ .GetConfigurationParameter "clusterapi" "enabled" }}"
if [ "${INSTALL_CLUSTERAPI}" = "true" ]; then
  # CertManager is a requirement for running ClusterAPI
  INSTALL_CERT_MANAGER="true"
fi

INSTALL_CLUSTERAPI_INFRASTRUCTURE_PROVIDER_OPENSTACK="{{ .GetConfigurationParameter "clusterapi" "providers" "infrastructureOpenstack" "enabled" }}"
INSTALL_CLUSTERAPI_INFRASTRUCTURE_PROVIDER_AWS="{{ .GetConfigurationParameter "clusterapi" "providers" "infrastructureAws" "enabled" }}"
if [ "${INSTALL_CLUSTERAPI}" = "false" ]; then
  # ClusterAPI providers require ClusterAPI to be installed
  INSTALL_CLUSTERAPI_INFRASTRUCTURE_PROVIDER_OPENSTACK="false"
	INSTALL_CLUSTERAPI_INFRASTRUCTURE_PROVIDER_AWS="false"
fi


##################################################
##     ADDONS - IAAS-SPECIFIC CLOUD-MANAGERS    ##
##################################################
INSTALL_OPENSTACK_CLOUD_CONTROLLER="{{ .GetConfigurationParameter "openstackcloudcontroller" "enabled" }}"
if [ "${KUMORI_IAAS}" = "openstack" ]; then
  INSTALL_OPENSTACK_CLOUD_CONTROLLER="true"
fi
OPENSTACK_CLOUD_CONTROLLER_CLOUDS_YAML_FILE="{{ .GetConfigurationParameter "openstackcloudcontroller" "cloudsYamlFile" }}"
OPENSTACK_CLOUD_CONTROLLER_CLOUDS_YAML_KEY="{{ .GetConfigurationParameter "openstackcloudcontroller" "cloudsYamlKey" }}"

INSTALL_AWS_CLOUD_CONTROLLER="{{ .GetConfigurationParameter "awscloudcontroller" "enabled" }}"
if [ "${KUMORI_IAAS}" = "aws" ]
then
  INSTALL_AWS_CLOUD_CONTROLLER="true"
fi

################################################################################
## RESOURCES ASSIGNED TO PLATFORM ELEMENTS                                     #
################################################################################

# CoreDNS
COREDNS_CPU_REQUEST="{{ .GetResourceParameter "kumori-coredns" "coredns" "cpu" }}"
COREDNS_CPU_LIMIT="{{ .GetResourceParameter "kumori-coredns" "coredns" "cpuLimit" }}"
COREDNS_MEM_REQUEST="{{ .GetResourceParameter "kumori-coredns" "coredns" "mem" }}"
COREDNS_MEM_LIMIT="{{ .GetResourceParameter "kumori-coredns" "coredns" "memLimit" }}"

# Ambassador
AMBASSADOR_CPU_REQUEST="{{ .GetResourceParameter "ambassador" "ambassador" "cpu" }}"
AMBASSADOR_CPU_LIMIT="{{ .GetResourceParameter "ambassador" "ambassador" "cpuLimit" }}"
AMBASSADOR_MEM_REQUEST="{{ .GetResourceParameter "ambassador" "ambassador" "mem" }}"
AMBASSADOR_MEM_LIMIT="{{ .GetResourceParameter "ambassador" "ambassador" "memLimit" }}"
AMBASSADOR_APIEXT_CPU_REQUEST="{{ .GetResourceParameter "ambassador" "apiext" "cpu" }}"
AMBASSADOR_APIEXT_CPU_LIMIT="{{ .GetResourceParameter "ambassador" "apiext" "cpuLimit" }}"
AMBASSADOR_APIEXT_MEM_REQUEST="{{ .GetResourceParameter "ambassador" "apiext" "mem" }}"
AMBASSADOR_APIEXT_MEM_LIMIT="{{ .GetResourceParameter "ambassador" "apiext" "memLimit" }}"

# Prometheus
PROMETHEUS_CPU_REQUEST="{{ .GetResourceParameter "kube-prometheus" "prometheus" "cpu" }}"
PROMETHEUS_CPU_LIMIT="{{ .GetResourceParameter "kube-prometheus" "prometheus" "cpuLimit" }}"
PROMETHEUS_MEM_REQUEST="{{ .GetResourceParameter "kube-prometheus" "prometheus" "mem" }}"
PROMETHEUS_MEM_LIMIT="{{ .GetResourceParameter "kube-prometheus" "prometheus" "memLimit" }}"

# Prometheus Config Reloader
PROMETHEUS_CONFIG_RELOADER_CPU_REQUEST="{{ .GetResourceParameter "kube-prometheus" "configreloader" "cpu" }}"
PROMETHEUS_CONFIG_RELOADER_CPU_LIMIT="{{ .GetResourceParameter "kube-prometheus" "configreloader" "cpuLimit" }}"
PROMETHEUS_CONFIG_RELOADER_MEM_REQUEST="{{ .GetResourceParameter "kube-prometheus" "configreloader" "mem" }}"
PROMETHEUS_CONFIG_RELOADER_MEM_LIMIT="{{ .GetResourceParameter "kube-prometheus" "configreloader" "memLimit" }}"

# AlertManager
ALERTMANAGER_CPU_REQUEST="{{ .GetResourceParameter "kube-prometheus" "alertmanager" "cpu" }}"
ALERTMANAGER_CPU_LIMIT="{{ .GetResourceParameter "kube-prometheus" "alertmanager" "cpuLimit" }}"
ALERTMANAGER_MEM_REQUEST="{{ .GetResourceParameter "kube-prometheus" "alertmanager" "mem" }}"
ALERTMANAGER_MEM_LIMIT="{{ .GetResourceParameter "kube-prometheus" "alertmanager" "memLimit" }}"

# Grafana
GRAFANA_CPU_REQUEST="{{ .GetResourceParameter "kube-prometheus" "grafana" "cpu" }}"
GRAFANA_CPU_LIMIT="{{ .GetResourceParameter "kube-prometheus" "grafana" "cpuLimit" }}"
GRAFANA_MEM_REQUEST="{{ .GetResourceParameter "kube-prometheus" "grafana" "mem" }}"
GRAFANA_MEM_LIMIT="{{ .GetResourceParameter "kube-prometheus" "grafana" "memLimit" }}"

# CAdvisor
CADVISOR_CPU_REQUEST="{{ .GetResourceParameter "kube-prometheus" "cadvisor" "cpu" }}"
CADVISOR_CPU_LIMIT="{{ .GetResourceParameter "kube-prometheus" "cadvisor" "cpuLimit" }}"
CADVISOR_MEM_REQUEST="{{ .GetResourceParameter "kube-prometheus" "cadvisor" "mem" }}"
CADVISOR_MEM_LIMIT="{{ .GetResourceParameter "kube-prometheus" "cadvisor" "memLimit" }}"

# Filebeat
FILEBEAT_CPU_REQUEST="{{ .GetResourceParameter "filebeat" "filebeat" "cpu" }}"
FILEBEAT_CPU_LIMIT="{{ .GetResourceParameter "filebeat" "filebeat" "cpuLimit" }}"
FILEBEAT_MEM_REQUEST="{{ .GetResourceParameter "filebeat" "filebeat" "mem" }}"
FILEBEAT_MEM_LIMIT="{{ .GetResourceParameter "filebeat" "filebeat" "memLimit" }}"

# Keycloak
KEYCLOAK_CPU_REQUEST="{{ .GetResourceParameter "keycloak" "keycloak" "cpu" }}"
KEYCLOAK_CPU_LIMIT="{{ .GetResourceParameter "keycloak" "keycloak" "cpuLimit" }}"
KEYCLOAK_MEM_REQUEST="{{ .GetResourceParameter "keycloak" "keycloak" "mem" }}"
KEYCLOAK_MEM_LIMIT="{{ .GetResourceParameter "keycloak" "keycloak" "memLimit" }}"

# OPENEBS
OPENEBS_REPLICATED_CPU_REQUEST="{{ .GetResourceParameter "openebs" "replicated" "cpu" }}"
OPENEBS_REPLICATED_CPU_LIMIT="{{ .GetResourceParameter "openebs" "replicated" "cpuLimit" }}"
OPENEBS_REPLICATED_MEM_REQUEST="{{ .GetResourceParameter "openebs" "replicated" "mem" }}"
OPENEBS_REPLICATED_MEM_LIMIT="{{ .GetResourceParameter "openebs" "replicated" "memLimit" }}"
OPENEBS_REPLICATED_AUX_CPU_REQUEST="{{ .GetResourceParameter "openebs" "auxReplicated" "cpu" }}"
OPENEBS_REPLICATED_AUX_CPU_LIMIT="{{ .GetResourceParameter "openebs" "auxReplicated" "cpuLimit" }}"
OPENEBS_REPLICATED_AUX_MEM_REQUEST="{{ .GetResourceParameter "openebs" "auxReplicated" "mem" }}"
OPENEBS_REPLICATED_AUX_MEM_LIMIT="{{ .GetResourceParameter "openebs" "auxReplicated" "memLimit" }}"

# DESCHEDULER
DESCHEDULER_CPU_REQUEST="{{ .GetResourceParameter "descheduler" "descheduler" "cpu" }}"
DESCHEDULER_CPU_LIMIT="{{ .GetResourceParameter "descheduler" "descheduler" "cpuLimit" }}"
DESCHEDULER_MEM_REQUEST="{{ .GetResourceParameter "descheduler" "descheduler" "mem" }}"
DESCHEDULER_MEM_LIMIT="{{ .GetResourceParameter "descheduler" "descheduler" "memLimit" }}"

# IpFiltering (ingress plugin)
IPFILTERING_CPU_REQUEST="{{ .GetResourceParameter "kumori-ipfiltering" "ipfiltering" "cpu" }}"
IPFILTERING_CPU_LIMIT="{{ .GetResourceParameter "kumori-ipfiltering" "ipfiltering" "cpuLimit" }}"
IPFILTERING_MEM_REQUEST="{{ .GetResourceParameter "kumori-ipfiltering" "ipfiltering" "mem" }}"
IPFILTERING_MEM_LIMIT="{{ .GetResourceParameter "kumori-ipfiltering" "ipfiltering" "memLimit" }}"

# AuthService (ingress plugin)
AUTHSERVICE_CPU_REQUEST="{{ .GetResourceParameter "kumori-authservice" "authservice" "cpu" }}"
AUTHSERVICE_CPU_LIMIT="{{ .GetResourceParameter "kumori-authservice" "authservice" "cpuLimit" }}"
AUTHSERVICE_MEM_REQUEST="{{ .GetResourceParameter "kumori-authservice" "authservice" "mem" }}"
AUTHSERVICE_MEM_LIMIT="{{ .GetResourceParameter "kumori-authservice" "authservice" "memLimit" }}"

################################################################################
## PATHS                                                                      ##
################################################################################

CUSTOM_DIR={{.GetCustomScriptsDirectory}}
JOIN_SCRIPTS_DIR="${LOCAL_WORKDIR}/join-scripts" # Local path
PATCH_DIR=""

################################################################################
## ETCD BACKUP                                                                ##
################################################################################

USE_ETCDBACKUP="{{.InlineParams.UseEtcdBackup}}"
ETCDBACKUP_FILE="{{.InlineParams.EtcdBackupFile}}"

################################################################################
## VERSIONS                                                                   ##
################################################################################
OS_FLAVOUR="{{.Distribution.OSVersion.Flavour}}"
OS_VERSION="{{.Distribution.OSVersion.Version}}"
KERNEL_VERSION="{{.Distribution.OSVersion.Kernel}}"
SYSTEMD_VERSION="{{.Distribution.OSVersion.Systemd}}"
KUBERNETES_VERSION="{{.Distribution.KubeVersion}}"
HELM_VERSION="v2.17.0"
DOCKER_VERSION="{{ .GetPackageVersion "docker" }}"
CONTAINERD_VERSION="{{ .GetPackageVersion "containerd" }}"
CRI_DOCKERD_VERSION="{{ .GetPackageVersion "cridockerd" }}"
KEEPALIVED_VERSION="{{ .GetPackageVersion "keepalived" }}"
ENVOY_VERSION="{{ .GetPackageVersion "envoy" }}"
YQ_VERSION="3.3.2"

CALICO_VERSION="{{ .GetPackageVersion "calico" }}"
AMBASSADOR_VERSION="{{ .GetPackageVersion "ambassador" }}"
KUBE_PROMETHEUS_VERSION="{{ .GetPackageVersion "kube-prometheus" }}"
ELASTICSEARCH_VERSION="{{ .GetPackageVersion "elasticsearch" }}"
FILEBEAT_VERSION="{{ .GetPackageVersion "filebeat" }}"
KIBANA_VERSION="{{ .GetPackageVersion "kibana" }}"
K8S_DASHBOARD_VERSION="{{ .GetPackageVersion "kubernetes-dashboard" }}"
KEYCLOAK_VERSION="{{ .GetPackageVersion "keycloak" }}"
MINIO_VERSION="{{ .GetPackageVersion "minio" }}"
MINIO_MC_VERSION="{{ .GetConfigurationParameter "minio" "minioMcVersion" }}"
ETCD_BACKUP_VERSION="{{ .GetPackageVersion "etcdbackup" }}"
ETCD_RESTORE_IMAGE="registry.k8s.io/etcd:3.5.6-0"
EXTERNALDNS_VERSION="{{ .GetPackageVersion "externaldns" }}"
EVENT_EXPORTER_VERSION="{{ .GetPackageVersion "eventexporter" }}"
OPENEBS_VERSION="{{ .GetPackageVersion "openebs" }}"
DESCHEDULER_VERSION="{{ .GetPackageVersion "descheduler" }}"
CADVISOR_VERSION="{{ .GetConfigurationParameter "kube-prometheus" "cadvisor" "version" }}"
CSI_CINDER_PROVIDER_VERSION="{{ .GetPackageVersion "csi-cinder" }}"
CSI_NFS_PROVIDER_VERSION="{{ .GetPackageVersion "csi-nfs" }}"
CSI_CEPH_RBD_PROVIDER_VERSION="{{ .GetPackageVersion "csi-ceph-rbd" }}"
CERT_MANAGER_VERSION="{{ .GetPackageVersion "certmanager" }}"
CERT_MANAGER_WEBHOOK_OVH_VERSION="{{ .GetPackageVersion "certmanager-webhook-ovh" }}"
CLUSTERAPI_VERSION="{{ .GetPackageVersion "clusterapi" }}"
CLUSTERAPI_OPENSTACK_PROVIDER_VERSION="{{ .GetConfigurationParameter "clusterapi" "providers" "infrastructureOpenstack" "version" }}"
CLUSTERAPI_AWS_PROVIDER_VERSION="{{ .GetConfigurationParameter "clusterapi" "providers" "infrastructureAws" "version" }}"
OPENSTACK_CLOUD_CONTROLLER_VERSION="{{ .GetPackageVersion "openstackcloudcontroller" }}"
AWS_CLOUD_CONTROLLER_VERSION="{{ .GetPackageVersion "awscloudcontroller" }}"

KUMORI_IMAGES_PULL_POLICY="Always"
KUMORI_CRD_VERSION="{{ .GetPackageVersion "kumori-crd" }}"
KUMORI_KUVOLUME_VERSION="{{ .GetPackageVersion "kumori-volume" }}"
KUMORI_COREDNS_VERSION="{{ .GetPackageVersion "kumori-coredns" }}"
KUMORI_TOPOLOGY_CONTROLLER_VERSION="{{ .GetPackageVersion "kumori-topology" }}"
KUMORI_KUCONTROLLER_VERSION="{{ .GetPackageVersion "kumori-kucontroller" }}"
KUMORI_KUINBOUND_VERSION="{{ .GetPackageVersion "kumori-kuinbound" }}"
KUMORI_SOLUTION_CONTROLLER_VERSION="{{ .GetPackageVersion "kumori-solution-controller" }}"
KUMORI_ADMISSION_VERSION="{{ .GetPackageVersion "kumori-admission" }}"
KUMORI_KUALARM_VERSION="{{ .GetPackageVersion "kumori-kualarm" }}"
KUMORI_VOLUME_CONTROLLER_VERSION="{{ .GetPackageVersion "kumori-volume-controller" }}"
KUMORI_IPFILTERING_VERSION="{{ .GetPackageVersion "kumori-ipfiltering" }}"
KUMORI_AUTHSERVICE_VERSION="{{ .GetPackageVersion "kumori-authservice" }}"

################################################################################
## KUMORI CONTROLLERS IMAGE REGISTRY                                          ##
################################################################################
KUMORI_IMAGES_REGISTRY={{.GetControllersHubRegistry}}
KUMORI_IMAGES_REGISTRY_USERNAME={{.GetControllersHubUsername}}
KUMORI_IMAGES_REGISTRY_PASSWORD={{.GetControllersHubPassword}}

# Optional Docker registry mirror (applies to the whole cluster)
DOCKER_REGISTRY_MIRRORS=(
  {{- range $mkey, $mirror := .Cluster.RegistryMirrors}}
  "{{$mirror}}"
  {{- end }}
)

DOCKERHUB_USERNAME={{.GetDockerHubUsername}}
DOCKERHUB_PASSWORD={{.GetDockerHubPassword}}

# Determine CoreDNS version expected by Kubernetes, based on versioning table:
# https://github.com/coredns/deployment/blob/master/kubernetes/CoreDNS-k8s_version.md
if [[ "${KUBERNETES_VERSION}" =~ ^1.16.* ]]; then
  KUBERNETES_COREDNS_VERSION="1.6.2"
elif [[ "${KUBERNETES_VERSION}" =~ ^1.17.* ]]; then
  KUBERNETES_COREDNS_VERSION="1.6.5"
elif [[ "${KUBERNETES_VERSION}" =~ ^1.18.* ]]; then
  KUBERNETES_COREDNS_VERSION="1.6.7"
elif [[ "${KUBERNETES_VERSION}" =~ ^1.19.* ]]; then
  KUBERNETES_COREDNS_VERSION="1.7.0"
elif [[ "${KUBERNETES_VERSION}" =~ ^1.20.* ]]; then
  KUBERNETES_COREDNS_VERSION="1.7.0"
elif [[ "${KUBERNETES_VERSION}" =~ ^1.21.* ]]; then
  KUBERNETES_COREDNS_VERSION="1.8.0"
elif [[ "${KUBERNETES_VERSION}" =~ ^1.25.* ]]; then
  KUBERNETES_COREDNS_VERSION="1.9.3"
else
  # Unsupported Kubernetes version! Set a default value
  KUBERNETES_COREDNS_VERSION="1.9.3"
fi

`
