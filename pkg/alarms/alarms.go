/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package alarms

import (
	"cluster-manager/pkg/admission"
	"cluster-manager/pkg/cue2json"
	"cluster-manager/pkg/helper"
	"cluster-manager/pkg/jsonwrapper"
	"cluster-manager/pkg/logger"
	"cluster-manager/pkg/types"
	"encoding/json"
	"os"
	"path/filepath"
	"strconv"
)

// U is just an alias to the type allowing manipulate json without go-structs
type U = map[string]interface{}

// Available implementations
func getImplTypes() []string {
	return []string{"prometheus"}
}

const (
	ALARMS_SUBDIR        = "alarms"
	ALARMDEFS_SUBDIR     = "alarmdefinitions"
	ALARMIMPLPROM_SUBDIR = "alarmimpls/prometheus"
	DIRPERM              = 0775
	FILEPERM             = 0644
)

// AlarmsConfig is an struct containing an alarm with its alarmsImpl and AlarmsDefinition
type AlarmConfig struct {
	Alarm           Alarm           `json:"alarm"`
	AlarmImpl       IAlarmImpl      `json:"alarmImpl"`
	AlarmDefinition AlarmDefinition `json:"alarmDefinition"`
}

// AlarmsConfig is an struct containing lists of alarms, alarmsImpls and AlarmsDefinitions
type AlarmsConfig struct {
	Alarms           []Alarm                 `json:"alarms"`
	AlarmImpls       map[string][]IAlarmImpl `json:"alarmImpls"`
	AlarmDefinitions []AlarmDefinition       `json:"alarmDefinitions"`
}

func (a AlarmsConfig) GetAlarmNames() (alarmNames []string) {
	for _, v := range a.Alarms {
		alarmNames = append(alarmNames, v.Name)
	}
	return
}

func (a AlarmsConfig) GetAlarmImplementationNames() (alarmImplNames map[string][]string) {
	alarmImplNames = map[string][]string{}
	for implType, impl := range a.AlarmImpls {
		for _, v := range impl {
			alarmImplNames[implType] = append(alarmImplNames[implType], v.GetName())
		}
	}
	return
}

func (a AlarmsConfig) GetAlarmDefinitionNames() (alarmDefNames []string) {
	for _, v := range a.AlarmDefinitions {
		alarmDefNames = append(alarmDefNames, v.Name)
	}
	return
}

func (a AlarmsConfig) GetDefinition(alarmdefname string) (bool, AlarmDefinition) {
	for _, v := range a.AlarmDefinitions {
		if v.Name == alarmdefname {
			return true, v
		}
	}
	return false, AlarmDefinition{}
}

func (a AlarmsConfig) GetImplementation(alarmimplname string, implType string) (bool, IAlarmImpl) {
	switch implType {
	case "prometheus":
		for _, v := range a.AlarmImpls[implType] {
			if v.GetName() == alarmimplname {
				return true, v
			}
		}
	}
	return false, nil
}

func (a AlarmsConfig) GetAlarm(alarmname string) (bool, Alarm) {
	for _, v := range a.Alarms {
		if v.Name == alarmname {
			return true, v
		}
	}
	return false, Alarm{}
}

// SearchAlarmImplByDefinition get an alarm implementation related
// to an alarm.
func (a AlarmsConfig) ExistsImplementationForAlarm(alarmName string) (exists bool) {
	for _, v := range a.Alarms {
		if v.Name == alarmName {
			for _, implList := range a.AlarmImpls {
				for _, impl := range implList {
					if impl.GetAlarmDefinition() == v.AlarmDefinition {
						return true
					}
				}
			}
			return false
		}
	}
	return false
}

// GetAlarmsConfig returns the complete alarms configuration from cluster
func GetAlarmsConfig() (alarmsConfig AlarmsConfig, err error) {

	alarmsBytes, err := admission.GetAlarms()
	if err != nil {
		return
	}
	err = json.Unmarshal(alarmsBytes, &alarmsConfig.Alarms)
	if err != nil {
		return
	}

	alarmDefinitionBytes, err := admission.GetAlarmDefinitions()
	if err != nil {
		return
	}
	err = json.Unmarshal(alarmDefinitionBytes, &alarmsConfig.AlarmDefinitions)
	if err != nil {
		return
	}

	alarmImplMapBytes, err := admission.GetAlarmImpls()
	if err != nil {
		return
	}
	alarmsConfig.AlarmImpls = map[string][]IAlarmImpl{}
	for implType, implBytes := range alarmImplMapBytes {
		switch implType {
		case "prometheus":
			var aux []AlarmImplPrometheus
			err = json.Unmarshal(implBytes, &aux)
			if err != nil {
				return
			}
			for _, v := range aux {
				alarmsConfig.AlarmImpls[implType] = append(alarmsConfig.AlarmImpls[implType], v)
			}
		}
	}

	return
}

// SearchAlarmImplByDefinition get an alarm implementation related
// to an alarm definition.
// Assumes than there is only one implementation per definition
func SearchAlarmImplByDefinition(alarmDefinitionName string) (alarmImpl IAlarmImpl, err error) {
	alarmImpls, err := admission.GetAlarmImplNames()
	if err != nil {
		return
	}
	for implType, implList := range alarmImpls {
		for _, implName := range implList {
			var implValue IAlarmImpl
			implValue, err = GetAlarmImpl(implName, implType)
			if err != nil {
				return
			}
			if implValue.GetAlarmDefinition() == alarmDefinitionName {
				alarmImpl = implValue
				return
			}
		}
	}
	return
}

//
// Functions related with the workspace
//

// GetAlarmsConfigFromWorkspace returns the complete alarms configuration from
// workspace
func GetAlarmsConfigFromWorkspace() (alarmsConfig AlarmsConfig, err error) {
	meth := "alarms.GetAlarmsConfigFromWorkspace()"

	// Temporary directory
	tmpDirectory, err := filepath.Abs("./.tmp/alarms")
	if err != nil {
		return
	}
	helper.EnsureDirExists(tmpDirectory)
	defer func() {
		errRemove := os.RemoveAll(tmpDirectory)
		if errRemove == nil {
			logger.Warn("Removing temporary directory", "meth", meth, "directory", tmpDirectory)
		}
	}()

	// Compiling cue directories and load into structs
	logger.Info("Compiling CUE files", "meth", meth)
	err = cue2json.ExportAll("./cluster", tmpDirectory+"/all.json")
	if err != nil {
		return
	}
	logger.Debug("Loading json files into structs", "meth", meth)
	clusterAndDistribution, err := types.NewClusterAndDistributionFromFile(tmpDirectory + "/all.json")
	if err != nil {
		return
	}

	// Get alarms configuration
	alarmPackageConfig, err := clusterAndDistribution.Distribution.GetPackageConfig("kumori-kualarm")
	if err != nil {
		return
	}

	// Process alarm items
	alarmPackageItems, err := jsonwrapper.GetArrayValue(alarmPackageConfig, "alarms")
	if err != nil {
		return
	}
	for _, v := range alarmPackageItems {
		var alarmItemBytes []byte
		alarmItemBytes, err = json.Marshal(v)
		if err != nil {
			return
		}
		var alarmItem Alarm
		alarmItem, err = CreateAlarmFromJSON(alarmItemBytes)
		if err != nil {
			return
		}
		alarmsConfig.Alarms = append(alarmsConfig.Alarms, alarmItem)
	}

	// Process alarm definitions
	alarmPackageDefs, err := jsonwrapper.GetArrayValue(alarmPackageConfig, "alarmdefinitions")
	if err != nil {
		return
	}
	for _, v := range alarmPackageDefs {
		var alarmDefBytes []byte
		alarmDefBytes, err = json.Marshal(v)
		if err != nil {
			return
		}
		var alarmDef AlarmDefinition
		alarmDef, err = CreateAlarmDefinitionFromJSON(alarmDefBytes)
		if err != nil {
			return
		}
		alarmsConfig.AlarmDefinitions = append(alarmsConfig.AlarmDefinitions, alarmDef)
	}

	// Process alarm implementations
	alarmsConfig.AlarmImpls = map[string][]IAlarmImpl{}
	for _, implType := range getImplTypes() {
		var alarmPackageImpls []interface{}
		alarmPackageImpls, err = jsonwrapper.GetArrayValue(alarmPackageConfig, "alarmimpl."+implType)
		if err != nil {
			return
		}
		for _, v := range alarmPackageImpls {
			var alarmImplBytes []byte
			alarmImplBytes, err = json.Marshal(v)
			if err != nil {
				return
			}
			var alarmImpl IAlarmImpl
			alarmImpl, err = CreateAlarmImplFromJSON(alarmImplBytes)
			if err != nil {
				return
			}
			alarmsConfig.AlarmImpls[implType] = append(alarmsConfig.AlarmImpls[implType], alarmImpl)
		}
	}

	return
}

func PrepareAlarmFiles(
	alarmDirectory string,
	config *types.ClusterAndDistribution,
) (
	err error,
) {
	meth := "alarms.PrepareAlarmFiles()"
	logger.Debug("Generating alarm related files for scripts", "meth", meth)

	// Ensures directory exists and is empty
	if helper.DirExists(alarmDirectory) {
		_, err = helper.EmptyDir(alarmDirectory)
		if err != nil {
			return
		}
	} else {
		err = os.MkdirAll(alarmDirectory, os.ModeDir|DIRPERM)
		if err != nil {
			return
		}
	}

	// Create subdirectories
	err = os.MkdirAll(alarmDirectory+"/"+ALARMS_SUBDIR, os.ModeDir|DIRPERM)
	if err != nil {
		return
	}
	err = os.MkdirAll(alarmDirectory+"/"+ALARMDEFS_SUBDIR, os.ModeDir|DIRPERM)
	if err != nil {
		return
	}
	err = os.MkdirAll(alarmDirectory+"/"+ALARMIMPLPROM_SUBDIR, os.ModeDir|DIRPERM)
	if err != nil {
		return
	}

	// Get configuration of package kumori-alarm
	alarmCfg, err := config.Distribution.GetPackageConfig("kumori-kualarm")
	if err != nil {
		return
	}

	// Process alarm definitions
	logger.Debug("Generating alarm definitions", "meth", meth)
	alarmDefs, err := jsonwrapper.GetArrayValue(alarmCfg, "alarmdefinitions")
	if err != nil {
		return
	}
	for k, v := range alarmDefs {
		var alarmDefBytes []byte
		alarmDefBytes, err = json.Marshal(v)
		if err != nil {
			return
		}
		filepath := alarmDirectory + "/" + ALARMDEFS_SUBDIR + "/alarmdef_" + strconv.Itoa(k) + ".json"
		err = helper.CreateFile(filepath, FILEPERM, alarmDefBytes)
		if err != nil {
			return
		}
	}
	logger.Debug("Generated alarm definitions", "meth", meth)

	// Process alarm implementations
	// PROVISIONAL: ASSUMES JUST PROMETHEUS IMPLEMENTATION!
	logger.Debug("Generating alarm implementations", "meth", meth)
	alarmImplProm, err := jsonwrapper.GetArrayValue(alarmCfg, "alarmimpl.prometheus")
	if err != nil {
		return
	}
	for k, v := range alarmImplProm {
		var alarmImplBytes []byte
		alarmImplBytes, err = json.Marshal(v)
		if err != nil {
			return
		}
		filepath := alarmDirectory + "/" + ALARMIMPLPROM_SUBDIR + "/alarmimplprometheus_" + strconv.Itoa(k) + ".json"
		err = helper.CreateFile(filepath, FILEPERM, alarmImplBytes)
		if err != nil {
			return
		}
	}
	logger.Debug("Generated alarm implementations", "meth", meth)

	// Process alarm instances
	logger.Debug("Generating alarm instance", "meth", meth)
	alarmInstances, err := jsonwrapper.GetArrayValue(alarmCfg, "alarms")
	if err != nil {
		return
	}
	for k, v := range alarmInstances {
		var alarmBytes []byte
		alarmBytes, err = json.Marshal(v)
		if err != nil {
			return
		}
		filepath := alarmDirectory + "/" + ALARMS_SUBDIR + "/alarm_" + strconv.Itoa(k) + ".json"
		err = helper.CreateFile(filepath, FILEPERM, alarmBytes)
		if err != nil {
			return
		}
	}
	logger.Debug("Generated alarm instance", "meth", meth)

	// Process alarm manager
	logger.Debug("Generating alarm manager configuration", "meth", meth)
	alarmManager, err := jsonwrapper.GetValue(alarmCfg, "alarmmanager")
	if err != nil {
		return
	}
	var alarmManagerCR string
	alarmManagerCR, err = createAlarmManagerCR(alarmManager)
	if err != nil {
		return
	}
	filepath := alarmDirectory + "/alarmmanager.yaml"
	err = helper.CreateFile(filepath, 0644, []byte(alarmManagerCR))
	logger.Debug("Generated alarm manager configuration", "meth", meth)

	return
}
