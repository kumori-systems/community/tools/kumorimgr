/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package alarms

import (
	"cluster-manager/pkg/admission"
	"encoding/json"
	"reflect"
)

type AlarmDefinition struct {
	Name      string            `json:"name"`
	Title     string            `json:"title"`
	Info      string            `json:"info"`
	Group     string            `json:"group"`
	Labels    map[string]string `json:"labels"`
	InParams  []string          `json:"inParams"`
	OutParams []string          `json:"outParams"`
}

// Creates an alarmDefinition object from JSON content
func CreateAlarmDefinitionFromJSON(alarmDefinitionBytes []byte) (alarmDefinition AlarmDefinition, err error) {
	err = json.Unmarshal(alarmDefinitionBytes, &alarmDefinition)
	return
}

// GetAlarmDefinition get an alarm definition
func GetAlarmDefinition(alarmDefName string) (alarmDefinition AlarmDefinition, err error) {
	alarmDefBytes, err := admission.GetAlarmDefinition(alarmDefName)
	if err != nil {
		return
	}
	return CreateAlarmDefinitionFromJSON(alarmDefBytes)
}

// ContainsAlarmDef return true if an array of AlarmDefinitions contains the one
// provided (its name) as parameter
func ContainsAlarmDef(name string, list []AlarmDefinition) bool {
	for _, v := range list {
		if v.Name == name {
			return true
		}
	}
	return false
}

func (a AlarmDefinition) IsEqual(b AlarmDefinition) bool {
	return reflect.DeepEqual(a, b)
}

func (a AlarmDefinition) ToJSON() (alarmDefinitionBytes []byte, err error) {
	alarmDefinitionBytes, err = json.Marshal(a)
	return
}

func (a AlarmDefinition) Create() error {
	alarmDefBytes, err := a.ToJSON()
	if err != nil {
		return err
	}
	return admission.CreateAlarmDefinition(alarmDefBytes)
}

func (a AlarmDefinition) Update() error {
	alarmDefBytes, err := a.ToJSON()
	if err != nil {
		return err
	}
	return admission.UpdateAlarmDefinition(alarmDefBytes)
}

func (a AlarmDefinition) Remove() error {
	return admission.RemoveAlarmDefinition(a.Name)
}
