/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package alarms

import (
	"encoding/json"
	"strings"

	"github.com/ghodss/yaml"
)

func createAlarmManagerCR(alarmmanager interface{}) (alarmManagerCR string, err error) {

	//
	// PROVISIONAL IMPLEMENTATION! GO DEFINITION SHOULD BE USED!!
	//
	// PROVISIONAL IMPLEMENTATION! USING YAML-KUBE-SECRET INSTEAD OF JSON-KUKU CRD
	//

	alarmManagerJSONBytes, err := json.Marshal(alarmmanager)
	if err != nil {
		return
	}
	alarmManagerYAMLBytes, err := yaml.JSONToYAML(alarmManagerJSONBytes)
	if err != nil {
		return
	}
	alarmManagerStr := string(alarmManagerYAMLBytes)

	// Add spaces, to build a valid yaml
	alarmManagerStrSpaced := ""
	for _, line := range strings.Split(strings.TrimSuffix(alarmManagerStr, "\n"), "\n") {
		alarmManagerStrSpaced = alarmManagerStrSpaced + "    " + line + "\n"
	}

	alarmManagerCR = `
apiVersion: "v1"
kind: "Secret"
metadata:
	name: "alertmanager-main"
	namespace: "monitoring"
type: "Opaque"
stringData:
	alertmanager.yaml: |-
` + alarmManagerStrSpaced

	// Tabs are not allowed in YAML
	alarmManagerCR = tabToSpace(alarmManagerCR)

	return
}

func tabToSpace(input string) string {
	var result []string
	for _, c := range input {
		if c == '\t' {
			result = append(result, "  ")
		} else {
			result = append(result, string(c))
		}
	}
	return strings.Join(result, "")
}
