/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package alarms

import (
	"cluster-manager/pkg/admission"
	"encoding/json"
	"reflect"
)

type Alarm struct {
	Name            string            `json:"name"`
	AlarmDefinition string            `json:"alarmDefinition"`
	Title           string            `json:"title,omitempty"`
	Info            string            `json:"info,omitempty"`
	InParams        map[string]string `json:"inParams"`
}

// Creates an alarm object from JSON content
func CreateAlarmFromJSON(alarmBytes []byte) (alarm Alarm, err error) {
	err = json.Unmarshal(alarmBytes, &alarm)
	return
}

// GetAlarm get an alarm
func GetAlarm(alarmName string) (alarm Alarm, err error) {
	alarmBytes, err := admission.GetAlarm(alarmName)
	if err != nil {
		return
	}
	return CreateAlarmFromJSON(alarmBytes)
}

func (a Alarm) IsEqual(b Alarm) bool {
	return reflect.DeepEqual(a, b)
}

func (a Alarm) ToJSON() (alarmBytes []byte, err error) {
	alarmBytes, err = json.Marshal(a)
	return
}

func (a Alarm) Create() error {
	alarmBytes, err := a.ToJSON()
	if err != nil {
		return err
	}
	return admission.CreateAlarm(alarmBytes)
}

func (a Alarm) Update() error {
	alarmBytes, err := a.ToJSON()
	if err != nil {
		return err
	}
	return admission.UpdateAlarm(alarmBytes)
}

func (a Alarm) Remove() error {
	return admission.RemoveAlarm(a.Name)
}
