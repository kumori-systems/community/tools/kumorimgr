/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package alarms

import (
	"cluster-manager/pkg/admission"
	"encoding/json"
	"fmt"
	"reflect"
)

type IAlarmImpl interface {
	GetName() string
	GetAlarmDefinition() string
	GetType() string
}

type AlarmImpl struct {
	Name            string `json:"name"`
	AlarmDefinition string `json:"alarmDefinition"`
	Type            string `json:"type,omitempty"`
}

type AlarmImplPrometheus struct {
	AlarmImpl
	Expr string `json:"expr"`
	For  string `json:"for"`
}

// Just an example of other alarm implementation
// type AlarmImplElk struct {
// 	AlarmImpl
// 	ElkThing1 string `json:"elkthing1"`
// 	ElkThing2 string `json:"elkthing2"`
// }

func (a AlarmImpl) GetName() string {
	return a.Name
}

func (a AlarmImpl) GetAlarmDefinition() string {
	return a.AlarmDefinition
}

func (a AlarmImpl) GetType() string {
	// If type is not defined, the default is "prometheus"
	if a.Type == "" {
		return "prometheus"
	}
	return a.Type
}

// Creates an alarmImpl object from JSON content
func CreateAlarmImplFromJSON(alarmImplBytes []byte) (alarmImpl IAlarmImpl, err error) {

	// Read the JSON with the common struct of all the implementations
	var commonImpl AlarmImpl
	err = json.Unmarshal(alarmImplBytes, &commonImpl)
	if err != nil {
		return
	}

	switch commonImpl.GetType() {
	case "prometheus":
		var alarmimplprometheus AlarmImplPrometheus
		err = json.Unmarshal(alarmImplBytes, &alarmimplprometheus)
		if err != nil {
			return
		}
		alarmImpl = alarmimplprometheus
	default:
		err = fmt.Errorf("invalid alarm implementation type %s", commonImpl.GetType())
	}

	return
}

// GetAlarmImpl get an alarm implementation
func GetAlarmImpl(alarmImplName string, implType string) (alarmImpl IAlarmImpl, err error) {
	alarmImplBytes, err := admission.GetAlarmImpl(alarmImplName, implType)
	if err != nil {
		return
	}
	alarmImpl, err = CreateAlarmImplFromJSON(alarmImplBytes)
	return
}

func AlarmImplIsEqual(a IAlarmImpl, b IAlarmImpl, implType string) bool {
	switch implType {
	case "prometheus":
		aProm := a.(AlarmImplPrometheus)
		bProm := b.(AlarmImplPrometheus)
		return reflect.DeepEqual(aProm, bProm)
	}
	return false
}

func AlarmImplToJSON(a IAlarmImpl, implType string) (alarmImplBytes []byte, err error) {
	switch implType {
	case "prometheus":
		alarmImplBytes, err = json.Marshal(a.(AlarmImplPrometheus))
		return
	default:
		err = fmt.Errorf("invalid alarm implementation type %s", implType)
		return
	}
}

func AlarmImplCreate(a IAlarmImpl, implType string) error {
	switch implType {
	case "prometheus":
		alarmDefBytes, err := AlarmImplToJSON(a.(AlarmImplPrometheus), implType)
		if err != nil {
			return err
		}
		return admission.CreateAlarmImpl(alarmDefBytes, implType)
	default:
		return fmt.Errorf("invalid alarm implementation type %s", implType)
	}
}

func AlarmImplUpdate(a IAlarmImpl, implType string) error {
	switch implType {
	case "prometheus":
		alarmDefBytes, err := AlarmImplToJSON(a.(AlarmImplPrometheus), implType)
		if err != nil {
			return err
		}
		return admission.UpdateAlarmImpl(alarmDefBytes, implType)
	default:
		return fmt.Errorf("invalid alarm implementation type %s", implType)
	}
}

func AlarmImplRemove(a IAlarmImpl, implType string) error {
	return admission.RemoveAlarmImpl(a.GetName(), implType)
}
