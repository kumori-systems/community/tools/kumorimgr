/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package jsonwrapper

import (
	"fmt"
	"strings"
)

// U is just an alias to the type allowing manipulate json without go-structs
type U = map[string]interface{}

func GetValue(elem U, keys string) (interface{}, error) {
	return getValue(elem, stringToArray(keys))
}

func GetString(elem U, keys string) (string, error) {
	v, err := getValue(elem, stringToArray(keys))
	if err != nil {
		return "", err
	}
	return v.(string), err
}

func GetNumber(elem U, keys string) (float64, error) {
	v, err := getValue(elem, stringToArray(keys))
	if err != nil {
		return 0, err
	}
	return v.(float64), err
}

func GetStringArray(elem U, keys string) ([]string, error) {
	v, err := GetArrayValue(elem, keys)
	if err != nil {
		return []string{}, err
	}
	aux := []string{}
	for _, v := range v {
		aux = append(aux, v.(string))
	}
	return aux, err
}

func GetNumberArray(elem U, keys string) ([]float64, error) {
	v, err := GetArrayValue(elem, keys)
	if err != nil {
		return []float64{}, err
	}
	aux := []float64{}
	for _, x := range v {
		aux = append(aux, x.(float64))
	}
	return aux, err
}

func GetArrayValue(elem U, keys string) ([]interface{}, error) {
	v, err := getValue(elem, stringToArray(keys))
	if err != nil {
		return nil, err
	}
	return v.([]interface{}), err
}

func getValue(elem U, keys []string) (interface{}, error) {
	current := elem
	for i := 0; i < len(keys)-1; i++ {
		k := keys[i]
		v, ok := current[k]
		if !ok {
			return nil, fmt.Errorf("Key %s not found", k)
		}
		current = v.(U)
	}
	lastKey := keys[len(keys)-1]
	v, ok := current[lastKey]
	if !ok {
		return nil, fmt.Errorf("Key %s not found", lastKey)
	}
	return v, nil
}

func stringToArray(str string) []string {
	return strings.Split(str, ".")
}
