/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package git

import (
	"cluster-manager/pkg/viper"
	"encoding/json"
	"fmt"
	"os"
)

type Credentials struct {
	CredType string  `json:"type"` // "password" or "token"
	Username string  `json:"username"`
	Password *string `json:"password,omitempty"`
	Token    *string `json:"token,omitempty"`
}

func NewCredentialsFromConfig(entryName string) (cred *Credentials, err error) {
	if !viper.Global.IsSet(entryName + "::git-credentials") {
		// Credentials is nil
		return
	}
	credType := viper.Global.GetString(entryName + "::git-credentials::type")
	username := viper.Global.GetString(entryName + "::git-credentials::username")
	token := viper.Global.GetString(entryName + "::git-credentials::token")
	password := viper.Global.GetString(entryName + "::git-credentials::password")
	if (credType != "token") && (credType != "password") {
		err = fmt.Errorf(
			"Git credentials type must be 'token' or 'password' (%s)", cred.CredType,
		)
	} else if (credType == "token") && (token != "") {
		cred = &Credentials{
			CredType: credType,
			Username: username,
			Token:    &token,
		}
	} else if (credType == "password") && (password != "") {
		cred = &Credentials{
			CredType: credType,
			Username: username,
			Password: &password,
		}
	} else {
		err = fmt.Errorf("Git credentials are not completed")
	}
	return
}

func NewCredentialsFromFile(filePath string) (cred *Credentials, err error) {
	cred = &Credentials{}
	content, err := os.ReadFile(filePath)
	if err != nil {
		return
	}
	err = json.Unmarshal(content, cred)
	if err != nil {
		return
	}
	if (cred.CredType != "password") && (cred.CredType != "token") {
		err = fmt.Errorf(
			"Git credentials type must be 'username' or 'token' (%s)", cred.CredType,
		)
	}
	return
}
