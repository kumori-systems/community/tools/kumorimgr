/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package git

import (
	"cluster-manager/pkg/helper"
	"cluster-manager/pkg/logger"
	"fmt"
	"os"
	"path"
	"strings"

	"gopkg.in/src-d/go-git.v4"
	"gopkg.in/src-d/go-git.v4/plumbing"
	"gopkg.in/src-d/go-git.v4/plumbing/object"
	"gopkg.in/src-d/go-git.v4/plumbing/transport/http"
)

// Clone clones a git repository in a given folder
func Clone(dst string, url string, credentials *Credentials) (err error) {

	err = os.MkdirAll(dst, os.ModeDir|0777)
	if err != nil {
		return
	}

	// Checks if we must clone a tag/commit (referenceName) or not
	referenceName := ""
	parts := strings.Split(url, "#")
	lenparts := len(parts)
	if lenparts == 2 {
		url = parts[0]
		referenceName = parts[1]
	} else if lenparts > 2 {
		last := lenparts - 2
		urlparts := parts[0:last]
		url = strings.Join(urlparts, "#")
		referenceName = parts[lenparts-1]
	}

	// Checks if the repository already contains the demanded git repository. If that
	// is the case, pull form the remote repository to update the folder. Otherwise,
	// clean the previous content and clone.
	alreadyCloned := false
	origin := ""
	gitpath := path.Join(dst, ".git")
	if helper.DirExists(gitpath) {
		repo, err := git.PlainOpen(dst)
		if err != nil {
			return fmt.Errorf("Error reading repository '%s': %s", url, err.Error())
		}
		config, err := repo.Config()
		if err != nil {
			return fmt.Errorf("Error getting remotes from repository '%s': %s", url, err.Error())
		}
		for name, remote := range config.Remotes {
			if remote == nil {
				continue
			}
			if len(remote.URLs) <= 0 {
				continue
			}
			for _, u := range remote.URLs {
				if strings.HasSuffix(url, u) {
					origin = name
					alreadyCloned = true
					break
				}
			}
		}

		// Pulls from the remote repo and returns if the destination folder already
		// contains the expected repo
		if alreadyCloned {
			err := pullFromRepo(dst, origin, referenceName)
			if err != nil {
				return err
			}
			return nil
		}
	}

	// Removes content of the destination folder since it isn't empty ant it not
	// contains the expected repository
	_, err = helper.EmptyDir(dst)
	if err != nil {
		return fmt.Errorf("Error removing content from folder '%s': %s", dst, err.Error())
	}

	// Clones the git repository in the target folder and returns
	err = cloneRepo(dst, url, referenceName, credentials)
	if err != nil {
		return
	}
	return nil
}

// pullFromRepo pulls the last changes from a given repo in a given folder.
// referenceName can be a branch or tag short name. If "" then it is not taken into account.
func pullFromRepo(dst string, origin string, referenceName string) error {
	// Opens the repo and reads the worktree
	repo, err := git.PlainOpen(dst)

	if err != nil {
		return fmt.Errorf("Error reading repository '%s': %s", dst, err.Error())
	}
	worktree, err := repo.Worktree()
	if err != nil {
		return fmt.Errorf("Error getting working tree from '%s': %s", dst, err.Error())
	}

	// If the reference name is set, we must check if it is a branch or a repo
	// NOTE: this code is not tested
	iter, err := repo.References()
	if err != nil {
		return err
	}
	branchReference := plumbing.NewRemoteReferenceName(origin, referenceName)
	pulled := false
	err = iter.ForEach(func(ref *plumbing.Reference) error {
		if (ref.Name().Short() == referenceName) || (ref.Name().Short() == branchReference.Short()) {
			options := &git.PullOptions{
				RemoteName:    origin,
				ReferenceName: ref.Name(),
			}
			if err = worktree.Pull(options); err != nil {
				return fmt.Errorf("Error pulling repository '%s' from remote '%s': %s", dst, origin, err.Error())
			}
			pulled = true
		}
		return nil
	})

	if !pulled {
		return fmt.Errorf("Unable to find reference %s in repository dir %s", referenceName, dst)
	}
	return nil
}

// cloneRepo clones a repo from a given url in a given folder. The destination
// folder is supposed to be created and empty.
// referenceName can be:
// - a commit
// - a tag short name
// - nothing (""), so master is used
// - branchs are not allowed
func cloneRepo(dst string, url string, referenceName string, credentials *Credentials) error {

	// Prepare the clone options
	cloneOptions := &git.CloneOptions{
		URL:               url,
		RecurseSubmodules: git.DefaultSubmoduleRecursionDepth,
	}

	// Add credentials if any
	if credentials != nil {
		auth := http.BasicAuth{
			Username: credentials.Username,
		}
		switch credentials.CredType {
		case "password":
			auth.Password = *credentials.Password
		case "token":
			auth.Password = *credentials.Token
		default:
			return fmt.Errorf("Invalid credentials for repository %s. Password or Token not found ", url)
		}
		cloneOptions.Auth = &auth
	}

	// Clone the repository on master branch
	repo, err := git.PlainClone(dst, false, cloneOptions)
	if err != nil {
		return err
	}

	// What commit must be used? Tag, commit or master?
	// Review all repo references, checking if any match tag or commit
	var finalReference plumbing.Hash
	finalName := ""
	iter, err := repo.References()
	err = iter.ForEach(func(ref *plumbing.Reference) error {
		name, commit, err := getNameAndCommit(ref, repo)
		if err != nil {
			return err
		}
		if referenceName != "" && (referenceName == name || referenceName == commit.String()) {
			finalReference = commit
			finalName = name
		}
		if referenceName == "" && name == "master" {
			finalReference = commit
			finalName = name
		}
		return nil
	})
	if err != nil {
		return err
	}
	if finalName == "" {
		err = fmt.Errorf("No matching reference %s found in repository %s", referenceName, url)
		return err
	}
	// Checkout the right commit
	worktree, err := repo.Worktree()
	if err != nil {
		return err
	}
	checkoutOptions := git.CheckoutOptions{}
	checkoutOptions.Hash = finalReference
	if err = worktree.Checkout(&checkoutOptions); err != nil {
		err = fmt.Errorf(
			"Error checking out reference '%s' from repository '%s': %s",
			finalName, url, err.Error(),
		)
		return err
	}

	return nil
}

func getNameAndCommit(
	ref *plumbing.Reference, repo *git.Repository,
) (
	name string, commit plumbing.Hash, err error,
) {

	name = ref.Name().Short()
	obj, err := repo.TagObject(ref.Hash()) // Annotated tags!
	switch err {
	case nil:
		// Its an annotated tag -> get its real commit
		commit = obj.Target
	case plumbing.ErrObjectNotFound:
		err = nil
		commit = ref.Hash()
	}
	return
}

// Check if Scripts repository directory is clean, fail otherwise
func CheckRepository(path string, tag string) (err error) {

	meth := fmt.Sprintf("CheckRepository(%s, %s)", path, tag)

	// "Open" the local repo
	logger.Debug(meth + " - Opening local repository...")
	repo, err := git.PlainOpen(path)
	if err != nil {
		err = fmt.Errorf("Error reading repository '%s': %s", path, err.Error())
		return err
	}

	// Get current HEAD (current commit)
	currentHead, _ := repo.Head()
	logger.Debug(meth + " - Current HEAD commit: " + currentHead.Hash().String())

	// Search for the commit of the configured tag or branch
	var targetReference plumbing.Hash
	finalName := ""
	iter, err := repo.References()
	err = iter.ForEach(func(ref *plumbing.Reference) error {
		name, commit, err := getNameAndCommit(ref, repo)
		if err != nil {
			return err
		}
		if tag != "" && (tag == name || tag == commit.String()) {
			targetReference = commit
			finalName = name
		}
		if tag == "" && name == "master" {
			targetReference = commit
			finalName = name
		}
		return nil
	})
	if err != nil {
		return err
	}
	if finalName == "" {
		err = fmt.Errorf("No matching reference %s found in repository %s", tag, path)
		return err
	}

	// Compare current commit with the expected tag (they must match)
	if currentHead.Hash().String() == targetReference.String() {
		logger.Debug(meth + " - Repository is in the correct commit.")
	} else {
		logger.Debug(meth + " - Repository is not in the correct commit.")
		err = fmt.Errorf("Repository is not in the correct commit (expected tag %s)", tag)
		return err
	}

	// Check if the repository si clean (no modified or untracked files)
	logger.Debug(meth + " - Checking if repository worktree is clean...")
	currentWorktree, err := repo.Worktree()
	if err != nil {
		err = fmt.Errorf("Error: unable to get current worktree.")
		return err
	}
	currentWorktreeStatus, err := currentWorktree.Status()
	if err != nil {
		err = fmt.Errorf("Error: unable to get current worktree status.")
		return err
	}
	if !currentWorktreeStatus.IsClean() {
		err = fmt.Errorf("Current worktree is not clean.")
		return err
	}

	logger.Debug(meth + " - All repository checks passed.")

	return nil
}

func Test() {
	fmt.Println("****************************************************")
	fmt.Println("GIT TESTS")
	fmt.Println("****************************************************")
	fmt.Println()

	// Local repository directory
	repoDir := "/home/jferrer/installertest-cluster/testing_v1.0.0-alpha/kumorimgr/scripts"

	// Expected Tag
	expectedTag := "v0.5.2-alpha7"

	// "Open" the local repo
	repo, err := git.PlainOpen(repoDir)
	if err != nil {
		fmt.Printf("Error reading repository '%s': %s\n", repoDir, err.Error())
	}

	// Get repo config
	config, err := repo.Config()
	if err != nil {
		fmt.Printf("Error getting remotes from repository '%s': %s\n", repoDir, err.Error())
	}
	fmt.Printf("GIT REPO CONFIG: '%+v'\n", config)
	fmt.Printf("GIT REPO BRANCHES: '%+v'\n", config.Branches)

	// Get current HEAD (current commit)
	currentHead, _ := repo.Head()
	fmt.Printf("CURRENT HEAD: '%+v'\n", currentHead.Hash().String())

	// Get expected tag info
	tagIter, err := repo.Tags()
	if err != nil {
		fmt.Printf("Error getting tags from repository '%s': %s\n", repoDir, err.Error())
	}

	var expectedTagCommit *object.Commit

	if err := tagIter.ForEach(func(ref *plumbing.Reference) error {

		// fmt.Printf("----------------------------------------------------------")
		// fmt.Printf("REF: '%+v'\n", ref)
		// fmt.Printf("REF HASH TO STRING: '%s'\n", ref.Hash().String())
		// fmt.Printf("REF TARGET TO STRING: '%s'\n", ref.Target().String())
		// fmt.Printf("REF NAME TO STRING: '%s'\n", ref.Name().String())
		// fmt.Printf("REF NAME TO STRING (SHORT): '%s'\n", ref.Name().Short())

		obj, err := repo.TagObject(ref.Hash())
		switch err {
		case nil:
			// Tag object present
			// fmt.Printf("GIT TAG: '%+v'\n", obj)
			if ref.Name().Short() == strings.ToLower(expectedTag) {
				commit, _ := obj.Commit()
				if currentHead.Hash().String() == commit.Hash.String() {
					expectedTagCommit = commit
					fmt.Println("**** REPO IS IN THE EXPECTED TAG!")
				} else {
					fmt.Println("XXXX REPO IS NOT IN THE EXPECTED TAG!")
				}
			}
			return nil
		case plumbing.ErrObjectNotFound:
			// Not a tag object
			return nil
		default:
			// Some other error
			fmt.Printf("Error getting tag object from repository '%s': %s\n", repoDir, err.Error())
			// return err
			return nil
		}
	}); err != nil {
		fmt.Printf("Error looping tags from repository '%s': %s\n", repoDir, err.Error())
	}

	if expectedTagCommit == nil {
		fmt.Printf("Error: unable to determine expected Tag commit.\n")
	} else {

		// commitObject, err := repo.CommitObject(currentHead.Hash())
		// if err != nil {
		// 	fmt.Printf("Error: unable to get current HEAD commit info.\n")
		// }
		// currentDirState, err := commitObject.Tree()
		// if err != nil {
		// 	fmt.Printf("Error: unable to get current HEAD status (tree).\n")
		// }

		currentWorktree, err := repo.Worktree()
		if err != nil {
			fmt.Printf("Error: unable to get current worktree.\n")
		}

		currentWorktreeStatus, err := currentWorktree.Status()
		if err != nil {
			fmt.Printf("Error: unable to get current worktree status.\n")
		}

		fmt.Printf("CURRENT WORKTREE STATUS:\n")
		fmt.Printf("- IsClean: %t\n", currentWorktreeStatus.IsClean())
		fmt.Printf("- Changes: %s\n", currentWorktreeStatus.String())

		if !currentWorktreeStatus.IsClean() {
			fmt.Printf("Current worktree is not clean. Use '--skip-scripts-check' to bypass this validation.\n")
			fmt.Printf("Changes in the worktree:\n")
			fmt.Printf("%s\n", currentWorktreeStatus.String())
		}
	}
}
