/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package admission

import (
	"github.com/Jeffail/gabs"
)

// Admission package functions related to users
//
// The KuAlarm API responses have this structure:
// {
//   "success": true / false
//   "message": "OK" / "description of the error"
//   "data": ...
// }
//

const (
	alarmApiPath           = "admission/alarms/kualarm"
	alarmDefinitionApiPath = "admission/alarms/kualarmdefinition"
)

func getImplApiPaths() map[string]string {
	return map[string]string{
		"prometheus": "admission/alarms/kualarmimplprometheus",
	}
}

func GetAlarmNames() (alarms []string, err error) {
	data, err := commonGet(alarmApiPath)
	if err != nil {
		return
	}
	alarms, err = gabsArrayToSlice(data)
	return
}

func GetAlarms() (alarms []byte, err error) {
	data, err := commonGet(alarmApiPath + "/all")
	if err != nil {
		return
	}
	alarms = data.Bytes()
	return
}

func GetAlarmDefinitionNames() (alarmDefinitions []string, err error) {
	data, err := commonGet(alarmDefinitionApiPath)
	if err != nil {
		return
	}
	alarmDefinitions, err = gabsArrayToSlice(data)
	return
}

func GetAlarmDefinitions() (alarmDefinitions []byte, err error) {
	data, err := commonGet(alarmDefinitionApiPath + "/all")
	if err != nil {
		return
	}
	alarmDefinitions = data.Bytes()
	return
}

func GetAlarmImplNames() (alarmImpls map[string][]string, err error) {
	alarmImpls = map[string][]string{}
	for impl, apiPath := range getImplApiPaths() {
		alarmImpls[impl] = []string{}
		var data *gabs.Container
		data, err = commonGet(apiPath)
		if err != nil {
			return
		}
		var items []string
		items, err = gabsArrayToSlice(data)
		if err != nil {
			return
		}
		alarmImpls[impl] = append(alarmImpls[impl], items...)
	}
	return
}

func GetAlarmImpls() (alarmImpls map[string][]byte, err error) {
	alarmImpls = map[string][]byte{}
	for impl, apiPath := range getImplApiPaths() {
		alarmImpls[impl] = []byte{}
		var data *gabs.Container
		data, err = commonGet(apiPath + "/all")
		if err != nil {
			return
		}
		alarmImpls[impl] = data.Bytes()
	}
	return
}

func GetAlarm(alarmName string) ([]byte, error) {
	jsonData, err := commonGet(alarmApiPath + "/" + alarmName)
	if err != nil {
		return nil, err
	}
	return jsonData.Bytes(), err
}

func GetAlarmDefinition(alarmDefName string) ([]byte, error) {
	jsonData, err := commonGet(alarmDefinitionApiPath + "/" + alarmDefName)
	if err != nil {
		return nil, err
	}
	return jsonData.Bytes(), err
}

func GetAlarmImpl(alarmImplName string, implType string) ([]byte, error) {
	jsonData, err := commonGet(getImplApiPaths()[implType] + "/" + alarmImplName)
	if err != nil {
		return nil, err
	}
	return jsonData.Bytes(), err
}

func CreateAlarm(alarm []byte) (err error) {
	return genericAlarmAPICreate(alarm, alarmApiPath)
}

func CreateAlarmDefinition(alarmDef []byte) (err error) {
	return genericAlarmAPICreate(alarmDef, alarmDefinitionApiPath)
}

func CreateAlarmImpl(alarmImpl []byte, implType string) (err error) {
	return genericAlarmAPICreate(alarmImpl, getImplApiPaths()[implType])
}

func UpdateAlarm(alarm []byte) (err error) {
	return genericAlarmAPIUpdate(alarm, alarmApiPath)
}

func UpdateAlarmDefinition(alarmDef []byte) (err error) {
	return genericAlarmAPIUpdate(alarmDef, alarmDefinitionApiPath)
}

func UpdateAlarmImpl(alarmImpl []byte, implType string) (err error) {
	return genericAlarmAPIUpdate(alarmImpl, getImplApiPaths()[implType])
}

func RemoveAlarm(alarmName string) (err error) {
	return genericAlarmAPIRemove(alarmName, alarmApiPath)
}

func RemoveAlarmDefinition(alarmDefName string) (err error) {
	return genericAlarmAPIRemove(alarmDefName, alarmDefinitionApiPath)
}

func RemoveAlarmImpl(alarmImplName string, implType string) (err error) {
	return genericAlarmAPIRemove(alarmImplName, getImplApiPaths()[implType])
}

func genericAlarmAPICreate(data []byte, apipath string) (err error) {
	authorization, err := getAuthorization()
	if err != nil {
		return
	}
	requestParams, err := NewRequestParamsUsingJSONPayload(data)
	if err != nil {
		return
	}

	admissionCA, clientCert, err := readCerts()
	if err != nil {
		return
	}

	response, err := doRequest("POST", apipath, admissionCA, clientCert, authorization, requestParams)
	if err != nil {
		return
	}
	err = analyzeResponseError(response)
	if err != nil {
		return
	}
	return
}

func genericAlarmAPIUpdate(data []byte, apipath string) (err error) {
	authorization, err := getAuthorization()
	if err != nil {
		return
	}
	requestParams, err := NewRequestParamsUsingJSONPayload(data)
	if err != nil {
		return
	}

	admissionCA, clientCert, err := readCerts()
	if err != nil {
		return
	}

	response, err := doRequest("PUT", apipath, admissionCA, clientCert, authorization, requestParams)
	if err != nil {
		return
	}
	err = analyzeResponseError(response)
	if err != nil {
		return
	}
	return
}

func genericAlarmAPIRemove(name string, apipath string) (err error) {
	authorization, err := getAuthorization()
	if err != nil {
		return
	}
	requestParams, err := NewRequestParamsUsingQueryParams()
	if err != nil {
		return
	}

	admissionCA, clientCert, err := readCerts()
	if err != nil {
		return
	}

	response, err := doRequest("DELETE", apipath+"/"+name, admissionCA, clientCert, authorization, requestParams)
	if err != nil {
		return
	}
	err = analyzeResponseError(response)
	if err != nil {
		return
	}
	return
}
