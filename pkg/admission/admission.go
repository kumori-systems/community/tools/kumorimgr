/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package admission

import (
	"cluster-manager/pkg/logger"
	"cluster-manager/pkg/viper"
	x509Validator "cluster-manager/pkg/x509-validator"
	"crypto/tls"
	"crypto/x509"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"os"
	"time"

	"github.com/Jeffail/gabs"
)

// TODO: admission access viper configuration directly... good idea?

const defaultAdmissionTimeOut = time.Second * 60

func Login(user string, password string) (err error) {
	meth := "admission.Login"
	authorization := "Basic " + base64.StdEncoding.EncodeToString([]byte(user+":"+password))
	reqParams, err := NewRequestParamsUsingQueryParams()
	if err != nil {
		return
	}
	logger.Info("Init", "meth", meth)
	resJSON, err := doRequest("GET", "auth/login", nil, nil, authorization, reqParams)
	if err != nil {
		return
	}
	logger.Info("Done", "response", resJSON.String(), "meth", meth)
	if resJSON.Exists("message") { // Admission returns "message" when error
		err = fmt.Errorf(resJSON.Path("message").Data().(string))
		return
	}
	accessToken, accessTokenExpiryDate, refreshToken, refreshTokenExpiryDate, err :=
		tokensFromJSON(resJSON)
	if err != nil {
		return
	}
	err = writeTokens(
		accessToken, accessTokenExpiryDate,
		refreshToken, refreshTokenExpiryDate,
	)
	return
}

func Logout() (err error) {
	meth := "admission.Logout"
	logger.Info("Init", "meth", meth)
	return writeTokens("", "", "", "")
}

func RefreshTokenIfNeeded() (refreshed bool, err error) {
	meth := "admission.RefreshTokenIfNeeded"
	logger.Info("Init", "meth", meth)
	refreshed = false
	accessToken, accessTokenExpiryDate, refreshToken, refreshTokenExpiryDate, err := readTokens()
	if err != nil {
		return
	}
	if accessToken != "" && accessTokenExpiryDate.Before(time.Now()) {
		if refreshToken == "" || refreshTokenExpiryDate.Before(time.Now()) {
			fmt.Println("------------------------------------------------------------------------------")
			fmt.Println("Both access and refresh tokens have expired. Please use kumorimgr login again.")
			fmt.Println("------------------------------------------------------------------------------")
			return
		}
		authorization := "Bearer " + accessToken
		var reqParams RequestParams
		reqParams, err = NewRequestParamsUsingFormParams(
			"grant_type", "refresh_token",
			"refresh_token", refreshToken,
		)
		if err != nil {
			return
		}

		var admissionCA []byte
		var clientCert *CertificatePair
		admissionCA, clientCert, err = readCerts()
		if err != nil {
			return
		}

		var resJSON *gabs.Container
		resJSON, err = doRequest("POST", "auth/tokens/refresh", admissionCA, clientCert, authorization, reqParams)
		if err != nil {
			return
		}
		logger.Info("Done", "response", resJSON.String(), "meth", meth)
		var accessToken, accessTokenExpiryDate, refreshToken, refreshTokenExpiryDate string
		accessToken, accessTokenExpiryDate, refreshToken, refreshTokenExpiryDate, err =
			tokensFromJSON(resJSON)
		if err != nil {
			return
		}
		err = writeTokens(
			accessToken, accessTokenExpiryDate,
			refreshToken, refreshTokenExpiryDate,
		)
		refreshed = true
		if err != nil {
			return
		}
	}
	return
}

func DescribeCluster() (state *gabs.Container, err error) {
	return commonGet("admission/clusterinfo")
}

func GetClusterConfig() (clusterConfig *ClusterConfiguration, err error) {

	// The cluster configuration call not requires any authentication type.
	admissionAuth := viper.Global.GetString("admission-authentication-type")
	if AdmissionAuth(admissionAuth) != ClientCertAdmissionAuth {
		admissionAuth = ""
	}

	reqParams, _ := NewRequestParamsUsingQueryParams()
	response, err := doRequest("GET", "admission/clusterconfig", nil, nil, admissionAuth, reqParams)

	// If the request to Admission fails, the error is returned
	if err != nil {
		return nil, err
	}

	// If the request Admission doesnt fail, but its response describes an error,
	// then an error is returned
	if !response.Path("success").Data().(bool) {
		strError := "unknown"
		if response.Exists("message") {
			strError = response.Path("message").Data().(string)
		}
		err = errors.New(strError)
		return nil, err
	}

	clusterConfigStr := response.Path("data").EncodeJSON()
	clusterConfig = &ClusterConfiguration{}
	err = json.Unmarshal(clusterConfigStr, clusterConfig)

	return clusterConfig, err
}

// commonGet executes a GET method assuming certain structure in response and
// authorization requirement
func commonGet(path string) (data *gabs.Container, err error) {
	authorization, err := getAuthorization()
	if err != nil {
		return
	}

	admissionCA, clientCert, err := readCerts()
	if err != nil {
		return nil, err
	}

	requestParams, err := NewRequestParamsUsingQueryParams()
	if err != nil {
		return
	}
	response, err := doRequest("GET", path, admissionCA, clientCert, authorization, requestParams)
	if err != nil {
		return
	}
	err = analyzeResponseError(response)
	if err != nil {
		return
	}
	found := response.Exists("data")
	if !found {
		err = fmt.Errorf("Unexpected result. Original: %s", response.String())
		return
	}
	data, err = response.JSONPointer("/data")
	if err != nil {
		return
	}
	return
}

func doRequest(
	httpMethod string,
	path string,
	admissionCA []byte,
	clientCert *CertificatePair,
	authorization string,
	requestParams RequestParams,
) (
	resJSON *gabs.Container, err error,
) {
	meth := "admission.doRequest"
	logger.Debug("Requesting", "method", httpMethod, "path", path, "meth", meth)
	admissionURL, err := getAdmissionUrl()
	if err != nil {
		return
	}
	body, contentType, err := requestParams.GetBody()
	if err != nil {
		return
	}
	req, err := http.NewRequest(httpMethod, admissionURL+"/"+path, body)
	if err != nil {
		return
	}
	if contentType != "" {
		req.Header.Set("Content-Type", contentType)
	}
	if authorization != "" {
		req.Header.Set("Authorization", authorization)
	}
	httpClient := &http.Client{Timeout: defaultAdmissionTimeOut}

	if clientCert != nil {
		transport, ok := httpClient.Transport.(*http.Transport)
		if !ok {
			transport = &http.Transport{
				TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
			}
		}
		if transport.TLSClientConfig == nil {
			transport.TLSClientConfig = &tls.Config{}
		}

		// Adds the admission CA only if a private CA is defined. Otherwise, it will rely on the CAs installed in the OS.
		if len(admissionCA) <= 0 {
			logger.Debug("Requesting", "Admission CA", admissionCA, "Method", meth)
			caCertPool := x509.NewCertPool()
			caCertPool.AppendCertsFromPEM(admissionCA)
			transport.TLSClientConfig.RootCAs = caCertPool
		}

		logger.Debug("Requesting", "Certificate", clientCert, "Method", meth)

		certificate, err := tls.X509KeyPair(clientCert.cert, clientCert.key)
		if err != nil {
			logger.Fatal(err.Error())
		}
		transport.TLSClientConfig.Certificates = []tls.Certificate{certificate}
		httpClient.Transport = transport
	}

	res, err := httpClient.Do(req)
	if err != nil {
		return
	}
	defer res.Body.Close()
	resRaw, err := io.ReadAll(res.Body)
	if err != nil {
		return
	}
	if res.StatusCode != 200 {
		// Check if response is a JSON
		parseRes, parseErr := gabs.ParseJSON(resRaw)
		if parseErr != nil {
			err = fmt.Errorf(string(resRaw))
			return
		}
		err = analyzeResponseError(parseRes)
		return
	} else {

	}
	resJSON, err = gabs.ParseJSON(resRaw)
	if err != nil {
		err = fmt.Errorf(string(resRaw))
		return
	}
	return
}

// analyzeResponseError analyzes response errors when response is like:
// { message: "bla bla bla", success: false, [...] }
func analyzeResponseError(response *gabs.Container) (err error) {
	success, foundSuccess := response.Path("success").Data().(bool)
	message, foundMessage := response.Path("message").Data().(string)
	if foundSuccess && foundMessage {
		if !success {
			err = fmt.Errorf(message)
		} else {
			err = nil // no error
		}
	} else {
		err = fmt.Errorf("Unexpected result: %s", response.String())
	}
	return
}

func readCerts() (
	admissionCA []byte,
	clientCert *CertificatePair,
	err error,
) {

	admissionAuth := viper.Global.GetString("admission-authentication-type")
	if AdmissionAuth(admissionAuth) != ClientCertAdmissionAuth {
		return
	}

	admissionCARaw := viper.Global.GetString("admission-ca")
	if len(admissionCARaw) > 0 {
		if admissionCA, err = base64.StdEncoding.DecodeString(admissionCARaw); err != nil {
			return
		}
	} else {
		if admissionCAFile := viper.Global.GetString("admission-ca-file"); len(admissionCAFile) > 0 {
			if admissionCA, err = os.ReadFile(admissionCAFile); err != nil {
				return
			}
		}
	}

	if len(admissionCA) > 0 {
		logger.Debug("Validaitng admission CA")
		if err = x509Validator.ValidateCertificateRaw(admissionCA); err != nil {
			return
		}
	} else {
		logger.Debug("Admission CA not set")
	}

	var cert, key []byte

	clientCertRaw := viper.Global.GetString("client-cert")
	if len(clientCertRaw) > 0 {
		if cert, err = base64.StdEncoding.DecodeString(clientCertRaw); err != nil {
			return
		}
	} else {
		if clientCertFile := viper.Global.GetString("client-cert-file"); len(clientCertFile) > 0 {
			if cert, err = os.ReadFile(clientCertFile); err != nil {
				return
			}
		} else {
			err = fmt.Errorf("missing client certificate")
			return
		}
	}

	clientKeyRaw := viper.Global.GetString("client-key")
	if len(clientKeyRaw) > 0 {
		if key, err = base64.StdEncoding.DecodeString(clientKeyRaw); err != nil {
			return
		}
	} else {
		if clientKeyFile := viper.Global.GetString("client-key-file"); len(clientKeyFile) > 0 {
			if key, err = os.ReadFile(clientKeyFile); err != nil {
				return
			}
		} else {
			err = fmt.Errorf("missing client private key")
			return
		}
	}

	// Validates the client certificate and key pairs
	logger.Debug("Validaitng client certificate")
	if err = x509Validator.ValidateKeyPairRaw(cert, key); err != nil {
		return
	}

	clientCert = &CertificatePair{
		cert: cert,
		key:  key,
	}

	return
}

func readTokens() (
	accessToken string, accessTokenExpiryDate time.Time,
	refreshToken string, refreshTokenExpiryDate time.Time,
	err error,
) {

	admissionAuth := viper.Global.GetString("admission-authentication-type")
	if AdmissionAuth(admissionAuth) != TokenClientCertAdmissionAuth {
		return
	}

	accessToken = viper.Global.GetString("access-token")
	accessTokenExpiryDate = time.Time{}
	if accessToken != "" {
		accessTokenExpiryDateStr := viper.Global.GetString("access-token-expiry-date")
		err = accessTokenExpiryDate.UnmarshalText([]byte(accessTokenExpiryDateStr))
		if err != nil {
			return
		}
	}
	refreshToken = viper.Global.GetString("refresh-token")
	refreshTokenExpiryDate = time.Time{}
	if refreshToken != "" {
		refreshTokenExpiryDateStr := viper.Global.GetString("refresh-token-expiry-date")
		err = refreshTokenExpiryDate.UnmarshalText([]byte(refreshTokenExpiryDateStr))
		if err != nil {
			return
		}
	}
	return
}

func writeTokens(
	accessToken string, accessTokenExpiryDate string,
	refreshToken string, refreshTokenExpiryDate string,
) (
	err error,
) {
	viper.Global.Set("access-token", accessToken)
	viper.Global.Set("access-token-expiry-date", accessTokenExpiryDate)
	viper.Global.Set("refresh-token", refreshToken)
	viper.Global.Set("refresh-token-expiry-date", refreshTokenExpiryDate)
	err = viper.Global.WriteConfig()
	if err != nil {
		err = fmt.Errorf(
			"Workspace initialization is mandatory to use this command (error %s)",
			err.Error(),
		)
		return
	}
	return
}

func getAdmissionUrl() (admissionURL string, err error) {
	admissionProtocol := viper.Global.GetString("admission-protocol")
	admissionHost := viper.Global.GetString("admission")
	if admissionProtocol == "" || admissionHost == "" {
		err = fmt.Errorf("Admission URL is empty")
	}
	admissionURL = admissionProtocol + "://" + admissionHost
	return
}

func getAuthorization() (authorization string, err error) {
	accessToken, _, _, _, err := readTokens()
	if err != nil {
		return
	}
	authorization = "Bearer " + accessToken
	return
}

func secondsToDateTimeString(numSeconds int) (dateTime string, err error) {
	duration := time.Duration(numSeconds) * time.Second
	dateTimeBytes, err := time.Now().Add(duration).MarshalText()
	if err != nil {
		return
	}
	dateTime = string(dateTimeBytes)
	return
}

func hoursToDateTimeString(numHours int) (dateTime string, err error) {
	duration := time.Duration(numHours) * time.Hour
	dateTimeBytes, err := time.Now().Add(duration).MarshalText()
	if err != nil {
		return
	}
	dateTime = string(dateTimeBytes)
	return
}

func tokensFromJSON(
	objJSON *gabs.Container,
) (
	accessToken string, accessTokenExpiryDate string,
	refreshToken string, refreshTokenExpiryDate string,
	err error,
) {

	// If Admission response includes the necessary token data, extract it.
	// Otherwise, return a clean error (to avoid a nasty stacktrace)
	if objJSON.Exists("access_token") && objJSON.Exists("expires_in") && objJSON.Exists("refresh_token") && objJSON.Exists("refresh_expires_in") {

		accessToken = objJSON.Path("access_token").Data().(string)
		accessTokenExpiresSeconds := int(objJSON.Path("expires_in").Data().(float64))
		accessTokenExpiryDate, err = secondsToDateTimeString(accessTokenExpiresSeconds)
		if err != nil {
			return
		}
		refreshToken = objJSON.Path("refresh_token").Data().(string)
		refreshTokenExpiresSeconds := int(objJSON.Path("refresh_expires_in").Data().(float64))

		if refreshTokenExpiresSeconds == 0 {
			// Expiration date is unknown, use a date very far in the future (10 years)
			refreshTokenExpiryDate, err = hoursToDateTimeString(10 * 365 * 24)
		} else {
			refreshTokenExpiryDate, err = secondsToDateTimeString(refreshTokenExpiresSeconds)
		}

		if err != nil {
			return
		}
	} else {
		err = fmt.Errorf("Unable to get token information from response.")
		return
	}

	return
}

// gabsArrayToSlice converts a gabs JSON containing an array of strings to
// a go-slice of strings
func gabsArrayToSlice(data *gabs.Container) (items []string, err error) {
	dataChildren, err := data.Children()
	if err != nil {
		return
	}
	for _, child := range dataChildren {
		items = append(items, child.Data().(string))
	}
	return
}
