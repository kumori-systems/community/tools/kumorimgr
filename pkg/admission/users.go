/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package admission

import (
	"github.com/Jeffail/gabs"
)

// Admission package functions related to users
//
// The Users API responses have this structure:
// {
//   "success": true / false
//   "message": "OK" / "description of the error"
//   "data": ...
// }
//

func GetUsers() (users *gabs.Container, err error) {
	return commonGet("admission/users")
}

func CreateUser(jsonUser []byte) (err error) {
	authorization, err := getAuthorization()
	if err != nil {
		return
	}
	requestParams, err := NewRequestParamsUsingJSONPayload(jsonUser)
	if err != nil {
		return
	}

	admissionCA, clientCert, err := readCerts()
	if err != nil {
		return
	}

	response, err := doRequest("POST", "admission/users", admissionCA, clientCert, authorization, requestParams)
	if err != nil {
		return
	}
	err = analyzeResponseError(response)
	if err != nil {
		return
	}
	return
}

func UpdateUser(jsonUser []byte) (err error) {
	authorization, err := getAuthorization()
	if err != nil {
		return
	}
	requestParams, err := NewRequestParamsUsingJSONPayload(jsonUser)
	if err != nil {
		return
	}

	admissionCA, clientCert, err := readCerts()
	if err != nil {
		return
	}

	response, err := doRequest("PUT", "admission/users", admissionCA, clientCert, authorization, requestParams)
	if err != nil {
		return
	}
	err = analyzeResponseError(response)
	if err != nil {
		return
	}
	return
}

func DeleteUser(userName string) (err error) {
	authorization, err := getAuthorization()
	if err != nil {
		return
	}
	requestParams, err := NewRequestParamsUsingQueryParams()
	if err != nil {
		return
	}

	admissionCA, clientCert, err := readCerts()
	if err != nil {
		return
	}

	response, err := doRequest("DELETE", "admission/users/"+userName, admissionCA, clientCert, authorization, requestParams)
	if err != nil {
		return
	}
	err = analyzeResponseError(response)
	if err != nil {
		return
	}
	return
}

func ChangeUserPassword(user string, jsonChange []byte) (err error) {
	authorization, err := getAuthorization()
	if err != nil {
		return
	}
	requestParams, err := NewRequestParamsUsingJSONPayload(jsonChange)
	if err != nil {
		return
	}

	admissionCA, clientCert, err := readCerts()
	if err != nil {
		return
	}

	response, err := doRequest("PATCH", "admission/users/"+user+"/password", admissionCA, clientCert, authorization, requestParams)
	if err != nil {
		return
	}
	err = analyzeResponseError(response)
	if err != nil {
		return
	}
	return
}
