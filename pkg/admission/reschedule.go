/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package admission

import (
	"cluster-manager/pkg/logger"

	"github.com/Jeffail/gabs"
)

func RunRescheduler(jsonConfig []byte) (err error) {
	authorization, err := getAuthorization()
	if err != nil {
		return
	}
	if len(jsonConfig) > 0 {
		logger.Info("Configuration", "meth", "admission.RunRescheduler", "configuration", string(jsonConfig))
	} else {
		logger.Info("Configuration: cluster default")
	}
	requestParams, err := NewRequestParamsUsingJSONPayload(jsonConfig)
	if err != nil {
		return
	}

	admissionCA, clientCert, err := readCerts()
	if err != nil {
		return
	}

	response, err := doRequest("POST", "management/reschedule/descheduler", admissionCA, clientCert, authorization, requestParams)
	if err != nil {
		return
	}
	err = analyzeResponseError(response)
	if err != nil {
		return
	}
	return
}

func GetReschedulerRuns() (users *gabs.Container, err error) {
	return commonGet("management/reschedule/descheduler")
}
