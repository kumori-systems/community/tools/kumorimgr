/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package rescheduler

import (
	"encoding/json"
	"time"
)

// Run struct contains the properties of a rescheduler run
type Run struct {
	StartTime      string  `json:"startTime"`
	CompletionTime *string `json:"duration"`
	Status         string  `json:"status"`
	Reason         string  `json:"reason"`
}

// RunsList struct contains the properties of a list of users
type RunsList []Run

// ToString returns the rescheduler run object as a string
func (r Run) ToString() (strRun string) {
	jsonRun, err := json.Marshal(r)
	if err == nil {
		strRun = string(jsonRun)
	}
	return
}

// Len returns the runs list length
func (l *RunsList) Len() int {
	if l == nil {
		return 0
	}

	return len(*l)
}

// Less returns true if the run in position "i" was launched before
// run in position "j". Needed to sort runs list
func (l *RunsList) Less(i, j int) bool {
	if l == nil {
		return false
	}
	first := (*l)[i]
	second := (*l)[j]

	firsTime, err := time.Parse(time.RFC3339, first.StartTime)
	if err != nil {
		return false
	}
	secondTime, err := time.Parse(time.RFC3339, second.StartTime)
	if err != nil {
		return false
	}

	return firsTime.Before(secondTime)
}

// Swap swaps element "i" and "j" positions. Needed to sort runs list
func (l *RunsList) Swap(i, j int) {
	if l == nil {
		return
	}

	if len(*l) <= j {
		return
	}

	aux := (*l)[i]
	(*l)[i] = (*l)[j]
	(*l)[j] = aux
}
