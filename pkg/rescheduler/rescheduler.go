/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package rescheduler

import (
	"cluster-manager/pkg/admission"
	"cluster-manager/pkg/logger"
	"encoding/json"
	"fmt"

	"strconv"

	"github.com/Jeffail/gabs"
)

// Rescheduler contains the rescheduler config
type Rescheduler struct {
	Config *Config
}

// Config contains the configuration to run rescheduler
type Config struct {
	LowNodeUtilization         *LowNodeUtilization         `json:"lowNodeUtilization,omitempty"`
	SpreadConstraintsViolation *SpreadConstraintsViolation `json:"spreadConstraintsViolation,omitempty"`
}

// LowNodeUtilizationThreshold contains the configuration for a LowNodeUtilization threshold
type LowNodeUtilizationThreshold struct {
	CPU    *int32 `json:"cpu,omitempty"`
	Memory *int32 `json:"memory,omitempty"`
	Pods   *int32 `json:"pods,omitempty"`
}

// LowNodeUtilization contains the configuration for the LowNodeUtilization strategy
type LowNodeUtilization struct {
	Enabled   bool                         `json:"enabled"`
	Threshold *LowNodeUtilizationThreshold `json:"threshold,omitempty"`
	Target    *LowNodeUtilizationThreshold `json:"target,omitempty"`
}

// SpreadConstraintsViolation contains the configuration for the SpreadConstraintsViolation strategy
type SpreadConstraintsViolation struct {
	Enabled bool `json:"enabled"`
}

// Run runs rescheduler using the given configuration
func (d *Rescheduler) Run() (err error) {
	// If a custom configuration is provided, convert the configuration into a
	// JSON document marshaled into a string
	var jsonConfig []byte
	if d.Config != nil {
		jsonConfig, err = json.Marshal(d.Config)
		if err != nil {
			return fmt.Errorf("Wrong configuration format: %v", err)
		}
	}
	err = admission.RunRescheduler(jsonConfig)
	if err != nil {
		return err
	}
	return
}

// GetReschedulerRuns retrieves the current list of users using Admission API
func (d *Rescheduler) GetReschedulerRuns() (userJSON *gabs.Container, err error) {
	return admission.GetReschedulerRuns()
}

// RunsListFromJSON creates an RunsList object from JSON
func RunsListFromJSON(
	runsGabs *gabs.Container,
	allowNoPassword bool,
) (*RunsList, error) {
	runsList := RunsList{}

	runsChildren, err := runsGabs.Children()
	if err != nil {
		return &runsList, err
	}

	for _, runGabs := range runsChildren {
		if runGabs.Exists("status") {
			runData := Run{
				StartTime: runGabs.Search("status", "startTime").Data().(string),
				Status:    "Unknown",
				Reason:    "",
			}
			if runGabs.Exists("status", "completionTime") {
				completionTime := runGabs.Search("status", "completionTime").Data().(string)
				runData.CompletionTime = &completionTime
			}

			// Checks the number of active, failed and succeeded pods
			active := 0
			failed := 0
			succeeded := 0
			if runGabs.Exists("status", "active") {
				value := runGabs.Search("status", "active").String()
				active, err = strconv.Atoi(value)
				if err != nil {
					logger.Error("Error getting the number of active pods: %s", err.Error())
				}
			}
			if runGabs.Exists("status", "failed") {
				value := runGabs.Search("status", "failed").String()
				failed, _ = strconv.Atoi(value)
				if err != nil {
					logger.Error("Error getting the number of failed pods: %s", err.Error())
				}
			}
			if runGabs.Exists("status", "succeeded") {
				value := runGabs.Search("status", "succeeded").String()
				succeeded, _ = strconv.Atoi(value)
				if err != nil {
					logger.Error("Error getting the number of succeded pods: %s", err.Error())
				}
			}

			// Initializes the run status considering the number of active, succeeded and failed pods.
			// If at least one pod failed, the run is considered as "Failed". If there isn't any pod
			// failed and at least one pod is active, the run is considered "Active". If there isn't
			// any failed or active pod and there is at least one succeeded pod, the status is "Succeeded".
			// Note that the status set at this point can be modified if any condition is active (has
			// status: true) in the conditions list
			if failed > 0 {
				runData.Status = "Failed"
			} else if active > 0 {
				runData.Status = "Active"
			} else if succeeded > 0 {
				runData.Status = "Succeeded"
			}

			// Reviews the job conditions and checks if there is any condition active (status: true). If
			// that's the case, the condition type is assigned as the run status. The possible types are:
			// * Failed
			// * Suspended
			// * Complete
			if runGabs.Exists("status", "conditions") {
				children, err := runGabs.Search("status", "conditions").Children()
				if err != nil {
					return &runsList, err
				}
				for _, child := range children {
					conditionStatus := child.Search("status").Data().(string)
					if conditionStatus != "True" {
						continue
					}
					conditionType := child.Search("type").Data().(string)
					runData.Status = conditionType
					if child.Exists("reason") {
						reason := child.Search("reason").Data().(string)
						runData.Reason = reason
					}
					break
				}
			}

			runsList = append(runsList, runData)
		}
	}

	return &runsList, nil
}
